﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.CharacterController {
    /// <summary>
    /// Assignable component to a volume (simply an area on the map
    /// with a triggerable box collider)
    /// This way this area can contain certain volume params like
    /// friction etc.
    /// 
    /// The params are ecapsulated in this monobehaviour to make
    /// - the params accessible as a component in an are
    /// - the params directly configurable inside the character controller
    ///     through the inspector
    /// 
    /// Tt is possible to assign 1 volume to an area or multiple volumes.
    /// 
    /// A volume component affects all game objects that enter the area 
    /// (if these gameobjects implements ontriggerenter).
    /// It is also possible to assign the volume a target. The target is
    /// taken from ControllerPhsyicsVolume2D.Targets, a tedious solution
    /// but the most straightforward.
    /// If the volume has a target all gameobjects of the same target are 
    /// affected. Therefore the game objects that shall be affected must have
    /// the same "target" in their "ControllerPhysicsVolume2D"
    /// 
    /// Example: the area "water" has a volume with a speedfactor 2.
    /// This volume has as "target" with the enum "ork" assigned.
    /// Every gameobject that has the component "ControllerPhysicsVolume2D" 
    /// with the target "ork" will be affected.
    /// </summary>
    public class ControllerPhsyicsVolume2D : MonoBehaviour {
        #region CONSTANTS 
        /// <summary>
        /// possible volume targets
        /// </summary>
        public enum Targets {
            Default,
            Player,
            Slime,
            Ork
        }
        #endregion CONSTANTS 

        #region CONFIGURATION
        /// <summary>
        /// The type of gameobject that will be affected by the volume. 
        /// May be left empty 
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("The type of gameobject that will be affected by the volume")]
        [SerializeField]
        private Targets _target;

        /// <summary>
        /// The parameters
        /// </summary>
        [SerializeField]
        private ControllerPhysicsVolume2DParams _parameters;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of gameobject that will be affected by the volume. 
        /// May be left empty 
        /// </summary>
        public Targets Target {
            get { return _target; }
        }

        /// <summary>
        /// configuration of the parameters of this volume
        /// </summary>
        public ControllerPhysicsVolume2DParams Parameters {
            get { return _parameters; }
        }
        #endregion
    }
}