﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.CharacterController {
    /// <summary>
    /// An arcade oriented charactercontroller
    /// </summary>
	public class CharacterController2D : MonoBehaviour {
        #region CONSTANTS
        private const float SkinWidth = .02f;
        private const int TotalHorizontalRays = 8;
        private const int TotalVerticalRays = 4;

        /// <summary>
        /// The min value when a horizontal and vertical movement are triggered
        /// </summary>
	    private const float AxisTreshhold = .001f;
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// Obstacle mask
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [SerializeField]
        private LayerMask _obstacleMask;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Getter for the _velocity
        /// </summary>
        public Vector2 Velocity {
            get { return _velocity; }
        }

        /// <summary>
        /// Normally collisions are handeled, but not eg. during death
        /// </summary>
        public bool HandleCollisions {
            get; set;
        }

        /// <summary>
        /// Getter for the current volume params.
        /// by default return the params from _volumeParameters.
        /// Those are set in the configuration
        /// But if a volume like water or sand contains volume params 
        /// use them
        /// </summary>
        public ControllerPhysicsVolume2DParams Parameters {
            get { return _overrideVolumeParameters ?? _volumeParameters; }
        }
        #endregion PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Final velocity of the character
        /// </summary>
        private Vector2 _velocity;

        /// <summary>
        /// The space between the collider and the origin of the
        /// rays. Purpose: let the rays _not_ start at the edge of
        /// the collider but a little bit inside to avoid "blank holes"
        /// at the corners
        /// </summary>
	    private float _verticalDistanceBetweenRays;
        private float _horizontalDistanceBetweenRays;

        /// <summary>
        /// For calculating the raycast origins 3 origins
        /// inside the player collider are required
        /// </summary>
        private Vector3 _raycastTopLeft;
        private Vector3 _raycastBottomRight;
        private Vector3 _raycastBottomLeft;

        /// <summary>
        /// Cached transform component
        /// </summary>
	    private Transform _t;

        /// <summary>
        /// Cached local scale component
        /// </summary>
        private Vector3 _ls;

        /// <summary>
        /// Cached box collider component
        /// </summary>
	    private BoxCollider2D _c;

        /// <summary>
        /// Reference to the volume = a gameobject with the character 
        /// controller needs as separate component a volume
        /// </summary>
        private ControllerPhsyicsVolume2D _volume;

        /// <summary>
        /// The default volume the character is using.
        /// a volume contains parameters like friction, gravitiy, acceleration
        /// etc. to influence the movement of a character
        /// Examples are sand, water, grass etc.
        /// </summary>
        private ControllerPhysicsVolume2DParams _volumeParameters;

        /// <summary>
        /// The default "volume" parameters may be replaced by other
        /// volume parameters. 
        /// This will happen if the character excounters a gameobject
        /// that has volume parameters like water
        /// </summary>
        private ControllerPhysicsVolume2DParams _overrideVolumeParameters;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();

            HandleCollisions = true;

            _volumeParameters = _volume.Parameters;

            var colliderWidth = _c.size.x * Mathf.Abs(_ls.x) - (2 * SkinWidth);
            _horizontalDistanceBetweenRays = colliderWidth / (TotalVerticalRays - 1);

            var colliderHeight = _c.size.y * Mathf.Abs(_ls.y) - (2 * SkinWidth);
            _verticalDistanceBetweenRays = colliderHeight / (TotalHorizontalRays - 1);
        }

        /// <summary>
        /// LateUpdate event
        /// </summary>
        public void LateUpdate() {
            Move(Velocity * Parameters.SpeedFactor * Time.deltaTime);
        }

        /// <summary>
        /// Check if the ground the character is standing on has 
        /// other volume parameters (like max velocity, friction etc.)
        /// </summary>
        /// <param name="other"></param>
        public void OnTriggerEnter2D(Collider2D other) {
            ControllerPhsyicsVolume2D[] areaVolumes = other.gameObject
                .GetComponents<ControllerPhsyicsVolume2D>();

            if (areaVolumes == null || 
                areaVolumes.Length == 0)
                return;

            // defaultVolume is the volume of the area that applies to
            // all characters if the have no special volume
            ControllerPhsyicsVolume2D defaultVolume = null;
            // volume will be set if the area contains a special volume
            // for the current gameobject
            ControllerPhsyicsVolume2D specialVolume = null;

            foreach (ControllerPhsyicsVolume2D v in areaVolumes) {
                if (v.Target == ControllerPhsyicsVolume2D.Targets.Default)
                    defaultVolume = v;
                if (v.Target == _volume.Target)
                    specialVolume = v;
            }

            ControllerPhsyicsVolume2D usedVolume = specialVolume ?? defaultVolume;
            if (usedVolume != null)
                _overrideVolumeParameters = usedVolume.Parameters;
        }

        /// <summary>
        /// Check if the character is on a "normal" ground
        /// </summary>
        /// <param name="other"></param>
        public void OnTriggerExit2D(Collider2D other) {
            ControllerPhsyicsVolume2D volume = other.gameObject
                .GetComponent(typeof(ControllerPhsyicsVolume2D)) as ControllerPhsyicsVolume2D;

            if (volume == null)
                return;
            _overrideVolumeParameters = null;
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        public void AddForce(Vector2 force) {
            _velocity += force;
        }

        public void SetForce(Vector2 force) {
            _velocity = force;
        }

        public void Stop() {
            _velocity = Vector2.zero;
        }

        public void SetHorizontalForce(float x) {
            _velocity.x = x;
        }

        public void SetVerticalForce(float y) {
            _velocity.y = y;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;
            _ls = transform.localScale;
            _c = GetComponent(typeof(BoxCollider2D)) as BoxCollider2D;
            _volume = GetComponent(typeof(ControllerPhsyicsVolume2D)) as ControllerPhsyicsVolume2D;
        }

        /// <summary>
        /// Move the character
        /// </summary>
        /// <param name="deltaMovement"></param>
        private void Move(Vector2 deltaMovement) {
            if (HandleCollisions) {
                CalculateRayOrigins();

                if (Mathf.Abs(deltaMovement.x) > AxisTreshhold)
                    MoveHorizontally(ref deltaMovement);

                if (Mathf.Abs(deltaMovement.y) > AxisTreshhold)
                    MoveVertically(ref deltaMovement);
            }

            _t.Translate(deltaMovement, Space.World);

            if (Time.deltaTime > 0)
                _velocity = deltaMovement / Time.deltaTime;

            _velocity.x = Mathf.Min(_velocity.x, Parameters.MaxVelocity.x);
            _velocity.y = Mathf.Min(_velocity.y, Parameters.MaxVelocity.y);
        }

        /// <summary>
        /// Calculate the origins from where the rays are cast out
        /// see https://www.youtube.com/watch?v=8_rIw0-AI8w&index=9&list=PLt_Y3Hw1v3QSFdh-evJbfkxCK_bjUD37n
        /// starting at ~00:37:00
        /// </summary>
        private void CalculateRayOrigins() {
            var size = new Vector2(_c.size.x * Mathf.Abs(_ls.x), _c.size.y * Mathf.Abs(_ls.y)) / 2;
            var center = new Vector2(_c.offset.x * _ls.x, _c.offset.y * _ls.y);

            _raycastTopLeft = _t.position + new Vector3(center.x - size.x + SkinWidth, center.y + size.y - SkinWidth);
            _raycastBottomRight = _t.position + new Vector3(center.x + size.x - SkinWidth, center.y - size.y + SkinWidth);
            _raycastBottomLeft = _t.position + new Vector3(center.x - size.x + SkinWidth, center.y - size.y + SkinWidth);
        }

        /// <summary>
        /// Calc the horizontal velocity
        /// </summary>
        /// <param name="deltaMovement"></param>
        private void MoveHorizontally(ref Vector2 deltaMovement) {
            var isGoingRight = deltaMovement.x > 0;
            var rayDistance = Mathf.Abs(deltaMovement.x) + SkinWidth;
            var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
            var rayOrigin = isGoingRight ? _raycastBottomRight : _raycastBottomLeft;

            for (var i = 0; i < TotalHorizontalRays; i++) {
                var rayVector = new Vector2(rayOrigin.x, rayOrigin.y + (i * _verticalDistanceBetweenRays));
                Debug.DrawRay(rayVector, rayDirection * rayDistance, Color.red);

                var rayCastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, _obstacleMask);
                if (!rayCastHit)
                    continue;

                deltaMovement.x = rayCastHit.point.x - rayVector.x;
                rayDistance = Mathf.Abs(deltaMovement.x);

                if (isGoingRight)
                    deltaMovement.x -= SkinWidth;
                else
                    deltaMovement.x += SkinWidth;

                if (rayDistance < SkinWidth + AxisTreshhold)
                    break;
            }
        }

        /// <summary>
        /// Calc the vertical velocity
        /// </summary>
        /// <param name="deltaMovement"></param>
        private void MoveVertically(ref Vector2 deltaMovement) {
            var isGoingUp = deltaMovement.y > 0;
            var rayDistance = Mathf.Abs(deltaMovement.y) + SkinWidth;
            var rayDirection = isGoingUp ? Vector2.up : -Vector2.up;
            var rayOrigin = isGoingUp ? _raycastTopLeft : _raycastBottomLeft;

            rayOrigin.x += deltaMovement.x;

            for (var i = 0; i < TotalVerticalRays; i++) {
                var rayVector = new Vector2(rayOrigin.x + (i * _horizontalDistanceBetweenRays), rayOrigin.y);
                Debug.DrawRay(rayVector, rayDirection * rayDistance, Color.red);

                var raycastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, _obstacleMask);
                if (!raycastHit)
                    continue;

                deltaMovement.y = raycastHit.point.y - rayVector.y;
                rayDistance = Mathf.Abs(deltaMovement.y);

                if (isGoingUp)
                    deltaMovement.y -= SkinWidth;
                else
                    deltaMovement.y += SkinWidth;

                if (rayDistance < SkinWidth + AxisTreshhold)
                    break;
            }
        }
        #endregion PRIVATE_METHODS
    }
}