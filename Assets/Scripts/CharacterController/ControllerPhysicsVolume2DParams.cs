﻿using System;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.CharacterController {
    /// <summary>
    /// The params of a volume
    /// </summary>
    [Serializable]
    public class ControllerPhysicsVolume2DParams {
        #region CONFIGURATION
        /// <summary>
        /// Max velocity inside the volume
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("Max velocity inside the volume")]
        [SerializeField]
        private Vector2 _maxVelocity;

        /// <summary>
        /// Increase or decrease the speed 
        /// </summary>
        [Tooltip("Increase or decrease the speed. 1 means no change")]
        [Range(0, 10)]
        [SerializeField]
        private float _speedFactor;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Max velocity inside the volume
        /// </summary>
        public Vector2 MaxVelocity {
            get { return _maxVelocity; }
        }

        /// <summary>
        /// Increase or decrease the speed 
        /// </summary>
        public float SpeedFactor {
            get { return _speedFactor; }
        }
        #endregion PROPERTIES

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        public ControllerPhysicsVolume2DParams() {
            _maxVelocity = new Vector2(float.MaxValue, float.MaxValue);
            _speedFactor = 1;
        }
        #endregion PUBLIC_METHODS
    }
}