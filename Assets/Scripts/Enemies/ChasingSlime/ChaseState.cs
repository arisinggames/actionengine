﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using ArisingGames.Assets.Scripts.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Enemies.ChasingSlime {
    public class ChaseState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the state machine")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;
        
        /// <summary>
        /// Take a break state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _takeABreakStateGameObject;

        /// <summary>
        /// Sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _watchSensorGameObject;

        /// <summary>
        /// Gameobject containing the bullet object pool
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _bulletPoolGameObject;

        /// <summary>
        /// Inspired by "Unity 4 Game Programming AI": the target this
        /// game object is validating against.
        /// In this case: The target that must be hit so it gets destroyed
        /// </summary>
        [Tooltip("The target that must be chased so it gets destroyed")]
        [AssertReference]
        [SerializeField]
        private GameObject _chaseTargetGameObject;

        /// <summary>
        /// The health attribute of the player to decrease health
        /// </summary>
        [Tooltip("The health attribute of the player to decrease health")]
        [AssertReference]
        [SerializeField]
        private GameObject _healthAttributeGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// Time til next shot
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("Time til next shot")]
        [SerializeField]
        private float _delay = 1f;

        /// <summary>
        /// Acceleration
        /// </summary>
        [Tooltip("Acceleration of slime in chasing state")]
        [SerializeField]
        private float _acceleration = 60;

        /// <summary>
        /// movement speed of slime
        /// </summary>
        [Tooltip("Movement speed of slime")]
        [SerializeField]
        private float _speed = 2;

        /// <summary>
        /// The min distance to the player in chasing state
        /// </summary>
        [Tooltip("The min distance to the player in chasing state")]
        [SerializeField]
        private float _minDistance = 4;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// Bullet pool script
        /// </summary>
        private Pool _bulletPool;

        /// <summary>
        /// Watch sensor script
        /// </summary>
        private WatchSensor _watchSensor;

        /// <summary>
        /// Time between 2 shots
        /// </summary>
        private float _elapsedTime;

        /// <summary>
        /// Reference to owners transform for easier access and caching
        /// </summary>
        private Transform _ot;

        /// <summary>
        /// Reference to chasing targets transform for easier access and caching
        /// </summary>
        private Transform _ct;

        /// <summary>
        /// Reference to the controller
        /// </summary>
        private CharacterController2D _controller;

        /// <summary>
        /// The script health attribute of the gameobject
        /// </summary>
        private HealthAttribute _healthAttribute;

        /// <summary>
        /// Reference to the text item "state" in the console
        /// </summary>
        private Text _consoleState;

        /// <summary>
        /// Reference to the text item (view) "status" in the console
        /// </summary>
        private Text _consoleStatus;

        /// <summary>
        /// Reference to the text item (view) "remaining" in the console
        /// </summary>
        private Text _consoleRemaining;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            t = GameObject.Find("ConsoleState").GetComponent<Text>();

        }

        private Text t;


        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            // as enable is called before Start
            // _ownerTransform might be null

            // reset the countdown
            toidlecountdown = countdown;

            //Debug.Log("setze state chase. aktuell: " + t.text);
            t.text = "State: Chase";
            //Debug.Log("setze state chase. neuer text: " + t.text);



            //            if (_ot != null) {

            _watchSensor.IsRotationEnabled = false;
                Shoot();
//            }
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
            
        }

        const int countdown = 4;
        private float toidlecountdown = countdown;

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {

            //WatchSensor.Detection status = _watchSensor.CheckPlayerState();
            //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: " + status.ToString();



            // if the player is outside the FOV - take a break
            if (!_watchSensor.IsColliding) {

                //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: not colliding";

                //if (status == WatchSensor.Detection.None) {

                //                Debug.Log("^chase: NONE!!!");

                //                Debug.Log("nicht colliding im chase mode. countdown");
                _controller.Stop();
                // if the player isn't any longer in the collider of the enemy
                // and / or not detected - stay in the chase state for n seconds
                // before swtiching to idle and therefore reenabling the rotation
                toidlecountdown -= Time.deltaTime;
                //GameObject.Find("ConsoleRemaining").GetComponent<Text>().text = "Remain: " + toidlecountdown;
                if (toidlecountdown <= 0) {

                    Debug.Log("Nicht colliding im chase mode. Coountdown 0. Wechsel zu idle countdown");
//                    toidlecountdown = countdown;
                    _controller.Stop();
                    gameObject.SetActive(false);
                    _takeABreakStateGameObject.SetActive(true);
                    return;
                }
            }

            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
            /// das muss ggf mit dem if weiter oben gemerged werden
            else if (!_watchSensor.IsDetected) {
                //else if (status != WatchSensor.Detection.Watching) {

                //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: not detected";

                //                Debug.Log("nicht detecting im chase mode. countdown");
                _controller.Stop();

                // if the player isn't any longer in the collider of the enemy
                // and / or not detected - stay in the chase state for n seconds
                // before swtiching to idle and therefore reenabling the rotation
                toidlecountdown -= Time.deltaTime;

                if (toidlecountdown <= 0) {
//                    Debug.Log("Nicht detecting im chase mode. Coountdown 0. Wechsel zu idle countdown");
//                    toidlecountdown = countdown;
                    
                    gameObject.SetActive(false);
                    _takeABreakStateGameObject.SetActive(true);
                    return;
                }
            }


            // if player is dead - take a break
            else if (_chaseTargetGameObject == null) {
//                Debug.Log("player dead");

                gameObject.SetActive(false);
                _takeABreakStateGameObject.SetActive(true);
                return;
            } else {

                //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: something";

                // reset the countdown
                toidlecountdown = countdown;

                UpdateDirections();

                ShootWithDelay();

                Move();
            }

        }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            _watchSensor.IsRotationEnabled = true;
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _controller = _ownerGameObject.GetComponent(typeof(CharacterController2D))
                as CharacterController2D;

            _ot = _ownerGameObject.transform;

            _healthAttribute = _healthAttributeGameObject
                .GetComponent(typeof(HealthAttribute)) as HealthAttribute;
            _bulletPool = _bulletPoolGameObject
                .GetComponent(typeof(Pool)) as Pool;
            _watchSensor = _watchSensorGameObject
                .GetComponent(typeof(WatchSensor)) as WatchSensor;

            _ct = _chaseTargetGameObject.transform;
        }

        /// <summary>
        /// Move the object if it doesn't exceed the min distance to the target
        /// </summary>
        private void Move() {
            float distance = Vector2.Distance(_ot.position, _ct.position);

            if (distance > _minDistance) {

                Vector3 moveDirection = _ct.position - _ot.position;

                Vector2 targetSpeed = Vector2.zero;
                targetSpeed.x = moveDirection.normalized.x * _speed;
                targetSpeed.y = moveDirection.normalized.y * _speed;

                EnemyHelper.Accelerate(_controller, targetSpeed, _acceleration);
            } else 
                _controller.Stop();
        }

        /// <summary>
        /// If the player is alive point view and move
        /// direction into players directionPlayerToMouse
        /// </summary>
        private void UpdateDirections() {
            if (_chaseTargetGameObject != null) {
                _watchSensor.MoveDirection = _chaseTargetGameObject.transform.position;
                _watchSensor.ViewDirection = _chaseTargetGameObject.transform.position;
            }
                
        }

        /// <summary>
        /// Try to shoot the bullet after a certain amount of time
        /// </summary>
        private void ShootWithDelay() {
            _elapsedTime += Time.deltaTime;

            if (_elapsedTime >= _delay) {
                _elapsedTime = 0;
                Shoot();
            }
        }

        /// <summary>
        /// Init a bullet in target direction
        /// </summary>
        private void Shoot() {
            GameObject go = _bulletPool.Get();

            if (go == null)
                return;

            go.transform.position = _ot.position;
            go.transform.rotation = _ot.rotation;

            EnemyBullet bullet = go.GetComponent<EnemyBullet>();
            if (bullet == null)
                throw new ComponentNotFoundException("EnemyBullet", go);

            bullet.HealthAttribute = _healthAttribute;
            bullet.HitTarget = _chaseTargetGameObject;
            bullet.TargetPos = _chaseTargetGameObject.transform.position;
            go.SetActive(true);
        }
        #endregion PRIVATE_METHODS
    }
}