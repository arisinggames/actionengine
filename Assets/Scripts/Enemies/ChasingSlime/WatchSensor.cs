﻿using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Enemies.ChasingSlime {
    /// <summary>
    /// Based on https://www.youtube.com/watch?v=rQG9aUWarwE
    /// </summary>
    public class WatchSensor : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the sensors")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// Inspired by "Unity 4 Game Programming AI": the target this
        /// game object is validating against.
        /// In this case: The target that will be watched for
        /// </summary>
        [Tooltip("The target that will be watched for")]
        [AssertReference]
        [SerializeField]
        private GameObject _watchTargetGameObject;

        /// <summary>
        /// Gameobject containing mesh renderer and mesh filter
        /// </summary>
        [Tooltip("Gameobject containing mesh renderer and mesh filter")]
        [AssertReference]
        [SerializeField]
        private GameObject _meshFilterGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region STRUCTS
        /// <summary>
        /// View cast info piece
        /// </summary>
        public struct ViewCastInfo {
            public bool Hit;
            public Vector3 Point;
            public float Distance;
            public float Angle;

            public ViewCastInfo(bool hit, Vector3 point, float dst, float angle) {
                Hit = hit;
                Point = point;
                Distance = dst;
                Angle = angle;
            }
        }

        /// <summary>
        /// Edge info piece
        /// </summary>
        public struct EdgeInfo {
            public Vector3 PointA;
            public Vector3 PointB;

            public EdgeInfo(Vector3 pointA, Vector3 pointB) {
                PointA = pointA;
                PointB = pointB;
            }
        }
        #endregion STRUCTS

        #region CONFIGURATION
        /// <summary>
        /// On which layer shall the enemy look for the player
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("On which layer shall the enemy look for the player")]
        [SerializeField]
        private LayerMask _targetMask;

        /// <summary>
        /// Which layer contains the collisionable objects?
        /// </summary>
        [Tooltip("Which layer contains the collisionable objects?")]
        [SerializeField]
        private LayerMask _obstacleMask;

        /// <summary>
        /// How many meshes shall be drawn?
        /// </summary>
        [Tooltip("How many meshes shall be drawn?")]
        [SerializeField]
        private float _meshResolution;

        /// <summary>
        /// How many times shall the edge detection of
        /// obstacles be executed? The more the accurate
        /// </summary>
        [Tooltip("How many times shall the edge detection of obstacles be executed?")]
        [SerializeField]
        private int _edgeResolveIterations;

        /// <summary>
        /// Prevent overlapping of 2 meshes raycasts
        /// </summary>
        [SerializeField]
        private float _edgeDstThreshold;

        /// <summary>
        /// Viewradius of the slime
        /// </summary>
        [SerializeField]
        private float _viewRadius = 9;

        /// <summary>
        /// View angle in degree
        /// </summary>
        [Tooltip("View angle in degree")]
        [Range(0, 360)]
        [SerializeField]
        private float _fov = 90;

        /// <summary>
        /// Detection rate for this sense
        /// </summary>
                [SerializeField]
        private float _detectionRate = .2f;

        /// <summary>
        /// Enable the rotation of the FOV?
        /// </summary>
        [SerializeField]
        public bool IsRotationEnabled = true;

        /// <summary>
        /// Show the field of view visually with meshes?
        /// </summary>
        [SerializeField]
        private bool _isDrawFovEnabled = true;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Is the target in the collision circle
        /// </summary>
        public bool IsColliding {
            get { return _isColliding; }
        }

        /// <summary>
        /// Is the target in the field of view
        /// </summary>
        public bool IsInFov {
            get { return _isInFov; }
        }

        /// <summary>
        /// Has the target (player) been detected?
        /// </summary>
        public bool IsDetected {
            get { return _isDetected; }
        }

        /// <summary>
        /// Setter for _viewDirection
        /// Ensure that the script execution order
        /// executes first WatchSensor as Unity calls
        /// by default PatrolState.Start() before 
        /// WatchSensor.Awake() though this shouldn't be...
        /// See https://docs.unity3d.com/Manual/ExecutionOrder.html
        /// </summary>
        public Vector3 ViewDirection {
            set {
                _viewDirection = (value - _ot.position).normalized;
                InitLineFov();
            }
        }

        /// <summary>
        /// Setter for _moveDirection
        /// </summary>
        public Vector3 MoveDirection {
            set {
                _moveDirection = (value - _ot.position).normalized;
//                InitLineFov();
            }
        }
        #endregion PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Is the target inside of the field of view of the owner?
        /// </summary>
        [SerializeField]
        private bool _isInFov;

        /// <summary>
        /// Is target colliding with the owner?
        /// </summary>
        [SerializeField]
        private bool _isColliding;

        /// <summary>
        /// Has the target (player) been detected?
        /// </summary>
        [SerializeField]
        private bool _isDetected;

        /// <summary>
        /// The mesh filter component
        /// </summary>
        private MeshFilter _meshFilter;

        /// <summary>
        /// time between the detection execution
        /// </summary>
        private float _elapsedTime;

        /// <summary>
        /// Mesh to visualize the _fov
        /// </summary>
        private Mesh _viewMesh;

        /// <summary>
        /// When drawing the gizmo: left and right line of the field of view
        /// </summary>
        private Vector2 _leftLineFov;
        private Vector2 _rightLineFov;

        /// <summary>
        /// View direction differs from movement directionPlayerToMouse = game objects position
        /// _viewDirection contains the _current_ view direction which
        /// is changed during update() due the rotation 
        /// </summary>
        private Vector3 _viewDirection;

        /// <summary>
        /// MoveDir is the vector from the game objects position to the 
        /// movements target 
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// Cache owners transform
        /// </summary>
        private Transform _ot;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
            _moveDirection = _ot.position.normalized;
            InitLineFov();

            _viewMesh = new Mesh { name = "View Mesh" };
            _meshFilter.mesh = _viewMesh;
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            if (IsRotationEnabled) {
                float angle = Mathf.PingPong(Time.time*20, _fov);

                if (angle > _fov*.5f)
                    angle = (angle - _fov*.5f)*-1;
                else if (angle < _fov*.5f)
                    angle = (_fov*.5f - angle);

                _viewDirection = RotatePointAroundTransform(_moveDirection, angle);
                _leftLineFov = RotatePointAroundTransform(_moveDirection*_viewRadius, _fov*.5f + angle);
                _rightLineFov = RotatePointAroundTransform(_moveDirection*_viewRadius, -_fov*.5f + angle);
            }

            // FindPlayer();
        }


        

        /// <summary>
        /// LateUpdate event
        /// </summary>
        private void LateUpdate() {
            _elapsedTime += Time.deltaTime;
            //
            if (_elapsedTime >= _detectionRate) {

                
                ////////////////////////////////////////FindPlayer(); ??????????????????????

                FindPlayer();



                _elapsedTime = 0;
            }
            if (_isDrawFovEnabled)
                DrawFieldOfView();
        }

        /// <summary>
        /// Show Debug Grids and obstacles inside the editor
        /// </summary>
        private void OnDrawGizmos() {
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(_ownerGameObject.transform.position, _viewRadius);

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(_ownerGameObject.transform.position, _moveDirection * _viewRadius);

            Gizmos.color = Color.green;
            Gizmos.DrawRay(_ownerGameObject.transform.position, _viewDirection * _viewRadius);

            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(_ownerGameObject.transform.position, _leftLineFov);
            Gizmos.DrawRay(_ownerGameObject.transform.position, _rightLineFov);
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _ot = _ownerGameObject.transform;

            _meshFilter = _meshFilterGameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (_meshFilter == null)
                throw new ComponentNotFoundException("MeshFilter", _meshFilterGameObject);
        }

        /// <summary>
        /// (Re)initialize the field of view borders
        /// </summary>
        private void InitLineFov() {
            _leftLineFov = RotatePointAroundTransform(_moveDirection * _viewRadius, _fov * .5f);
            _rightLineFov = RotatePointAroundTransform(_moveDirection * _viewRadius, -_fov * .5f);
        }


        public enum Detection {
            None,
            Colliding,
            InFov,
            Watching
        }


        /// <summary>
        /// Provisorisch: mir scheint, dass zwischen den states und dem sensor
        /// syncronisationsprobleme bestehen
        /// Deswegen dieser fkt um direkt vom state aus den player zu checken 
        /// </summary>
        public Detection CheckPlayerState() {
            Collider2D c = Physics2D.OverlapCircle(_ot.position, _viewRadius, _targetMask);

            Detection status = Detection.None;

            if (c != null && c.gameObject == _watchTargetGameObject) {
                Transform target = c.transform;
                Vector3 dirToTarget = (target.position - _ot.position).normalized;

                status = Detection.Colliding;

                if (Vector2.Angle(_viewDirection, dirToTarget) < _fov / 2) {
                    float dstToTarget = Vector3.Distance(_ot.position, target.position);

                    status = Detection.InFov;

                    if (!Physics2D.Raycast(_ot.position, dirToTarget, dstToTarget, _obstacleMask))
                        status = Detection.Watching;
                }
            }

            return status;
        }

        /// <summary>
        /// Method should be called by coroutine to 
        /// increase performance
        /// </summary>
        private void FindPlayer() {
            Collider2D c = Physics2D.OverlapCircle(_ot.position, _viewRadius, _targetMask);

            _isColliding = false;
            _isInFov = false;
            _isDetected = false;

            if (c != null && c.gameObject == _watchTargetGameObject) {
                Transform target = c.transform;
                Vector3 dirToTarget = (target.position - _ot.position).normalized;

                _isColliding = true;

                if (Vector2.Angle(_viewDirection, dirToTarget) < _fov / 2) {
                    float dstToTarget = Vector3.Distance(_ot.position, target.position);

                    _isInFov = true;

                    if (!Physics2D.Raycast(_ot.position, dirToTarget, dstToTarget, _obstacleMask)) 
                        _isDetected = true;
                }
            }
        }

        /// <summary>
        /// Draw the field of view
        /// </summary>
        private void DrawFieldOfView() {
            // stepcount = number of meshes for each degree
            int stepCount = Mathf.RoundToInt(_fov * _meshResolution);
            float stepAngleSize = _fov / stepCount;
            List<Vector3> viewPoints = new List<Vector3>();
            ViewCastInfo oldViewCast = new ViewCastInfo();

            float angleViewDir = Mathf.Atan2(_viewDirection.x, _viewDirection.y) * Mathf.Rad2Deg;

            for (int i = 0; i <= stepCount; i++) {
                float angle = angleViewDir - _fov / 2 + stepAngleSize * i;
                ViewCastInfo newViewCast = ViewCast(angle);

                if (i > 0) {
                    bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.Distance - newViewCast.Distance) > _edgeDstThreshold;
                    if (oldViewCast.Hit != newViewCast.Hit || (oldViewCast.Hit && newViewCast.Hit && edgeDstThresholdExceeded)) {
                        EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                        if (edge.PointA != Vector3.zero)
                            viewPoints.Add(edge.PointA);
                        if (edge.PointB != Vector3.zero)
                            viewPoints.Add(edge.PointB);
                    }
                }

                viewPoints.Add(newViewCast.Point);
                oldViewCast = newViewCast;
            }

            int vertexCount = viewPoints.Count + 1;
            Vector3[] vertices = new Vector3[vertexCount];
            int[] triangles = new int[(vertexCount - 2) * 3];

            vertices[0] = Vector3.zero;
            for (int i = 0; i < vertexCount - 1; i++) {
                vertices[i + 1] = _ot.InverseTransformPoint(viewPoints[i]);

                if (i < vertexCount - 2) {
                    triangles[i * 3] = 0;
                    triangles[i * 3 + 1] = i + 1;
                    triangles[i * 3 + 2] = i + 2;
                }
            }

            _viewMesh.Clear();

            _viewMesh.vertices = vertices;
            _viewMesh.triangles = triangles;
            _viewMesh.RecalculateNormals();
        }

        /// <summary>
        /// Create viewcast info piece
        /// </summary>
        /// <param name="globalAngle"></param>
        /// <returns></returns>
        private ViewCastInfo ViewCast(float globalAngle) {
            Vector3 dir = DirFromAngle(globalAngle, true);
            RaycastHit2D hit = Physics2D.Raycast(_ot.position, dir, _viewRadius, _obstacleMask);

            if (hit.transform != null)
                return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);

            return new ViewCastInfo(false, _ot.position + dir * _viewRadius, _viewRadius, globalAngle);
        }

        /// <summary>
        /// Create edge info piece
        /// </summary>
        /// <param name="minViewCast"></param>
        /// <param name="maxViewCast"></param>
        /// <returns></returns>
        private EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
            float minAngle = minViewCast.Angle;
            float maxAngle = maxViewCast.Angle;
            Vector3 minPoint = Vector3.zero;
            Vector3 maxPoint = Vector3.zero;

            for (int i = 0; i < _edgeResolveIterations; i++) {
                float angle = (minAngle + maxAngle) / 2;
                ViewCastInfo newViewCast = ViewCast(angle);

                bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.Distance - newViewCast.Distance) > _edgeDstThreshold;
                if (newViewCast.Hit == minViewCast.Hit && !edgeDstThresholdExceeded) {
                    minAngle = angle;
                    minPoint = newViewCast.Point;
                } else {
                    maxAngle = angle;
                    maxPoint = newViewCast.Point;
                }
            }

            return new EdgeInfo(minPoint, maxPoint);
        }

        /// <summary>
        /// Based on the current objects pos pass an Angle to
        /// the method and return the direction
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <param name="angleIsGlobal"></param>
        /// <returns></returns>
        private Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
            if (!angleIsGlobal)
                angleInDegrees += _ownerGameObject.transform.eulerAngles.y;

            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }

        /// <summary>
        /// Acoording to the direction of the current sprite 
        /// rotate the lines of the _fov and return a vector with the new direction
        /// This rotation is based on a matrix calculation
        /// https://www.youtube.com/watch?v=iXEMYkwAl-0 
        ///
        /// Rotate point (px, py) around Point (ox, oy) by Angle theta you'll get:
        /// p'x = cos(theta) * (px-ox) - sin(theta) * (py-oy) + ox
        /// p'y = sin(theta) * (px-ox) + cos(theta) * (py-oy) + oy
        ///
        /// </summary>
        private Vector2 RotatePointAroundTransform(Vector2 p, float angles) {
            return new Vector2(Mathf.Cos((angles) * Mathf.Deg2Rad) * (p.x) - Mathf.Sin((angles) * Mathf.Deg2Rad) * (p.y),
                               Mathf.Sin((angles) * Mathf.Deg2Rad) * (p.x) + Mathf.Cos((angles) * Mathf.Deg2Rad) * (p.y));
        }
        #endregion PRIVATE_METHODS
    }
}
