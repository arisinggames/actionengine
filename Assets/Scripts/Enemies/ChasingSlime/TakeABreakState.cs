﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Enemies.ChasingSlime {
    public class TakeABreakState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the state machine")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// Sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _watchSensorGameObject;

        /// <summary>
        /// Patrol state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _patrolStateGameObject;

        /// <summary>
        /// Chase state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _chaseStateGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// How long takes a break in seconds
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("How long takes a break in seconds")]
        [SerializeField]
        private float _timeBetweenMove = 2;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// The owner's transform of state
        /// </summary>
        private Transform _ot;

        /// <summary>
        /// Watch sensor script
        /// </summary>
        private WatchSensor _watchSensor;

        /// <summary>
        /// Remaining time until the break will be finished and 
        /// the state is changed to patrol
        /// </summary>
        private float _remaining;

        /// <summary>
        /// Reference to the controller
        /// </summary>
        private CharacterController2D _controller;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            t = GameObject.Find("ConsoleState").GetComponent<Text>();
        }

        private Text t;

        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            _remaining = Random.Range(_timeBetweenMove * .75f, _timeBetweenMove * 1.75f);
            //Debug.Log("setze state idle. aktuell: " + t.text);
            t.text = "State: Idle";
            //Debug.Log("setze state idle. neuer text: " + t.text);
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {

            //WatchSensor.Detection status = _watchSensor.CheckPlayerState();
            //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: " + status.ToString();



            //if the watch sense indicates that the player (or any target) 
            // has been spotted deactivate the fov rotation and chase the player
                        if (_watchSensor.IsDetected) {
                Debug.Log("Idle dtected - zu chase");
                //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: " + _watchSensor.IsDetected;

                gameObject.SetActive(false);
                _chaseStateGameObject.SetActive(true);
                return;
            }

            //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: Other";

            //if (status == WatchSensor.Detection.Watching) {
            //                    Debug.Log("Idle dtected - zu chase");

            //                    gameObject.SetActive(false);
            //                    _chaseStateGameObject.SetActive(true);
            //                    return;
            //}


            _remaining -= Time.deltaTime;

            if (_remaining < 0) {
                gameObject.SetActive(false);
                _patrolStateGameObject.SetActive(true);
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Try to enable this state due "certain circumstances"
        /// In this case: if the enemy gets hit by an aggressor
        /// try to enable the idle state, disable the others 
        /// (= patrol state) and change the view direction to the bullets direction
        /// But only do it if the enemy is not already in
        /// chase state
        /// </summary>
        /// <param name="dir">the direction of the aggressor</param>    
        public void TryForceState(Vector3 dir) {
            if (_chaseStateGameObject.activeSelf)
                return;

            // when the damage handler calls TryForceState() it might
            // happen that the Awake() method hasn't been called so far
            // of the state. Therefore check if the components have been
            // cached and if not cache them 
            if (_controller == null)
                CacheComponents();

            EnemyHelper.Stop(_controller);
            _patrolStateGameObject.SetActive(false);

            // the FoV of the enemy shall be turned to the direction
            // the aggressor came from
            Vector3 turnToAggressorDir = new Vector3();
            if (dir.x < _ot.position.x)
                turnToAggressorDir.x = dir.x * -1;
            else
                turnToAggressorDir.x = dir.x;
            if (dir.y < _ot.position.y)
                turnToAggressorDir.y = dir.y * -1;
            else
                turnToAggressorDir.y = dir.y;

            _watchSensor.ViewDirection = turnToAggressorDir;
            _watchSensor.MoveDirection = turnToAggressorDir;

            gameObject.SetActive(true);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _watchSensor = _watchSensorGameObject
                .GetComponent(typeof(WatchSensor)) as WatchSensor;
            
            _controller = _ownerGameObject
                .GetComponent(typeof(CharacterController2D)) as CharacterController2D;

            _ot = _ownerGameObject.transform;
        }
        #endregion PRIVATE_METHODS
    }
}