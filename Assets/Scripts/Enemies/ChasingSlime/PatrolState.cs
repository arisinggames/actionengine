﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Map;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.PathFinder;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace ArisingGames.Assets.Scripts.Enemies.ChasingSlime {
    /// <summary>
    /// Inspired by the book "Unity 4 AI Game Programming"
    /// </summary>
    public class PatrolState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the state machine")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// Take a break state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _takeABreakStateGameObject;

        /// <summary>
        /// Chase state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _chaseStateGameObject;

        /// <summary>
        /// Sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _watchSensorGameObject;

        /// <summary>
        /// The gameobject containing the grid for the pathfinding
        /// </summary>
        [AssertReference]
        [Tooltip("The gameobject containing the grid for the pathfinding")]
        [SerializeField]
        private GameObject _cartesianGridGameObject;

        /// <summary>
        /// Gameobject containing the character boundaries.
        /// Used to clamp the waypoints to the map boundaries
        /// </summary>
        [Tooltip("Gameobject containing the character boundaries. " +
                 "Used to clamp the waypoints to the map boundaries")]
        [AssertReference]
        [SerializeField]
        private GameObject _characterBoundariesGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The kind of movement: keep the object moving at the 
        /// same speed or accelerate it (used for the start of
        /// the movement)
        /// </summary>
        protected enum Movement {
            Accelerate,
            KeepMomentum
        }
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// Movement speed of slime
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("Movement speed of slime")]
        [SerializeField]
        private float _speed = 2;

        /// <summary>
        /// Acceleration
        /// </summary>
        [Tooltip("Acceleration of slime in patrol state")]
        [SerializeField]
        private float _acceleration = 20;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// Watch sensor script
        /// </summary>
        private WatchSensor _watchSensor;

        /// <summary>
        /// The next target the game object is moving to
        /// </summary>
        private Vector3 _targetPos;

        /// <summary>
        /// Boundary configuration in game object "_characterBoundariesGameObject"
        /// </summary>
        private MapBoundaries _boundaries;

        /// <summary>
        /// Reference to owners transform for easier access
        /// </summary>
        private Transform _t;

        /// <summary>
        /// Reference to the controller
        /// </summary>
        private CharacterController2D _controller;

        /// <summary>
        /// The grid for the pathfinding
        /// </summary>
        private CartesianGrid _cartesianGrid;

        /// <summary>
        /// The current path the owner is following
        /// </summary>
        private List<Vector3> _path;

        /// <summary>
        /// The target speed of the object that shall be reached via
        /// EnemyHelper.Accelerate()
        /// </summary>
        private Vector2 _targetSpeed;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            _path = new List<Vector3>();
            t = GameObject.Find("ConsoleState").GetComponent<Text>();
        }

        private Text t;


        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            // before the target will be moved
            // set the next target to adjust the view direction based on the new target
            // _boundaries might be null as OnEnable is called before Start
//            if (_boundaries != null)
                InitNextNode();
            //            Debug.Log("onenable wird auch aufgerufen");
            //Debug.Log("setze state patrol. aktuell: " + t.text);
            t.text = "State: Patrol";
            //Debug.Log("setze state patrol. neuer text: " + t.text);


        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
//            DebugConsole.Log("Hallo ");
//            InitNextNode();

//            GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "Status: Patrol";
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            //WatchSensor.Detection status = _watchSensor.CheckPlayerState();
            //            Debug.Log("Status in Patrol: "+status);
            



            // if the watch sense indicates that the player (or any target) 
            // has been spotted deactivate the fov rotation and chase the player
            if (_watchSensor.IsDetected) {
                //if (status == WatchSensor.Detection.Watching) { 
                //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: IsDetected";

                gameObject.SetActive(false);
                _chaseStateGameObject.SetActive(true);
                return;
            }

            //GameObject.Find("ConsoleStatus").GetComponent<Text>().text = "View: Other";


            if (Vector3.Distance(_targetPos, _t.position) <= 1.0f) {
                EnemyHelper.Stop(_controller);
                gameObject.SetActive(false);
                _takeABreakStateGameObject.SetActive(true);
                return;
            }

            FollowPath();
        }

//        /// <summary>
//        /// OnDrawGizmos event
//        /// </summary>
//        private void OnDrawGizmos() {
//            Gizmos.color = Color.red;
//            Gizmos.DrawWireSphere(_targetPos, .25f);
//        }

        /// <summary>
        /// Draw the path onto the map
        /// </summary>
        public void OnDrawGizmos() {
            if (_path != null)
                for (int i = 0; i < _path.Count; i++) {
                    Gizmos.color = Color.black;
                    Gizmos.DrawCube(_path[i], Vector3.one *.5f);

                    if (i == 0)
                        Gizmos.DrawLine(transform.position, _path[i]);
                    else
                        Gizmos.DrawLine(_path[i - 1], _path[i]);
                }
        }

        #endregion MONO_EVENTS

        #region PRIVATE_METHODS 
        /// <summary>
        /// Walk the waypoints one by one and once one
        /// is reached remove it from the wayoint list
        /// </summary>
        private void FollowPath() {
            if (_path.Count == 0)
                return;

            // if the waypoint has been reached removed it from
            // the waypoint list
            if (Vector3.Distance(_path[0], _t.position) <= .5f) {
//                Debug.Log("Ziel erreicht. " + _path[0]);
                
                _path.RemoveAt(0);

                if (_path.Count > 0)
                    Move(Movement.KeepMomentum);
            }

            if (_path.Count > 0)
                EnemyHelper.Accelerate(_controller, _targetSpeed, _acceleration);
        }

        /// <summary>
        /// Change the movement and view from one waypoint to another
        /// </summary>
        /// <param name="movement"></param>
        private void Move(Movement movement) {
            // _path[0] is the current waypoint
            Vector3 moveDirection = _path[0] - _t.position;

            _watchSensor.ViewDirection = _path[0];
            _watchSensor.MoveDirection = _path[0];

            _targetSpeed = Vector2.zero;
            _targetSpeed.x = moveDirection.normalized.x * _speed;
            _targetSpeed.y = moveDirection.normalized.y * _speed;

//            float x = Mathf.Lerp(_controller.Velocity.x, targetSpeed.x, .7f);
//            float y = Mathf.Lerp(_controller.Velocity.y, targetSpeed.y, .7f);
//            Debug.Log("x " + x);
//            
//            _controller.SetHorizontalForce(x);
//            _controller.SetVerticalForce(y);
//
//                        if (movement == Movement.Accelerate)
//                            EnemyHelper.Accelerate(_controller, targetSpeed, _acceleration);
//                        else 
//                            EnemyHelper.Move(_controller, targetSpeed);
        }

        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = _ownerGameObject.transform;

            _cartesianGrid = _cartesianGridGameObject
                .GetComponent(typeof(CartesianGrid)) as CartesianGrid;

            _controller = _ownerGameObject.GetComponent(typeof(CharacterController2D)) 
                as CharacterController2D;

            _watchSensor = _watchSensorGameObject
                .GetComponent(typeof(WatchSensor)) as WatchSensor;

            _boundaries = _characterBoundariesGameObject.GetComponent(typeof(MapBoundaries))
                as MapBoundaries;
            if (_boundaries == null)
                throw new ComponentNotFoundException("MapBoundaries", _characterBoundariesGameObject);
        }

        /// <summary>
        /// Get next patrol target
        /// </summary>
        private void InitNextPosition() {
            float x = Random.Range(_boundaries.XMin, _boundaries.XMax);
            float y = Random.Range(_boundaries.YMin, _boundaries.YMax);
            _targetPos = new Vector3(x, y);
            _watchSensor.ViewDirection = _targetPos;
            _watchSensor.MoveDirection = _targetPos;
        }

        /// <summary>
        /// Get next patrol target but this time by getting a walkable node
        /// from the CartesianGrid 
        /// </summary>
        private void InitNextNode() {
            
            
            Node randomNode = _cartesianGrid.FindRandomWalkableNode(_t.position);
            _targetPos = randomNode.worldPosition;

//            Debug.Log("InitNextNode für target " + _targetPos);



            //            _watchSensor.ViewDirection = _targetPos;
            //            _watchSensor.MoveDirection = _targetPos;
            PathRequestManager.RequestJumpPointPath(_t.position, _targetPos, JumpPointPathFoundCallback);
        }

        /// <summary>
        /// Method will be called as soon as a path has been found
        /// </summary>
        /// <param name="newPath"></param>
        private void JumpPointPathFoundCallback(Vector3[] newPath) {
            _path.Clear();
            if (newPath.Length > 0)
                for (int i = 0; i < newPath.Length; i++)
                    _path.Add(newPath[i]);
            else
                throw new Exception("No path found!");
//
//            _path.Add(new Vector3(19.5f,6.5f));
//            _path.Add(new Vector3(18.5f,7.5f));
//            _path.Add(new Vector3(15.5f,10.5f));
//            _path.Add(new Vector3(14.5f,11.5f));
//            _path.Add(new Vector3(2.5f,11.5f));

            if (_path.Count > 1)
                _path.RemoveAt(0);

            Move(Movement.Accelerate);
        }
        #endregion PRIVATE_METHODS
    }
}