﻿using ArisingGames.Assets.Scripts.CharacterController;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Enemies {
    public static class EnemyHelper {
        /// <summary>
        /// Move the object using the character controller
        /// </summary>
        public static void Accelerate(CharacterController2D c, Vector2 targetSpeed, float acceleration) {
            float x = Mathf.Lerp(c.Velocity.x, targetSpeed.x, Time.deltaTime * acceleration);
            float y = Mathf.Lerp(c.Velocity.y, targetSpeed.y, Time.deltaTime * acceleration);

            c.SetHorizontalForce(x);
            c.SetVerticalForce(y);
        }

        /// <summary>
        /// Move the object without acceleration. Usefull if the movement
        /// direction will be changed during an existing movement
        /// </summary>
        /// <param name="c"></param>
        /// <param name="targetSpeed"></param>
        public static void Move(CharacterController2D c, Vector2 targetSpeed) {
            float x = targetSpeed.x;
            float y = targetSpeed.y;

            c.SetHorizontalForce(x);
            c.SetVerticalForce(y);
        }

        /// <summary>
        /// Stop the character controller moving the object
        /// </summary>
        public static void Stop(CharacterController2D c) {
            c.SetHorizontalForce(0);
            c.SetVerticalForce(0);
        }
    }
}
