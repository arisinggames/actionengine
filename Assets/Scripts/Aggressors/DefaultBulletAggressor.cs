﻿using ArisingGames.Assets.Scripts.DamageHandlers;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Aggressors {
    /// <summary>
    /// The player weapons bullet differs from the enemys weapon bullet
    /// as the enemys bullet checks if it hits the player.
    /// But the players bullet doesnt check anything - the enemy checks
    /// if it has been hit by a bullet
    /// </summary>
	public class DefaultBulletAggressor : MonoBehaviour, IAggressor {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Bullets speed
        /// </summary>
        [SerializeField]
        [Header("Configuration")]
        [Tooltip("Bullet speed")]
        private float _speed = 4;

        /// <summary>
        /// The id of this aggressor. If not listed add it to the enum in IAggressor
        /// </summary>
        [SerializeField]
        [Tooltip("The id of this aggressor. If not listed add it to the enum in IAggressor")]
        private Aggressors _id;

        /// <summary>
        /// Which layer contains the collisionable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer contains the collisionable objects? " +
                 "Very important: it should include all objects that can cause an " +
                 "explosion as well as destroy the bullet. " +
                 "This includes the enemies layer too")]
        private LayerMask _obstacleMask;

        /// <summary>
        /// Which layer(s) contain hitable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer(s) contain hitable objects?")]
        private LayerMask _hitMask;
        
        /// <summary>
        /// Time til destruction
        /// </summary>
        [SerializeField]
        [Tooltip("time til destruction")]
        private float _liveTime = 1f;

        /// <summary>
        /// The radius of the explosion which would affect other gameobjects
        /// </summary>
        [SerializeField]
        [Tooltip("The radius of the explosion which would affect other " +
                 "gameobjects. 0 = ignore radius")]
        private float _radius;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// 
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        [HideInInspector]
        public Vector3 Direction {
            set { _direction = value; }
        }

        /// <summary>
        /// Retrieve the id of the aggressor
        /// </summary>
        public Aggressors Id {
            get { return _id; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// </summary>
        private Vector3 _direction;

        /// <summary>
        /// Direction from bullet to player
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// The transform of the bullet gameobject
        /// </summary>
        private Transform _t;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();
        }

        /// <summary>
        /// OnEnable event
        /// </summary>
	    void OnEnable() {
	        _moveDirection = (_direction - transform.position).normalized;
            Invoke("Destroy", _liveTime);
        }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            CancelInvoke();
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void Update() {
            _t.Translate(_moveDirection * Time.deltaTime * _speed);
        }

        /// <summary>
        /// OnTriggerEnter2D event
        /// </summary>
        /// <param name="c"></param>
        private void OnTriggerEnter2D(Collider2D c) {
            // check if bullet hits any object on the ObstacleMask
            // or if it hits an object on the hitmask
            // in both cases the bullet gets destroyed and - if
            // the bullet is an explosive one - triggers an explosion
            if (CommonHelper.IsInLayerMask(c.gameObject, _obstacleMask) ||
                CommonHelper.IsInLayerMask(c.gameObject, _hitMask)) {
                // check if the bullet has an explosion radius. 
                // In this case get all targets in this radius
                Collider2D[] colliders;
                if (_radius > 0) 
                    colliders = Physics2D.OverlapCircleAll(_t.position, _radius, _hitMask);
                else
                    colliders = new[] { c };

                for (int i = 0; i < colliders.Length; i++) {
                    IDamageable damageable = colliders[i].GetComponent(typeof(IDamageable)) as IDamageable;
                    if (damageable != null)
                        damageable.Damage(_id, _t.position);
                }

                Destroy();
            }
        }

#if DEBUG
        /// <summary>
        /// Show the radius of the bullet
        /// </summary>
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }
#endif
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;
        }

        /// <summary>
        /// Destroy the bullet
        /// </summary>
        private void Destroy() {
            gameObject.SetActive(false);
        }
        #endregion PRIVATE_METHODS
    }
}