﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.DamageHandlers;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Aggressors {
    /// <summary>
    /// Once the sword attack is triggered 
    /// - disable the sprite of the sword holder
    /// - play an animation to rotate the sword
    /// - with the animation also the collider rotates
    /// - if the collider hits a hit target - Damage() it
    /// - play particle effect
    /// - if during a hit another hit is triggered - restart
    /// </summary>
    public class SwordStrikeAggressor : MonoBehaviour, IAggressor {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the sword strike
        /// </summary>
        [Tooltip("The owner of the sword strike = the sword holder")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// Gameobject containing the slash animation
        /// </summary>
        [Tooltip("Gameobject containing the slash animation")]
        [AssertReference]
        [SerializeField]
        private GameObject _slashGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The states of the sword strike
        /// </summary>
        private enum States {
            Idle,
            Strike,
        }

        /// <summary>
        /// What is the maximum angle to cancel the rotation
        /// and return to the sword holding state?
        /// </summary>
        private float AngleTreshhold = .1f;
        #endregion CONSTANTS

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Lerp time of the sword strike
        /// </summary>
        [SerializeField]
        [Header("Configuration")]
        [Tooltip("Lerp time of the sword strike")]
        private float _lerpTime = .27f;

        /// <summary>
        /// The id of this aggressor. If not listed add it to the enum in IAggressor
        /// </summary>
        [SerializeField]
        [Tooltip("The id of this aggressor. If not listed add it to the enum in IAggressor")]
        private Aggressors _id;

        /// <summary>
        /// Which layer contains the collisionable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer contains the collisionable objects? " +
                    "Very important: it should include all objects that can cause an " +
                    "explosion as well as destroy the bullet. " +
                    "This includes the enemies layer too")]
        private LayerMask _obstacleMask;

        /// <summary>
        /// Which layer(s) contain hitable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer(s) contain hitable objects?")]
        private LayerMask _hitMask;

        /// <summary>
        /// The angle between the sword and the starting vector
        /// </summary>
        [SerializeField]
        [Tooltip("The angle between the sword and the starting vector")]
        private float _startAngle = 45f;

        /// <summary>
        /// The angle between the sword and the target vector
        /// </summary>
        [SerializeField]
        [Tooltip("The angle between the sword and the target vector")]
        private float _endAngle = 15f;

        /// <summary>
        /// Show the gizmos?
        /// </summary>
        [SerializeField]
        [Tooltip("Show the gizmos?")]
        private bool _areGizmosEnabled;

        /// <summary>
        /// The swordstrike happens a bit away from the player
        /// </summary>
        [SerializeField]
        [Tooltip("Sword strike offset")]
        private float _offset = .45f;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// 
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        public Vector3 Direction {
            set { _direction = value; }
        }

        /// <summary>
        /// Retrieve the id of the aggressor
        /// </summary>
        public Aggressors Id {
            get { return _id; }
        }

        /// <summary>
        /// Setter for the callback to reenable the renderer of the boomerang holder
        /// </summary>
        public delegate void ReenableRendererSignature();
        public ReenableRendererSignature ReenableRendererCallback {
            set { _reenableRendererCallback = value; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The owner's transform of the sword strike is the sword holder 
        /// gameobject's transform itself. Required to get the position
        /// of the sword weapon object to return to it
        /// </summary>
        private Transform _ot;

        /// <summary>
        /// The state the boomerang is in.
        /// </summary>
        [SerializeField]
        private States _state = States.Idle;

        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// </summary>
        private Vector3 _direction;

        /// <summary>
        /// Direction from bullet to player
        /// </summary>
        private Vector3 _viewDirection;

        /// <summary>
        /// The transform of the sword strike gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The callback passed by the sword holder
        /// </summary>
        private ReenableRendererSignature _reenableRendererCallback;

        /// <summary>
        /// The starting vector of the rotation
        /// </summary>
        private Vector3 _start;

        /// <summary>
        /// The target vector of the rotation
        /// </summary>
        private Vector3 _end;

        /// <summary>
        /// The starting quaternion of the rotation
        /// </summary>
        private Quaternion _startQuaternion;

        /// <summary>
        /// The starting quaternion of the rotation
        /// </summary>
        private Quaternion _endQuaternion;

        /// <summary>
        /// Cache current lerp time
        /// </summary>
        private float _currentLerpTime;

        /// <summary>
        /// The direction vector of the current rotation
        /// </summary>
        private Vector3 _rotationDirection;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Start has to assign references because at start of the game
        /// the boomerang gameobject is deactivated
        /// </summary>
        private void Start() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            // it's very, very, very important to set the gameobj first as active 
            // simpy by creating the gameobject "SwordStrikeAggressor" from the prefab 
            // "SwordStrikeAggressor" and then to deactivate it in Start() 
            // reason: the "SwordStrikeAggressor" will be hidden at the startup of the game
            // but _before_ it is hidden it must be initialized.
            // otherwise the "SwordHolderWeapon" will reference with _aggressorGameObject
            // an gameobject that exists but that isn't initialized.
            // This way the references in "SwordStrikeAggressor" itself are not accessable
            gameObject.SetActive(false);
        }

        /// <summary>
        /// As at the start of the game the swordstrike is disabled
        /// the transform has to be set in OnEnable
        /// </summary>
        private void OnEnable() {
            if (_t == null)
                _t = transform;
        }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            CancelInvoke();
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            switch (_state) {
                case States.Strike:
                    SetPosition();

                    float t = Lerp();
                    _t.rotation = Quaternion.Lerp(_startQuaternion, _endQuaternion, t);

                    // check if the angle between the current direction vector of 
                    // the sword during the rotation is < 0.1
                    // if so: stop the rotation and return to the sword holder
                    _rotationDirection = _t.rotation * Vector3.right;
                    float angle = Vector3.Angle(_rotationDirection, _end);
                    if (angle < AngleTreshhold) 
                        Cancel();
                    break;
            }
        }

        /// <summary>
        /// OnTriggerEnter2D event
        /// </summary>
        /// <param name="c"></param>
        private void OnTriggerEnter2D(Collider2D c) {
            // check if the sword hits any object on the ObstacleMask or hitmask
            if (CommonHelper.IsInLayerMask(c.gameObject, _obstacleMask) ||
                CommonHelper.IsInLayerMask(c.gameObject, _hitMask)) {
                IDamageable damageable = c.GetComponent(typeof(IDamageable)) as IDamageable;
                if (damageable != null)
                    damageable.Damage(_id, _t.position);

                // note: at this point sword strike does _not_
                // return to the sword holder = "the bullet gets not destroyed"
                // instead the animation (rotation) will be finsihed
            }
        }

        /// <summary>
        /// Show the original direction vector and the left and right side
        /// the sword strike is rotating from - to
        /// </summary>
        private void OnDrawGizmos() {
            if (_areGizmosEnabled) {
                Gizmos.color = Color.blue;
                Gizmos.DrawRay(transform.position, _viewDirection * 3);

                Gizmos.color = Color.black;
                Gizmos.DrawRay(transform.position, _rotationDirection);

                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, _start * 3);
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position, _end * 3);
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// As the sword strike is not retrieved by the pool manager
        /// it has to be "manually" started
        /// </summary>
        public void Strike() {
            // set the state to idle to reset everything, after that execute the strike
            _state = States.Idle;

            SetPosition();

            _currentLerpTime = 0;

            _viewDirection = (_direction - _t.position).normalized;

            _start = CommonHelper.RotateVector(_viewDirection, _startAngle);
            _end = CommonHelper.RotateVector(_viewDirection, -_endAngle);

            _startQuaternion = CommonHelper.Rotate(_start);
            _endQuaternion = CommonHelper.Rotate(_end);

            _state = States.Strike;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _ot = _ownerGameObject.transform;
            if (_ot == null)
                throw new ComponentNotFoundException("transform", _ownerGameObject);
        }

        /// <summary>
        /// Calculate for the sword strike the new position as the 
        /// sword strike happens at an offset of the sword holder
        /// 
        /// The offset is the vector of the direction to the mouse position
        /// clamped to a certain "offset radius" 
        /// </summary>
        private void SetPosition() {
            Vector3 dir = _direction - _ot.position;
            Vector3 pos = _ot.position + Vector3.ClampMagnitude(dir, _offset);
            pos.z = 0;
            _t.position = pos;
        }

        /// <summary>
        /// Cancel the strike and return to holder state
        /// </summary>
        private void Cancel() {
            _state = States.Idle;
            _t.gameObject.SetActive(false);
            _reenableRendererCallback();
        }

        /// <summary>
        /// Calculate the lerp time
        /// See https://chicounity3d.wordpress.com/2014/05/23/how-to-lerp-like-a-pro/
        /// </summary>
        /// <returns></returns>
        private float Lerp() {
            _currentLerpTime += Time.deltaTime;
            if (_currentLerpTime > _lerpTime)
                _currentLerpTime = _lerpTime;

            float t = _currentLerpTime / _lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);
            return t;
        }
        #endregion PRIVATE_METHODS
    }
}