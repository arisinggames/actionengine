﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.DamageHandlers;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Aggressors {
    /// <summary>
    /// Once the boomerang is thrown
    /// - disable the sprite of the boomerang weapon
    /// - play an animation
    /// - return to the player after n seconds
    /// - on the way back to the player ignore obstacles 
    /// - hurt hit targets but don't stop movement back to player
    /// - when the collider hit the player call a callback
    ///     of the owner which 
    /// </summary>
    public class BoomerangThrowAggressor : MonoBehaviour, IAggressor {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The sprite gameobject of the sword weapon
        /// </summary>
        [Tooltip("The sprite gameobject of the sword weapon")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;

        /// <summary>
        /// The owner of the boomerang aggressor
        /// </summary>
        [Tooltip("The owner of the boomerang aggressor = the boomerang holder")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The states of the boomerang
        /// </summary>
        protected enum States {
            Idle,
            Chase,
            Return
        }
        #endregion CONSTANTS

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Bullets speed
        /// </summary>
        [SerializeField]
        [Header("Configuration")]
        [Tooltip("Bullet speed")]
        private float _speed = 4;

        /// <summary>
        /// The id of this aggressor. If not listed add it to the enum in IAggressor
        /// </summary>
        [SerializeField]
        [Tooltip("The id of this aggressor. If not listed add it to the enum in IAggressor")]
        private Aggressors _id;

        /// <summary>
        /// Which layer contains the collisionable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer contains the collisionable objects? " +
                 "Very important: it should include all objects that can cause an " +
                 "explosion as well as destroy the bullet. " +
                 "This includes the enemies layer too")]
        private LayerMask _obstacleMask;

        /// <summary>
        /// Which layer(s) contain hitable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer(s) contain hitable objects?")]
        private LayerMask _hitMask;

        /// <summary>
        /// The layer the player resides on
        /// </summary>
        [SerializeField]
        [Tooltip("The layer the player resides on")]
        private LayerMask _playerMask;
        
        /// <summary>
        /// Time til return
        /// </summary>
        [SerializeField]
        [Tooltip("time til return")]
        private float _liveTime = 1f;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// 
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        public Vector3 Direction {
            set { _direction = value; }
        }

        /// <summary>
        /// Retrieve the id of the aggressor
        /// </summary>
        public Aggressors Id {
            get { return _id; }
        }

        /// <summary>
        /// Setter for the callback to reenable the renderer of the boomerang holder
        /// </summary>
        public delegate void ReenableRendererSignature();
        public ReenableRendererSignature ReenableRendererCallback {
            set { _reenableRendererCallback = value; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The owner's transform of the boomerang aggressor is the boomerang 
        /// gameobject's transform itself. Required to get the position
        /// of the boomerang weapon object to return to it
        /// </summary>
        private Transform _ot;

        /// <summary>
        /// The state the boomerang is in.
        /// </summary>
        [SerializeField]
        private States _state = States.Idle;

        /// <summary>
        /// set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// </summary>
        private Vector3 _direction;

        /// <summary>
        /// direction from bullet to player
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// The transform of the boomerang aggressor gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// 
        /// </summary>
        private ReenableRendererSignature _reenableRendererCallback;

        /// <summary>
        /// The sprite renderer of the sword holder
        /// </summary>
        private SpriteRenderer _renderer;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Start has to assign references because at start of the game
        /// the boomerang gameobject is deactivated
        /// </summary>
        private void Start() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            // it's very, very, very important to set the gameobj first as active 
            // simply by creating the gameobject "BoomerangThrowAggressor" from the prefab 
            // "BoomerangThrowAggressor" and then to deactivate it in Start() 
            // reason: the "BoomerangThrow" will be hidden at the startup of the game
            // but _before_ it is hidden it must be initialized.
            // otherwise the "BoomerangHolderWeapon" will reference with _aggressorGameObject
            // an gameobject that exists but that isn't initialized.
            // This way the references in "BoomerangThrowAggressor" itself are not accessable
            gameObject.SetActive(false);
        }

        /// <summary>
        /// As at the start of the game the boomerang is disabled
        /// the transform has to be set in OnEnable
        /// </summary>
        private void OnEnable() {
            if (_t == null)
                _t = transform;
        }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            CancelInvoke();
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void Update() {
            switch (_state) {
                case States.Chase:
                    _t.Translate(_moveDirection * Time.deltaTime * _speed);
                    break;
                case States.Return:
                    // get the direction on every frame as the player might move
                    Vector3 dir = (_ot.position - _t.position).normalized;
                    _t.Translate(dir * Time.deltaTime * _speed);
                    break;
            }
        }

        /// <summary>
        /// OnTriggerEnter2D event
        /// </summary>
        /// <param name="c"></param>
        private void OnTriggerEnter2D(Collider2D c) {
            // if the boomerang is heading to its target destination 
            // (= chase state) and hits an obstacle or damageable object:
            // try to damage the object and return to player
            if (_state == States.Chase &&
                CommonHelper.IsInLayerMask(c.gameObject, _obstacleMask) ||
                CommonHelper.IsInLayerMask(c.gameObject, _hitMask)) {
                IDamageable damageable = c.GetComponent(typeof(IDamageable)) as IDamageable;
                if (damageable != null)
                    damageable.Damage(_id, _t.position);
                _state = States.Return;
            }

            // if the boomerang has returned to the player deactivate the 
            // boomerang and enable the spritee renderer by executing the
            // callback
            else if (_state == States.Return &&
                CommonHelper.IsInLayerMask(c.gameObject, _playerMask)) {
                _state = States.Idle;
                _t.gameObject.SetActive(false);
                _reenableRendererCallback();
            }

            // if the boomerang is on its way back to the player it will ignore
            // any obstacles but hit damageable objects
            else if (_state == States.Return &&
                CommonHelper.IsInLayerMask(c.gameObject, _hitMask)) {
                IDamageable damageable = c.GetComponent(typeof(IDamageable)) as IDamageable;
                if (damageable != null)
                    damageable.Damage(_id, _t.position);
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// As the boomerang aggressor is not retrieved by the pool manager
        /// it has to be "manually" started
        /// </summary>
        public void Throw() {
            if (_state == States.Idle) {
                _moveDirection = (_direction - _t.position).normalized;

                Invoke("ReturnToSource", _liveTime);
                _state = States.Chase;
            }
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _renderer = _spriteGameObject.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;

            if (_renderer == null)
                throw new ComponentNotFoundException("SpriteRender", this);

            _ot = _ownerGameObject.transform;
        }

        /// <summary>
        /// Return to the player
        /// </summary>
        private void ReturnToSource() {
            _state = States.Return;
        }
        #endregion PRIVATE_METHODS
    }
}