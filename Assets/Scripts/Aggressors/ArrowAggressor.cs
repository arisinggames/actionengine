﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.DamageHandlers;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Aggressors {
    /// <summary>
    /// Arrow takes care of shooting an arrow.
    /// Differs from default bullet in this way that it rotates towards
    /// the target, has different particle effect and maybe in the future
    /// it doesnt disappear after the shot
    /// </summary>
	public class ArrowAggressor : MonoBehaviour, IAggressor {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The sprite gameobject of the sword weapon
        /// </summary>
        [Tooltip("The sprite gameobject of the sword weapon")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Bullets speed
        /// </summary>
        [SerializeField]
        [Header("Configuration")]
        [Tooltip("Bullet speed")]
        private float _speed = 4;

        /// <summary>
        /// The id of this aggressor. If not listed add it to the enum in IAggressor
        /// </summary>
        [SerializeField]
        [Tooltip("The id of this aggressor. If not listed add it to the enum in IAggressor")]
        private Aggressors _id;

        /// <summary>
        /// which layer contains the collisionable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer contains the collisionable objects? " +
                 "Very important: it should include all objects that can cause an " +
                 "explosion as well as destroy the bullet. " +
                 "This includes the enemies layer too")]
        private LayerMask _obstacleMask;

        /// <summary>
        /// which layer(s) contain hitable objects?
        /// </summary>
        [SerializeField]
        [Tooltip("Which layer(s) contain hitable objects?")]
        private LayerMask _hitMask;

        /// <summary>
        /// time til destruction
        /// </summary>
        [SerializeField]
        [Tooltip("time til destruction")]
        private float _liveTime = 1f;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// 
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        [HideInInspector]
        public Vector3 Direction {
            set { _direction = value; }
        }

        /// <summary>
        /// Retrieve the id of the aggressor
        /// </summary>
        public Aggressors Id {
            get { return _id; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// </summary>
        private Vector3 _direction;

        /// <summary>
        /// Direction from bullet to player
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// The transform of the arrow gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The transform of the sprite renderer
        /// </summary>
        private Transform _st;

        /// <summary>
        /// The sprite renderer of the sword holder
        /// </summary>
        private SpriteRenderer _renderer;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// OnEnable event
        /// </summary>
	    void OnEnable() {
            _moveDirection = (_direction - transform.position).normalized;

            // set the rotation of the arrow in the mouse direction
            _st.rotation = CommonHelper.Rotate(_moveDirection);

            Invoke("Destroy", _liveTime);
        }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            CancelInvoke();
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void Update() {
            _t.Translate(_moveDirection * Time.deltaTime * _speed);
        }

        /// <summary>
        /// OnTriggerEnter2D event
        /// </summary>
        /// <param name="c"></param>
        private void OnTriggerEnter2D(Collider2D c) {
            // check if the arrow hits any object on the ObstacleMask or hitmask
            if (CommonHelper.IsInLayerMask(c.gameObject, _obstacleMask) ||
                CommonHelper.IsInLayerMask(c.gameObject, _hitMask)) {
                IDamageable damageable = c.GetComponent(typeof(IDamageable)) as IDamageable;
                if (damageable != null)
                    damageable.Damage(_id, _t.position);

                Destroy();
            }
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _renderer = _spriteGameObject.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;

            if (_renderer == null)
                throw new ComponentNotFoundException("SpriteRender", this);

            _t = transform;
            _st = _renderer.transform;
        }

        /// <summary>
        /// Destroy the arrow
        /// </summary>
        private void Destroy() {
            gameObject.SetActive(false);
        }
        #endregion PRIVATE_METHODS
    }
}