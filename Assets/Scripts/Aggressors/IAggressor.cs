﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Aggressors {
    /// <summary>
    /// All available aggressors the are able to call Damage() of a 
    /// damageable gameobject
    /// </summary>
    public enum Aggressors {
        PlasmaBullet,
        Rocket,
        Slime, 
        Arrow,
        BoomerangThrow,
        SwordStrike
    }

    /// <summary>
    /// IAggressor is more or less a helper for the following purpose:
    /// If an aggressor hits a damageable object it calls the method Damage()
    /// of the damageable object.
    /// The damageable object may have a list of aggressors to check
    /// which damage occures by which aggressor.
    /// There are several possibilities to compare the attacking aggressor 
    /// against an aggressor in the list of aggressors:
    /// The aggressor could have an integer and the aggressor in the list
    /// of aggressors has also an integer field to identify the aggressor.
    /// But that's not a good idea, as if the id changes it has to be touched
    /// in many places.
    /// Same applies in assigning a tag to the aggressor gameobject.
    /// Maybe it is required to create hundreds of gametags and to
    /// identify the  aggressor in the list of aggressors we would have to
    /// use a string (to compare it later against the gameobjects tag). 
    /// This solution is even worse!
    /// 
    /// Therefore an aggressor has to 
    /// - implement the IAggressor interface
    /// - return it's ID via Id 
    /// - on calling Damage() pass itself to the method so that the 
    ///     method can be sure that the aggressor implements the interface
    /// </summary>
    public interface IAggressor {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The id of the aggressor
        /// </summary>
        Aggressors Id {
            get;
        }

        /// <summary>
        /// Set the direction the aggressor is going to,
        /// eg the direction a bullet or arrow is flying to
        /// </summary>
        Vector3 Direction {
            set;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}