﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Enemies.ChasingSlime;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.DamageHandlers {
    /// <summary>
    /// EnemyDamageHandler takes care of the things (especially projectiles from
    /// the player) that harm an enemy
    /// </summary>
	public class EnemyDamageHandler : AbstractDamageHandler, IDamageable {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Take a break state
        /// </summary>
        [Header("More references")]
        [AssertReference]
        [SerializeField]
        private GameObject _takeABreakStateGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The grid used for pathfinding
        /// </summary>
        private TakeABreakState _takeABreakState;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            Init();
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Do the damage to the owner
        /// </summary>
        /// <param name="aggressor">Id of the gameobject that is attacking the object</param>
        /// <param name="dir">the direction of the aggressor</param>    
        public void Damage(Aggressors.Aggressors aggressor, Vector2 dir) {
            // decrement the owners health by the 
            // damage the aggressor is doing
            if (_cachedAggressors.ContainsKey(aggressor)) {
                _takeABreakState.TryForceState(dir);

                Aggressor dp = _cachedAggressors[aggressor];
                _health.Dec(dp.Damage);

                AlterSprite();

                if (dp.Particle != null)
                    SpawnParticle(dp.Particle, dir);

                if (_health.IsDead)
                    Destroy(_ownerGameObject);
            }

#region HACK
            // hack!!!
            // eigentlich muß gecheckt werden, ob der Feind noch lebt
            // also _health > 0 und dann muss der state des Feindes
            // auf knockback gesetzt werden, was ... momentan gar nicht möglich ist
            // weil die states sich untereinander managen
            // einfachste Lösung: der aktuelle state des enemys auf knockback setzen
            // damit alle anderen deaktiviert werden und dann knockout state, welches
            // dem damagetrigger übergeben wird, aktivieren und parameter übergeben
            _countdown = _duration;
            _aggressorDir = dir;

            knockBackDir = (_ownerGameObject.transform.position - _aggressorDir).normalized;
            _knockback = true;

            /////// VERBESSERN!!!!!
            // Pro Name verschiedene knockout parameter festlegen, also acceleration, speed etc.
#endregion HACK

        }




#region HACK
        /////////// >>>>>>>>>>>> HACK HACK HACK
        private Vector3 knockBackDir;
        private Vector3 _aggressorDir;
        private bool _knockback = false;
        // speed knockback
        private float _speed = 4;
        /// the number of seconds of knockback
        private float _duration = .1f;
        /// counter til the dodge stops
        private float _countdown;

        /// <summary>
        ///             >>>>> HACK - nur temporär da um knockback zu simulieren
        // eigentlich muß gecheckt werden, ob der Feind noch lebt
        // also _health > 0 und dann muss der state des Feindes
        // auf knockback gesetzt werden, was ... momentan gar nicht möglich ist
        // weil die states sich untereinander managen
        // einfachste Lösung: der aktuelle state des enemys auf knockback setzen
        // damit alle anderen deaktiviert werden und dann knockout state, welches
        // dem damagetrigger übergeben wird, aktivieren und parameter übergeben
        /// </summary>
        private void Update() {
            // start countdown and move player
            if (_knockback && _countdown > 0) {
                //float x = Mathf.Lerp(sourcePos.x, targetPos.x, Time.deltaTime );
                //float y = Mathf.Lerp(sourcePos.y, targetPos.y, Time.deltaTime );
                //Vector3 v = new Vector3(x,y);
                //_owner.transform.(v);



                _ownerGameObject.transform.Translate(knockBackDir * Time.deltaTime * _speed);
                _countdown -= Time.deltaTime;
            } else
                _knockback = false;
        }

        /////////// >>>>>>>>>>>> HACK HACK HACK
        #endregion HACK

        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        protected void CacheComponents() {
            _health = _healthGameObject
                .GetComponent(typeof(HealthAttribute)) as HealthAttribute;
            _renderer = _rendererGameObject
                .GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            _takeABreakState = _takeABreakStateGameObject
                .GetComponent(typeof(TakeABreakState)) as TakeABreakState;
        }
        #endregion PRIVATE_METHODS
    }
}