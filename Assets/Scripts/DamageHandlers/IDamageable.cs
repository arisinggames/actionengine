﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.DamageHandlers {
    /// <summary>
    /// Damage interface
    /// </summary>
	public interface IDamageable {
        #region PUBLIC_METHODS
        /// <summary>
        /// Do the damage
        /// </summary>
        /// <param name="aggressor">Id of the gameobject that is attacking the object</param>
        /// <param name="dir">the direction of the aggressor</param>
        void Damage(Aggressors.Aggressors aggressor, Vector2 dir);
        #endregion PUBLIC_METHODS
    }	
}