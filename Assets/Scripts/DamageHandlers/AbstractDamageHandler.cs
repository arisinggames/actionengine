﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.PathFinder;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.DamageHandlers {
    /// <summary>
    /// A DamageHandler reacts to incomming "aggressions" like hit by a 
    /// sword, an arrow etc.
    /// Reactions might be decreasing health of the target object,
    /// playing a particle effect etc.
    /// Each target object might react differently to the aggressions:
    /// The flame might be killed by water, but the tree might grow
    /// Therefore the DamageHandler is an abstract class and each
    /// target that wants to react to Damage has to create it's own 
    /// implementation
    /// </summary>
	public abstract class AbstractDamageHandler : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the attribute
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the handler")]
        [AssertReference]
        [SerializeField]
        protected GameObject _ownerGameObject;

        /// <summary>
        /// The sprite renderer of the owner
        /// </summary>
        [Tooltip("The sprite renderer of the owner")]
        [AssertReference]
        [SerializeField]
        protected GameObject _rendererGameObject;

        /// <summary>
        /// The health attribute of the owner
        /// </summary>
        [Tooltip("The health attribute of the owner")]
        [AssertReference]
        [SerializeField]
        protected GameObject _healthGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region STRUCTS
        /// <summary>
        /// An aggressor struct specificies the aggressor (= tag name
        /// of gameobject) and the amount of health it substractes from the 
        /// health attribute as well as the particle effect displayed
        /// on a hit
        /// </summary>
        [Serializable]
        public struct Aggressor {
            public Aggressors.Aggressors AggressorId;
            public float Damage;
            public GameObject Particle;
        }

        /// <summary>
        /// A damage level struct specifies at how much percent of the health
        /// the sprite changes. 
        /// If for example the health drops below 50% of a red circle (the sprite
        /// image of the game object) a half red circle could be displayed.
        /// In this case a DamageLevel{50,"half-red-circle.png"} has to be set
        /// </summary>
        [Serializable]
        public struct DamageLevel {
            public int Percent;
            public Sprite Sprite;
        }
        #endregion STRUCTS

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// As Unity is not capable of displaying a dictionary in the inspector
        /// add a list with structs to the inspector and convert it on
        /// startup to a dictionary for faster lookup
        /// see http://answers.unity3d.com/questions/642431/dictionary-in-inspector.html
        /// 
        /// Aggressor specifies how much an aggressor (tag of an gameobjects, eg a bullet)
        /// hurts the owner of the DamageTrigger
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("DamagePoints specifies how much an aggressor (tag of an gameobjects, " +
                 "eg a bullet) hurts the owner of the DamageTrigger")]
        [SerializeField]
        protected Aggressor[] _aggressors;

        /// <summary>
        /// A DamageLevel specifies at which level of health (percentage)
        /// the sprite of the game object change
        /// </summary>
        [Tooltip("A DamageLevel specifies at which level of health (percentage) " +
                 "the sprite of the game object changes")]
        [SerializeField]
        protected DamageLevel[] _damageLevels;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// Sprite renderer
        /// </summary>
        protected SpriteRenderer _renderer;

        /// <summary>
        /// Health script
        /// </summary>
        protected HealthAttribute _health;

        /// <summary>
        /// _aggressors converted to a dictionary for faster lookup
        /// </summary>
        protected Dictionary<Aggressors.Aggressors, Aggressor> _cachedAggressors;

        /// <summary>
        /// _damageLevels converted to a dictionary for faster lookup
        /// </summary>
        protected Dictionary<int, Sprite> _cachedDamageLevels;
        #endregion PRIVATE_FIELDS

        #region PRIVATE_METHODS
        /// <summary>
        /// Common init for all damage handlers
        /// </summary>
        protected void Init() {
            _cachedAggressors = CacheAggressors(_aggressors);
            _cachedDamageLevels = CacheDamageLevels(_damageLevels);
        }

        /// <summary>
        /// show the particle effect for the aggressor
        /// </summary>
        /// <param name="p">particle to spawn</param>   
        /// <param name="pos">position where to init the particle</param>   
        protected void SpawnParticle(GameObject p, Vector3 pos) {
            GameObject init = (GameObject)Instantiate(p, pos, Quaternion.identity);
            ParticleSystem ps = init.GetComponent<ParticleSystem>();
            ps.Play();
            Destroy(init, ps.startLifetime);
        }

        /// <summary>
        /// change the sprite based on the damage level
        /// </summary>
        protected void AlterSprite() {
            if (_cachedDamageLevels.Count > 0) {
                // iterate through the damage levels
                // if the current health percentage is below 
                // a damagelevel use the new sprite unless
                // the damage level is below the next damage level
                Sprite sprite = null;
                foreach (var pair in _damageLevels) {
                    int key = pair.Percent;
                    Sprite value = pair.Sprite;
                    
                    // leave the loop as the next level is smaller
                    // then the current health percentage
                    if (_health.Percentage > key)
                        break;

                    if (_health.Percentage < key) {
                        // set sprite
                        sprite = value;
                    }
                }

                if (sprite != null)
                    _renderer.sprite = sprite;
            }
        }

        /// <summary>
        /// Convert _aggressors to _cachedAggressors for faster lookup
        /// </summary>
        /// <returns></returns>
        private Dictionary<Aggressors.Aggressors, Aggressor> CacheAggressors(Aggressor[] aggressors) {
            Dictionary<Aggressors.Aggressors, Aggressor> points = new Dictionary<Aggressors.Aggressors, Aggressor>();

            for (int i = 0; i < aggressors.Length; i++) {
                Aggressor a = aggressors[i];
                points[a.AggressorId] = a;
            }

            return points;
        }

        /// <summary>
        /// Convert _damagePoint to _cachedAggressors for faster lookup
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, Sprite> CacheDamageLevels(DamageLevel[] damageLevels) {
            Dictionary<int, Sprite> points = new Dictionary<int, Sprite>();

            for (int i = 0; i < damageLevels.Length; i++) {
                DamageLevel dl= damageLevels[i];
                points[dl.Percent] = dl.Sprite;
            }

            return points;
        }
		#endregion PRIVATE_METHODS
	}	
}