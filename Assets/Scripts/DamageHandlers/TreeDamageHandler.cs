﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.PathFinder;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.DamageHandlers {
    /// <summary>
    /// TreeDamageHandler takes care of the things (especially projectiles from
    /// the player) that harm a tree
    /// </summary>
	public class TreeDamageHandler : AbstractDamageHandler, IDamageable {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The cartesion grid game object to rebuild the 
        /// grid if an object has been destroyed (tree, house, barrel...)
        /// </summary>
        [Header("More references")]
        [Tooltip("The cartesion grid game object to rebuild the grid " +
                 "if an object has been destroyed (tree, house, barrel...)")]
        [AssertReference]
        [SerializeField]
        protected GameObject _cartesianGridGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The grid used for pathfinding
        /// </summary>
        private CartesianGrid _cartesianGrid;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            Init();
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Do the damage to the owner
        /// </summary>
        /// <param name="aggressor">Id of the gameobject that is attacking the object</param>
        /// <param name="dir">the direction of the aggressor</param>    
        public void Damage(Aggressors.Aggressors aggressor, Vector2 dir) {
            // decrement the owners health by the 
            // damage the aggressor is doing
            if (_cachedAggressors.ContainsKey(aggressor)) {
                Aggressor dp = _cachedAggressors[aggressor];
                _health.Dec(dp.Damage);

                AlterSprite();

                if (dp.Particle != null)
                    SpawnParticle(dp.Particle, dir);

                if (_health.IsDead) {
                    Destroy(_ownerGameObject);
                    _cartesianGrid.CreateGrid();
                    _cartesianGrid.DrawGrid();
                }
            }
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        protected void CacheComponents() {
            _health = _healthGameObject
                .GetComponent(typeof(HealthAttribute)) as HealthAttribute;
            _renderer = _rendererGameObject
                .GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            _cartesianGrid = _cartesianGridGameObject
                .GetComponent(typeof(CartesianGrid)) as CartesianGrid;
        }
        #endregion PRIVATE_METHODS
    }
}