﻿namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// The weapon manager
    /// </summary>
	public interface IWeaponManager {
        #region PUBLIC_METHODS
        /// <summary>
        /// Assigns a new weapon
        /// 
        /// To specify the id: supply the weapons class name with WeaponId 
        /// for example ChangeWeapon(PlayerWeaponBlue.WeaponId)
        /// </summary>
        /// <param name="id"></param>
        void ChangeWeapon(WeaponTypes id);
        #endregion PUBLIC_METHODS
    }
}