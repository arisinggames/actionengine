﻿using ArisingGames.Assets.Scripts.Aggressors;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// Abstract weapon
    /// </summary>
	public abstract class AbstractWeapon : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the weapon - required for pushback
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the weapon - required for pushback")]
        [AssertReference]
        [SerializeField]
        protected GameObject _ownerGameObject;

        /// <summary>
        /// The camera
        /// </summary>
        [Tooltip("The camera")]
        [AssertReference]
        [SerializeField]
        protected GameObject _cameraGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The states of the weapon
        /// </summary>
        protected enum States {
            Idle,
            Fire,
            Upgrade,
            Reload
        }
        #endregion CONSTANTS

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Eg. used to change the weapon but also for supplying an ID when building the weapon list 
        /// </summary>
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("Eg. used to change the weapon but also for supplying an " +
                 "ID when building the weapon list")]
        protected WeaponTypes _id;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The manager is used in the weapons eg. to raise 
        /// the ammo consumed event
        /// </summary>
        public WeaponManager Manager {
            set { _manager = value; }
        }

        /// <summary>
        /// How huge is the delay between each shot?
        /// DelayBetweenShots must be a property and
        /// overwritten to enable the the method Shoot()
        /// to access the field _timeBetweenShot set
        /// via the inspector
        /// </summary>
        public abstract float DelayBetweenShots {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The state the weapon is in.
        /// </summary>
        protected States _state;

        /// <summary>
        /// Cache owners transform
        /// </summary>
        protected Transform _ownerTransform;

        /// <summary>
        /// Camera component
        /// </summary>
        protected UnityEngine.Camera _c;

        /// <summary>
        /// The manager is used in the weapons eg. to raise 
        /// the ammo consumed event
        /// </summary>
        protected WeaponManager _manager;

        /// <summary>
        /// The bullet pool script
        /// </summary>
        protected Pool _bulletPool;

        /// <summary>
        /// Remaining time until the next shot is triggerable
        /// </summary>
        protected float _remaining;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected virtual void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            _state = States.Idle;
        }

        /// <summary>
        /// Update: consider two states (in theory) that are running 
        /// 'til the state ends
        /// </summary>
        protected virtual void Update() {
            _remaining -= Time.deltaTime;

            // if the weapon is in idle state it could play an animation
            // or leave some dirt on the floor
            switch (_state) {
                case States.Idle: // do something in idle state
                    break;
                case States.Reload: // do something in reload state
                    break;
            }
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _c = _cameraGameObject
                .GetComponent(typeof(UnityEngine.Camera)) as UnityEngine.Camera;
            _ownerTransform = _ownerGameObject.transform;
        }

        /// <summary>
        /// Init a bullet in target direction
        /// </summary>
        protected virtual void Shoot() {
            if (_remaining > 0)
                return;
            _remaining = DelayBetweenShots;

            GameObject aggressorGameObject = _bulletPool.Get();

            if (aggressorGameObject == null)
                return;

            aggressorGameObject.transform.position = _ownerTransform.position;
            aggressorGameObject.transform.rotation = _ownerTransform.rotation;

            IAggressor aggressor = aggressorGameObject
                .GetComponent(typeof(IAggressor)) as IAggressor;
            if (aggressor == null)
                throw new ComponentNotFoundException("IAggressor", aggressorGameObject);

            Vector3 target = _c.ScreenToWorldPoint(Input.mousePosition);
            target.z = 0;
            aggressor.Direction = target;
            aggressorGameObject.SetActive(true);
        }
        #endregion PRIVATE_METHODS
    }
}