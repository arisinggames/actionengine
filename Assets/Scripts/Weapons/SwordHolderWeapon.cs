﻿using ArisingGames.Assets.Scripts.Aggressors;
using ArisingGames.Assets.Scripts.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// The sword holder which has as aggressor (="bullet") a sword
    /// </summary>
    public class SwordHolderWeapon : AbstractWeapon, IWeapon {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The sword aggressor gameobject
        /// </summary>
        [Header("More references")]
        [Tooltip("The sword aggressor gameobject")]
        [AssertReference]
        [SerializeField]
        private GameObject _aggressorGameObject;

        /// <summary>
        /// The sprite gameobject of the sword weapon
        /// </summary>
        [Tooltip("The sprite gameobject of the sword weapon")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        [Space(3)]
        [Header("More configuration")]
        [Tooltip("How huge is the delay between each shot?")]
        [SerializeField]
        private float _delayBetweenShots = 3;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Retrieve the id of the weapon
        /// </summary>
        public WeaponTypes Id {
            get { return _id; }
        }

        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        public override float DelayBetweenShots {
            get { return _delayBetweenShots; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// SwordHolder aggressor script of _aggressorGameObject object,
        /// though it is implementing the IAggressor Interface "_aggressor" 
        /// references the sword script itself as it is accessing
        /// fields and methods in the Bommerang script not available
        /// in IAggressor
        /// </summary>
        private SwordStrikeAggressor _aggressor;

        /// <summary>
        /// The transform of the sword holder gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The sprite renderer of the sword holder
        /// </summary>
        private SpriteRenderer _renderer;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected override void Awake() {
            base.Awake();
            CacheComponents();

            SwordStrikeAggressor.ReenableRendererSignature callback = ReenableRendererCallback;
            _aggressor.ReenableRendererCallback = callback;
            _aggressor.transform.position = _t.position;
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Execute the attack
        /// Fires the bullet, pushes the player back, switches to reload state
        /// </summary>
        public void Fire() {
            _state = States.Fire;

            // shoot bullet in mouse direction
            Shoot();
        }

        /// <summary>
        /// In reload an animation could be played, a recover state (= reload state) could be set which 
        /// reloads the energy of the weapon and blocks firing the wepaon
        /// then it swtiches back to the idle state
        /// </summary>
        public void Reload() {
            _state = States.Reload;
        }

        /// <summary>
        /// Might be used in the future to upgrade a weapon
        /// </summary>
        public void Upgrade() {
            _state = States.Upgrade;
        }

        /// <summary>
        /// ReenableRendererCallback is used in swordHolder
        /// as callback to reenable the renderer
        /// </summary>
        public void ReenableRendererCallback() {
            _renderer.enabled = true;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;

            _renderer = _spriteGameObject
                .GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;

            _aggressor = _aggressorGameObject
                .GetComponent(typeof(SwordStrikeAggressor)) as SwordStrikeAggressor;
        }

        /// <summary>
        /// Init the sword aggressor in the mouse direction &
        /// disable the sprite renderer of the weapon 
        /// </summary>
        protected override void Shoot() {
            if (_remaining > 0)
                return;
            _remaining = DelayBetweenShots;

            _renderer.enabled = false;

            Vector3 target = _c.ScreenToWorldPoint(Input.mousePosition);
            target.z = 0;

            _aggressorGameObject.transform.rotation = _t.rotation;

            _aggressor.Direction = target;

            _aggressorGameObject.SetActive(true);
            _aggressor.Strike();
        }
        #endregion PRIVATE_METHODS
    }
}