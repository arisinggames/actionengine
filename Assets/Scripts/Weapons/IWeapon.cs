﻿namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// All available weapons
    /// </summary>
    public enum WeaponTypes {
        None,
        PlasmaGun,
        RocketLauncher,
        SlimeGun,
        Bow,
        Boomerang,
        Sword
    }

    /// <summary>
    /// Weapon interface 
    /// </summary>
	public interface IWeapon {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The id of the weapon. Must be a unique value compared 
        /// to other weapons.
        /// 
        /// It's used to identfiy the weapon by their classname.ID 
        /// 
        /// In theory it would be possible to use for each weapon a constant
        /// and identify the weapon by this constant but the drawback is
        /// that an interface cannot force its implementing class
        /// to implement a constant
        /// </summary>
        WeaponTypes Id {
            get;
        }

        /// <summary>
        /// The manager is used in the weapons eg. to raise 
        /// the ammo consumed event
        /// </summary>
        WeaponManager Manager {
            set;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PUBLIC_METHODS
        /// <summary>
        /// Wxecute the attack for the current weapon
        /// </summary>
        void Fire();

        /// <summary>
        /// Upgrade a weapon
        /// </summary>
        void Upgrade();

        /// <summary>
        /// Maybe used in the future
        /// </summary>
        void Reload();
        #endregion PUBLIC_METHODS
    }	
}