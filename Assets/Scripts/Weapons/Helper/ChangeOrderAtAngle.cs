﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons.Helper {
    /// <summary>
    /// To simulate the weapon holder appearance of nuclear throne
    /// change order in layer of the sprite renderer depending on the
    /// angle of the object
    /// </summary>
	public class ChangeOrderAtAngle : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The sprite gameobject of the sword weapon
        /// </summary>
        [Header("References")]
        [Tooltip("The sprite gameobject of the sword weapon")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Target order in the layer
        /// </summary>
        [SerializeField]
        [Header("Configuration")]
        [Tooltip("Target order in the layer")]
        private int _newOrder = -1;

        /// <summary>
        /// The min angle of the sprite to change the order
        /// </summary>
        [SerializeField]
        [Tooltip("The min angle of the sprite to change the order")]
        private float _min = 0;

        /// <summary>
        /// The max angle of the sprite to change the order
        /// </summary>
        [SerializeField]
        [Tooltip("The max angle of the sprite to change the order")]
        private float _max = 180;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// Cache the old order to restore
        /// </summary>
        private int _backup;

        /// <summary>
        /// Cache sprite transform
        /// </summary>
        private Transform _st;

        /// <summary>
        /// The sprite renderer of the sword holder
        /// </summary>
        private SpriteRenderer _renderer;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            _backup = _renderer.sortingOrder;
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void LateUpdate() {
            if (_st.rotation.z >= _min && _st.rotation.z <= _max)
                _renderer.sortingOrder = _newOrder;
            else
                _renderer.sortingOrder = _backup;
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _st = _spriteGameObject.transform;

            _renderer = _spriteGameObject
                .GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (_renderer == null)
                throw new ComponentNotFoundException("SpriteRenderer", _spriteGameObject);
        }
        #endregion PRIVATE_METHODS
    }
}