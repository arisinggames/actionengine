﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons.Helper {
    /// <summary>
    /// Rotate the object the script is attached to with the mouse position
    /// </summary>
	public class LookAtMouse : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The object to rotate 
        /// </summary>
        [Header("References")]
        [Tooltip("The gameobject to rotate")]
        [AssertReference]
        [SerializeField]
        private GameObject _targetGameObject;

        /// <summary>
        /// The camera
        /// </summary>
        [Tooltip("The camera")]
        [AssertReference]
        [SerializeField]
        private GameObject _cameraGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Cache targets transform
        /// </summary>
        private Transform _t;

        /// <summary>
        /// Camera component
        /// </summary>
        private UnityEngine.Camera _c;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event        
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void Update() {
            Vector3 mouse = _c.ScreenToWorldPoint(Input.mousePosition);
            Vector3 dir = mouse - _t.position;
            _t.rotation = CommonHelper.Rotate(dir);
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _c = _cameraGameObject.GetComponent<UnityEngine.Camera>();
            _t = _targetGameObject.transform;
        }
        #endregion PRIVATE_METHODS
    }
}