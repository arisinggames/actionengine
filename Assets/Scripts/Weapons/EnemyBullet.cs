﻿using System;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
	public class EnemyBullet : MonoBehaviour {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Bullets speed
        /// </summary>
        [Header("Configuration")]
        [Tooltip("Bullet speed")]
        [SerializeField]
        private float _speed = 4;

        /// <summary>
        /// The damage the bullet is causing
        /// </summary>
        [Tooltip("The damage the bullet is causing")]
        [SerializeField]
        private float _damage = 1;

        /// <summary>
        /// Which layer contains the collisionable objects?
        /// </summary>
        [Tooltip("Which layer contains the collisionable objects?")]
        [SerializeField]
        private LayerMask _obstacleMask;

        /// <summary>
        /// Time til destruction
        /// </summary>
        [Tooltip("Time til destruction")]
        [SerializeField]
        private float _liveTime = 1f;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The health attribute of the player to decrease health
        /// </summary>
        public HealthAttribute HealthAttribute {
	        set { _healthAttribute = value; }
	    }

	    /// <summary>
	    /// Inspired by "Unity 4 Game Programming AI": the target this
	    /// game object is validating against.
	    /// In this case: The target that must be hit so it gets destroyed
	    /// Field is hidden as it is not set in the inspector but in the bullets'
	    /// initiator
	    /// </summary>
	    public GameObject HitTarget {
	        set { _hitTarget = value; }
	    }

        /// <summary>
        /// Position of the target
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        public Vector3 TargetPos {
            set { _targetPos = value; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Inspired by "Unity 4 Game Programming AI": the target this
        /// game object is validating against.
        /// In this case: The target that must be hit so it gets destroyed
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        private GameObject _hitTarget;

        /// <summary>
        /// Position of the target
        /// Field is hidden as it is not set in the inspector but in the bullets'
        /// initiator
        /// </summary>
        private Vector3 _targetPos;

        /// <summary>
        /// Direction from bullet to player
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// The script health attribute of the gameobject
        /// </summary>
	    private HealthAttribute _healthAttribute;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
	        _moveDirection = (_targetPos - transform.position).normalized;
	        Invoke("Destroy", _liveTime);
	    }

        /// <summary>
        /// Destroy event
        /// </summary>
	    private void Destroy() {
	        gameObject.SetActive(false);
	    }

        /// <summary>
        /// OnDisable event
        /// </summary>
        private void OnDisable() {
            CancelInvoke();
        }

        /// <summary>
        /// Update event
        /// </summary>
		private void Update() {
            transform.Translate(_moveDirection * Time.deltaTime * _speed);
        }

        /// <summary>
        /// OnTriggerEnter2D event
        /// </summary>
        /// <param name="c"></param>
        private void OnTriggerEnter2D(Collider2D c) {
            if (_hitTarget == null)
                throw new Exception("Check the references. _hitTarget is not supplied for EnemyBullet");

            if (c.gameObject == _hitTarget) {
                _healthAttribute.Dec(_damage);
                Destroy();
            }

            // check if bullet hits any object on the ObstacleMask
            if ((1 << c.gameObject.layer) == _obstacleMask.value)
                Destroy();
        }
        #endregion MONO_EVENTS
    }
}