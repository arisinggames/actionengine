﻿using ArisingGames.Assets.Scripts.Aggressors;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// Bow with arrows
    /// </summary>
    public class BowWeapon : AbstractWeapon, IWeapon, IAmmoConsumer {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Gameobject containing the bullet object pool for this weapon
        /// </summary>
        [Tooltip("Gameobject containing the bullet object pool for this weapon")]
        [AssertReference]
        [SerializeField]
        protected GameObject _bulletPoolGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The consumed ammo per shhot
        /// </summary>
        private const int ConsumedAmmo = 1;

        /// <summary>
        /// The ammo at the collection of the weapon
        /// </summary>
        private const int AmmoStartSize = 10;
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        [Space(3)]
        [Header("More configuration")]
        [Tooltip("How huge is the delay between each shot?")]
        [SerializeField]
        private float _delayBetweenShots = 3;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Retrieve the id of the weapon
        /// </summary>
        public WeaponTypes Id {
            get { return _id; }
        }

        /// <summary>
        /// Return the remaining ammo - if supported by the weapon
        /// </summary>
        public int RemainingAmmo {
            get { return _remainingAmmo; }
        }

        /// <summary>
        /// Return the ammo type the weapon is using
        /// </summary>
        public AmmoTypes AmmoType {
            get { return _ammoType; }
        }

        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        public override float DelayBetweenShots {
            get { return _delayBetweenShots; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The number of arrows
        /// </summary>
        [SerializeField]
        private int _remainingAmmo;

        /// <summary>
        /// The ammo type
        /// </summary>
        private AmmoTypes _ammoType;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected override void Awake() {
            _remainingAmmo = AmmoStartSize;
            _ammoType = AmmoTypes.Arrow;

            base.Awake();
            CacheComponents();
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Execute the attack
        /// Fires the bullet, pushes the player back, switches to reload state
        /// </summary>
        public void Fire() {
            if (_remaining > 0)
                return;

            // only fire if the weapon has ammo
            if (_remainingAmmo <= 0)
                return;

            Dec(ConsumedAmmo);

            _state = States.Fire;

            // shoot bullet in mouse direction
            Shoot();

            // switch to reload so the weapon can cool down
            _state = States.Reload;
        }

        /// <summary>
        /// In reload an animation could be played, a recover state (= reload state) could be set which 
        /// reloads the energy of the weapon and blocks firing the wepaon
        /// then it swtiches back to the idle state
        /// </summary>
        public void Reload() {
            _state = States.Reload;
        }

        /// <summary>
        /// Might be used in the future to upgrade a weapon
        /// </summary>
        public void Upgrade() {
            _state = States.Upgrade;
        }

        /// <summary>
        /// Increment the ammo
        /// </summary>
        /// <param name="size"></param>
        public void Inc(int size) {
            _remainingAmmo += size;
        }

        /// <summary>
        /// Decrement the ammo
        /// </summary>
        /// <param name="size"></param>
        public void Dec(int size) {
            _remainingAmmo -= size;
            if (_remainingAmmo < 0)
                _remainingAmmo = 0;

            _manager.OnAmmoConsumed(AmmoTypes.Arrow, WeaponTypes.Bow, _remainingAmmo);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _bulletPool = _bulletPoolGameObject.GetComponent(typeof(Pool)) as Pool;
        }
        #endregion PRIVATE_METHODS
    }
}