﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// A weapon with blue bullets including a "placeholder" in Update()
    /// for 2 states
    /// 
    /// StateMachine is very simple and inspired by 
    /// https://www.youtube.com/watch?v=lo80BOomaU4
    /// </summary>
	public class PlasmaGunWeapon : AbstractWeapon, IWeapon {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Gameobject containing the bullet object pool for this weapon
        /// </summary>
        [Tooltip("Gameobject containing the bullet object pool for this weapon")]
        [AssertReference]
        [SerializeField]
        protected GameObject _bulletPoolGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        [Space(3)]
        [Header("More configuration")]
        [Tooltip("How huge is the delay between each shot?")]
        [SerializeField]
        private float _delayBetweenShots = 3;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Retrieve the id of the weapon
        /// </summary>
        public WeaponTypes Id {
            get { return _id; }
        }

        /// <summary>
        /// How huge is the delay between each shot?
        /// </summary>
        public override float DelayBetweenShots {
            get { return _delayBetweenShots; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected override void Awake() {
            base.Awake();
            CacheComponents();
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Execute the attack
        /// Fires the bullet, pushes the player back, switches to reload state
        /// </summary>
        public void Fire() {
            _state = States.Fire;

            // shoot bullet in mouse direction
            Shoot();

            // switch to reload so the weapon can cool down
            _state = States.Reload;
        }

        /// <summary>
        /// In reload an animation could be played, a recover state 
        /// (= reload state) could be set which 
        /// reloads the energy of the weapon and blocks firing the wepaon
        /// then it swtiches back to the idle state
        /// </summary>
        public void Reload() {
            _state = States.Reload;
        }

        /// <summary>
        /// Might be used in the future to upgrade a weapon
        /// </summary>
        public void Upgrade() {
            _state = States.Upgrade;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _bulletPool = _bulletPoolGameObject
                .GetComponent(typeof(Pool)) as Pool;
        }
        #endregion PRIVATE_METHODS
    }
}