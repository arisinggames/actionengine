﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// About the weapons and the weapon manager:
    /// - Each weapon is a statemachine on its own with states like 
    /// idle, reload, uprade, fire
    /// - Each weapon has it's own game object. 
    ///     - easier for debugging (values displayed in the inspector)
    ///     - values alterable using the inspector
    ///     - as the weapon script inherits from mono behaviour the
    ///     weapon has it's own life meaning: it doen not only react on triggers
    ///     but could display animations in the idle state for exmaple or recover
    ///     energy over the time
    /// - A weapon might have an array with different ammo types 
    /// - The weapon manager itself resides as component on the game object 
    /// "weapons". The manager makes sense as the weapons don't have 
    /// a relation between each other like the states of the player
    /// or the enemies have. 
    /// The manager also offers a common interface to all weapons.
    /// The manager populates on start a weapons array with all weapons,
    /// sets the as active marked weapon as current weapon and controlls
    /// the component of the current weapon that implements IWeapon
    /// </summary>
    public class WeaponManager : MonoBehaviour {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: ammo haas been consumed
        /// </summary>
        public delegate void AmmoConsumedHandler(object sender, AmmoConsumedEventArgs e);
        public static event AmmoConsumedHandler Consumed;
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The current weapon, set weapon from inspector as starting weapon
        /// </summary>
        private IWeapon _current;

        /// <summary>
        /// All weapons = dictionary with the weapon.id and the 
        /// weapon gameobject
        /// </summary>
        private Dictionary<WeaponTypes, GameObject> _weapons;

        /// <summary>
        /// Cache all weapons that consume ammo
        /// </summary>
        private Dictionary<WeaponTypes, IAmmoConsumer> _ammoConsumers;

        /// <summary>
        /// The transform of the weapon manager gameobject
        /// </summary>
        private Transform _t;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();

            _weapons = new Dictionary<WeaponTypes, GameObject>();
            PopulateWeapons();

            _ammoConsumers = new Dictionary<WeaponTypes, IAmmoConsumer>();
            PopulateAmmoConsumers();

            InitEventListeners();
        }

        /// <summary>
        /// Update event: executes attack
        /// </summary>
        private void Update() {
            if (Input.GetButtonDown("Fire1") && 
                _current != null)
                _current.Fire();

            // just for testing: assign 3 weapons to hotkeys
            if (Input.GetKeyDown("1") )
                ChangeWeapon(WeaponTypes.PlasmaGun);
            if (Input.GetKeyDown("2"))
                ChangeWeapon(WeaponTypes.SlimeGun);
            if (Input.GetKeyDown("3"))
                ChangeWeapon(WeaponTypes.RocketLauncher);
            if (Input.GetKeyDown("4"))
                ChangeWeapon(WeaponTypes.Bow);
            if (Input.GetKeyDown("5"))
                ChangeWeapon(WeaponTypes.Boomerang);
            if (Input.GetKeyDown("6"))
                ChangeWeapon(WeaponTypes.Sword);
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Assigns a new weapon
        /// 
        /// To specify the id: supply the weapons class name with WeaponId 
        /// for example ChangeWeapon(PlayerWeaponBlue.WeaponId)
        /// </summary>
        /// <param name="id"></param>
        public void ChangeWeapon(WeaponTypes id) {
            // if a weapon is activated deactivate it
            if (_current != null)
                _weapons[_current.Id].SetActive(false);

            _weapons[id].SetActive(true);
            _current = _weapons[id].GetComponent(typeof(IWeapon)) as IWeapon;
        }

        /// <summary>
        /// Inc the ammo of the weapon
        /// </summary>
        /// <param name="id"></param>
        /// <param name="size"></param>
        public void Inc(WeaponTypes id, int size) {
            // get the weapon and ensure that it implements the interface
            IAmmoConsumer c = _ammoConsumers[id];
            if (c == null)
                throw new Exception("The weapon \""+id+"\" does not implement IAmmoConsumer"); 

            c.Inc(size);
            OnAmmoConsumed(c.AmmoType, id, c.RemainingAmmo);
        }

        /// <summary>
        /// Get the remaining ammo of a weapon
        /// </summary>
        /// <param name="id"></param>
        public int GetRemainingAmmo(WeaponTypes id) {
            // get the weapon and ensure that it implements the interface
            IAmmoConsumer c = _ammoConsumers[id];
            if (c == null)
                throw new Exception("The weapon \"" + id + "\" does not implement IAmmoConsumer");

            return c.RemainingAmmo;
        }

        /// <summary>
        /// Raise the event
        /// </summary>
        public void OnAmmoConsumed(AmmoTypes type, WeaponTypes weaponType, int size) {
            AmmoConsumedEventArgs args = new AmmoConsumedEventArgs(type, weaponType, size);
            // Check if there are any Subscribers
            if (Consumed != null)
                Consumed(this, args);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;
        }

        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            AmmoCollectable.Collected += (sender, e) => {
                IAmmoCollectable c = sender as IAmmoCollectable;
                if (c != null)
                    Inc(c.WeaponType, c.Size);
            };
        }

        /// <summary>
        /// Populate the weapons list
        /// </summary>
        private void PopulateWeapons() {
            int children = _t.childCount;

            for (int i = 0; i < children; ++i) {
                Transform c = _t.GetChild(i);
                IWeapon weapon = c.GetComponent(typeof(IWeapon)) as IWeapon;
                if (weapon == null) {
                    Debug.Log("Error: no IWeapon component found for " + c.name);
                    continue;
                }

                if (_weapons.ContainsKey(weapon.Id)) 
                    throw new Exception("The weapong key " + weapon.Id + " is assigned at least twice");

                // assign the weapon the weapon manager
                // so the weapon is able to raise the event "Consumed"
                weapon.Manager = this;
                _weapons.Add(weapon.Id, c.gameObject);

                // if the game object is set as active set it as starting weapon
                if (c.gameObject.activeSelf && _current == null) 
                    _current = weapon;
                else if (c.gameObject.activeSelf && _current != null)
                    Debug.Log("Warning: starting weapon already set but " + c.name + " also set as active");
            }

            if (_current == null)
                Debug.Log("Warning: No starting weapon has been set. All weapons marked as inactive");

#if DEBUG
            //// as dictionaries are not serialized and therefore not displayed in the
            //// inspector quickly output _weapons to the console
            //Debug.Log("Weapons:");
            //foreach (WeaponTypes key in _weapons.Keys) {
            //    IWeapon weapon = _weapons[key].GetComponent(typeof(IWeapon)) as IWeapon;
            //    Debug.Log(weapon);
            //}
#endif
        }

        /// <summary>
        /// Populate the consumer list
        /// </summary>
        private void PopulateAmmoConsumers() {
            foreach (var pair in _weapons) {
                // get the component and check if it implements IAmmoConsumer
                IAmmoConsumer ac = pair.Value.GetComponent(typeof(IAmmoConsumer)) as IAmmoConsumer;
                if (ac != null)
                    _ammoConsumers.Add(pair.Key, ac);
            }
        }
        #endregion
    }
}