﻿using ArisingGames.Assets.Scripts.Aggressors;
using ArisingGames.Assets.Scripts.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// The boomerang which has as aggressor (="bullet") a boomerang
    /// </summary>
    public class BoomerangHolderWeapon : AbstractWeapon, IWeapon {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The boomerang aggressor gameobject
        /// </summary>
        [Header("More references")]
        [Tooltip("The boomerang aggressor gameobject")]
        [AssertReference]
        [SerializeField]
        private GameObject _aggressorGameObject;

        /// <summary>
        /// The sprite gameobject of the sword weapon
        /// </summary>
        [Tooltip("The sprite gameobject of the sword weapon")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Retrieve the id of the weapon
        /// </summary>
        public WeaponTypes Id {
            get { return _id; }
        }

        /// <summary>
        /// DelayBetweenShots must be overwritten
        /// but for the boomerang it has no
        /// usage
        /// </summary>
        public override float DelayBetweenShots {
            get { return 0; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// BoomerangHolderWeapon aggressor script of _aggressorGameObject object,
        /// though it is implementing the IAggressor Interface "_aggressor" 
        /// references the BoomerangThrowAggressor script itself as it is accessing
        /// fields and methods in the Bommerang script not available
        /// in IAggressor
        /// </summary>
        private BoomerangThrowAggressor _aggressor;

        /// <summary>
        /// The transform of the boomerang weapon gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The sprite renderer of the sword holder
        /// </summary>
        private SpriteRenderer _renderer;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected override void Awake() {
            base.Awake();
            CacheComponents();

            BoomerangThrowAggressor.ReenableRendererSignature callback = ReenableRendererCallback;
            _aggressor.ReenableRendererCallback = callback;
            _aggressor.transform.position = _t.position;
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Execute the attack
        /// Fires the bullet, pushes the player back, switches to reload state
        /// </summary>
        public void Fire() {
            _state = States.Fire;

            // shoot bullet in mouse direction
            Shoot();

            // switch to reload so the weapon can cool down
            _state = States.Reload;
        }

        /// <summary>
        /// In reload an animation could be played, a recover state (= reload state) could be set which 
        /// reloads the energy of the weapon and blocks firing the wepaon
        /// then it swtiches back to the idle state
        /// </summary>
        public void Reload() {
            _state = States.Reload;
        }

        /// <summary>
        /// Might be used in the future to upgrade a weapon
        /// </summary>
        public void Upgrade() {
            _state = States.Upgrade;
        }

        /// <summary>
        /// ReenableRendererCallback is used in BoomerangHolderWeapon
        /// as callback to reenable the renderer
        /// </summary>
        public void ReenableRendererCallback() {
            _renderer.enabled = true;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;

            _renderer = _spriteGameObject
                .GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;

            _aggressor = _aggressorGameObject
                .GetComponent(typeof(BoomerangThrowAggressor)) as BoomerangThrowAggressor;
        }

        /// <summary>
        /// Init the boomerang aggressor in the mouse direction &
        /// disable the sprite renderer of the weapon 
        /// </summary>
        protected override void Shoot() {
            // if the renderer isn't enabled this means
            // the boomerang is flying around
            if (_renderer.enabled == false)
                return;

            _renderer.enabled = false;

            _aggressorGameObject.transform.position = _ownerTransform.position;
            _aggressorGameObject.transform.rotation = _ownerTransform.rotation;

            Vector3 target = _c.ScreenToWorldPoint(Input.mousePosition);
            target.z = 0;
            _aggressor.Direction = target;

            _aggressorGameObject.SetActive(true);
            _aggressor.Throw();
        }
        #endregion PRIVATE_METHODS
    }
}