﻿using ArisingGames.Assets.Scripts.Collectables;

namespace ArisingGames.Assets.Scripts.Weapons {
    /// <summary>
    /// A weapon that requires a (limited number of) ammo may implement this
    /// interface to offer eg. increament of the ammo
    /// </summary>
	public interface IAmmoConsumer {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Return the remaining ammo
        /// </summary>
        int RemainingAmmo {
            get;
        }

        /// <summary>
        /// Return the ammo type the weapon is using
        /// </summary>
        AmmoTypes AmmoType {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PUBLIC_METHODS
        /// <summary>
        /// Increase the ammo
        /// </summary>
        /// <param name="size"></param>
        void Inc(int size);

        /// <summary>
        /// Decrease the ammo
        /// </summary>
        /// <param name="size"></param>
        void Dec(int size);
        #endregion PUBLIC_METHODS
    }
}