﻿using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Map;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player {
    /// <summary>
    /// Some helper functions for the player states
    /// 
    /// Thoughts on static classes: 
    /// http://stackoverflow.com/questions/241339/when-to-use-static-classes-in-c-sharp
    /// </summary>
    public static class PlayerHelper {
        #region CONSTANTS
        /// <summary>
        /// The direction the player is moving to
        /// </summary>
        public enum Direction {
            None,
            Right,
            RightUp,
            RightDown,
            Left,
            LeftUp,
            LeftDown,
            Up,
            Down
        }

        /// <summary>
        /// on checking the input of the axis the AxisTreshhold 
        /// must be exceeded to assume the player is really moving the joystick
        /// </summary>
        public const float AxisTreshhold = .01f;
        #endregion

        /// <summary>
        /// Try to switch to idle state
        /// 
        /// Return a bool true if switch happend
        /// so the calling method can cancel the processing
        /// of the following code
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="c"></param>
        /// <param name="state"></param>
        /// <param name="newState"></param>
        /// <returns>true if switch happend</returns>
        public static bool TryToIdleState(Direction dir, CharacterController2D c, GameObject state, GameObject newState) {
            if (dir == Direction.None) {
                c.Stop();
                state.SetActive(false);
                newState.SetActive(true);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if the axis is pushed
        /// Example: if the input comes from a gamepad and the 
        /// player pressed vertical up the method return true
        /// If the user doesnt press anything the method returns false
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool IsAxisCalm(float i) {
            if (i < AxisTreshhold && i > -AxisTreshhold)
                return true;
            return false;
        }

        /// <summary>
        /// Based on the input determine the direction
        /// </summary>
        /// <param name="h"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Direction DetermineDirection(float h, float v) {
            Direction dir = Direction.None;

            if (h < 0 && IsAxisCalm(v)) {
                dir = Direction.Left;
            } else if (h > 0 && IsAxisCalm(v)) {
                dir = Direction.Right;
            } else if (h < 0 && v > 0) {
                dir = Direction.LeftUp;
            } else if (h < 0 && v < 0) {
                dir = Direction.LeftDown;
            } else if (h > 0 && v > 0) {
                dir = Direction.RightUp;
            } else if (h > 0 && v < 0) {
                dir = Direction.RightDown;
            } else if (IsAxisCalm(h) && v < 0) {
                dir = Direction.Down;
            } else if (IsAxisCalm(h) && v > 0) {
                dir = Direction.Up;
            }

            return dir;
        }

        /// <summary>
        /// Play animation based on direction
        /// At the moment it's only one animation for all directions
        /// </summary>
        /// <param name="a"></param>
        /// <param name="animation"></param>
        public static void PlayAnimation(Animator a, string animation) {
            a.Play(animation);
        }

        /// <summary>
        /// Based on a direction and a given speed calc
        /// the speed to apply to the player movement
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public static Vector2 CalcTargetSpeed(Direction dir, float speed) {
            Vector2 targetSpeed = Vector2.zero;
            switch (dir) {
                case Direction.Left:
                    targetSpeed.x = -speed;
                    break;
                case Direction.LeftUp:
                    targetSpeed.x = -speed;
                    targetSpeed.y = speed;
                    break;
                case Direction.LeftDown:
                    targetSpeed.x = -speed;
                    targetSpeed.y = -speed;
                    break;
                case Direction.Right:
                    targetSpeed.x = speed;
                    break;
                case Direction.RightUp:
                    targetSpeed.x = speed;
                    targetSpeed.y = speed;
                    break;
                case Direction.RightDown:
                    targetSpeed.x = speed;
                    targetSpeed.y = -speed;
                    break;
                case Direction.Up:
                    targetSpeed.y = speed;
                    break;
                case Direction.Down:
                    targetSpeed.y = -speed;
                    break;
            }
            return targetSpeed;
        }

        /// <summary>
        /// Move the object using the character controller
        /// </summary>
        public static void Move(CharacterController2D c, Vector2 targetSpeed, float acceleration) {
            float x = Mathf.Lerp(c.Velocity.x, targetSpeed.x, Time.deltaTime * acceleration);
            float y = Mathf.Lerp(c.Velocity.y, targetSpeed.y, Time.deltaTime * acceleration);

            c.SetHorizontalForce(x);
            c.SetVerticalForce(y);
        }

        /// <summary>
        /// Ensure that the character cannot run outside the map
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="boundary"></param>
        /// <returns></returns>
        public static Vector3 Clamp(Vector3 pos, MapBoundaries boundary) {
            return new Vector2(
                Mathf.Clamp(pos.x, boundary.XMin, boundary.XMax),
                Mathf.Clamp(pos.y, boundary.YMin, boundary.YMax)
            );
        }
    }
}