﻿using System;

namespace ArisingGames.Assets.Scripts.Player {
    /// <summary>
    /// The playersavedata contains all values that are gonna be save / 
    /// loaded
    /// </summary>
    [Serializable]
    public class PlayerSaveData {

        // vorerst nur zum Test
        public string Name;

        public float Health;

    }
}