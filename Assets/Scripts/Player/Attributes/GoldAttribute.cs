﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.Attributes {
    public class GoldAttribute : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the attributes
        /// </summary>
        [Header("References")]
        [Tooltip("the owner of the attributes")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The current amount of gold
        /// </summary>
        public int Current {
            get { return _current; }
        }

        /// <summary>
        /// Event: health has changed
        /// </summary>
        public delegate void GoldChangedHandler(object sender, GoldChangedEventArgs e);
        public static event GoldChangedHandler GoldChanged;
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The current amount of gold
        /// </summary>
        private int _current;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS	
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            InitEventListeners();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            // subscribe to the treasure collected event
            TreasureCollectable.Collected += OnTreasureCollected;
        }

        /// <summary>
        /// on collected treasure send a message that the gold attribute has changed
        /// </summary>
        private void OnTreasureCollected(object sender, ItemCollectedEventArgs e) {
            _current += ((ITreasureCollectable)sender).Value;

            GoldChangedEventArgs args = new GoldChangedEventArgs(_current);
            if (GoldChanged != null)
                GoldChanged(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}
