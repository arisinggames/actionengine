﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Inventory.Slots.Strategies;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Owner;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.Attributes {
    /// <summary>
    /// Manage the health of the player
    /// </summary>
    public class HealthAttribute : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the attribute
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the attribute")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// Draw gizmo health bar
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("Draw gizmo health bar")]
        [SerializeField]
        private bool _gizmo = true;

        /// <summary>
        /// Max health
        /// </summary>
        [Tooltip("Max health")]
        [SerializeField]
        private float _max = 10;

        /// <summary>
        /// Min health
        /// </summary>
        [Tooltip("Max health")]
        [SerializeField]
        private float _min = 0;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Getter for max
        /// </summary>
        public float Max {
            get { return _max; }
        }

        /// <summary>
        /// The current health
        /// </summary>
        public float Current {
            get { return _current; }
        }

        /// <summary>
        /// Check if the health ran out
        /// </summary>
        public bool IsDead {
            get { return _current <= _min; }
        }

        /// <summary>
        /// Get the current health in percentage in relation to the max health
        /// </summary>
        public int Percentage {
            get { return Mathf.FloorToInt(_current / _max * 100); }
        }

        /// <summary>
        /// Event: health has changed
        /// </summary>
        public delegate void HealthChangedHandler(object sender, HealthChangedEventArgs e);
        public static event HealthChangedHandler HealthChanged;

        /// <summary>
        /// Event: death
        /// </summary>
        public delegate void DeathHandler(object sender, DeathEventArgs e);
        public static event DeathHandler Death;
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The current health
        /// </summary>
        private float _current;

        /// <summary>
        /// The owner of the attribute - implementing IOwner
        /// </summary>
        private IOwner _owner;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            _current = _max;

            InitEventListeners();
        }

        /// <summary>
        /// Show a provisional health bar at the bottom of the player
        /// </summary>
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            // the position of the owners transform must be accessed
            // via _ownerGameObject.transform... It's not possible to cache 
            // _ownerGameObject.transform in a class field like "_t" or
            // something like that because Start() or Awake() are
            // (by default) not executed in the editor. And to keep things
            // simple we are accessing _ownerGameObject.transform directly in 
            // OnDrawGizmos
            if (_gizmo) {
                float start = .5f;
                float mod = _current % 1;
                float length = .2f;
                float offset;
                int bars = (int)_current / 1;

                for (int i = 0; i < bars; i++) {
                    offset = i * .3f;
                    
                    Vector2 from = new Vector2(_ownerGameObject.transform.position.x - start + offset, _ownerGameObject.transform.position.y - .3f);
                    Vector2 to = new Vector2(_ownerGameObject.transform.position.x - start + offset + length, _ownerGameObject.transform.position.y - .3f);
                    Gizmos.DrawLine(from, to);
                }

                if (mod.Equals(.5f)) {
                    Gizmos.color = Color.magenta;
                    length = .1f;
                    offset = bars * .3f;

                    Vector2 from = new Vector2(_ownerGameObject.transform.position.x - start + offset, _ownerGameObject.transform.position.y - .3f);
                    Vector2 to = new Vector2(_ownerGameObject.transform.position.x - start + offset + length, _ownerGameObject.transform.position.y - .3f);
                    Gizmos.DrawLine(from, to);
                }
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// - Increment the players health. Ensure that only by full
        /// integers or floats with a decimal value of .5
        /// if wrong number supplied do nothing, just print error message
        /// - Ensure, that the max and min values are not exceeded
        /// - Fire an event to re-render the UI and / or if death
        /// has arrived
        /// </summary>
        /// <param name="value">integer or float with .5 as decimal value</param>
        public void Inc(float value) {
            float mod = value % 1;
            if (mod.Equals(.5f) || mod.Equals(0)) {
                if (_current + value > _max) {
                    Debug.Log("Health not increased. Maximum already achieved.");
                    return;
                }

                _current += value;

                OnHealthChanged();
                return;
            } 

            Debug.LogError("Error: invalid value as health supplied. Must be dividable by 1 or .5f");
        }

        /// <summary>
        /// - Decrement the players health. Ensure that only by full
        /// integers or floats with a decimal value of .5
        /// if wrong number supplied do nothing, just print error message
        /// - Ensure, that the max and min values are not exceeded
        /// - Fire an event to re-render the UI and / or if death
        /// has arrived
        /// </summary>
        /// <param name="value">integer or float with .5 as decimal value</param>
        public void Dec(float value) {
            float mod = value % 1;
            if (mod.Equals(.5f) || mod.Equals(0)) {

                if (Current - value < _min) {
                    _current = _min;
                    OnDeath();
                } else
                    _current -= value;

                OnHealthChanged();
                return;
            }

            Debug.LogError("Error: invalid value as health supplied. Must be dividable by 1 or .5f");
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _owner = _ownerGameObject.GetComponent(typeof(IOwner)) as IOwner;
            if (_owner == null)
                throw new OwnerNotImplementedException(_ownerGameObject);
        }

        /// <summary>
        /// Raise the event OnDeath
        /// </summary>
        private void OnDeath() {
            // if somebody is listening
            if (Death != null) {
                DeathEventArgs args = new DeathEventArgs(_owner);
                Death(this, args);
            }
        }

        /// <summary>
        /// Raise the event OnHealthChanged
        /// </summary>
        private void OnHealthChanged() {
            // if somebody is listening
            if (HealthChanged != null) {
                HealthChangedEventArgs args = new HealthChangedEventArgs(_current, _owner);
                HealthChanged(this, args);
            }
        }

        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            PotionStrategy.Consumed += (sender, e) => {
                Inc(e.Size);
            };
        }
        #endregion PRIVATE_METHODS
    }
}