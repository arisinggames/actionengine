﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.Attributes {
    /// <summary>
    /// Manage the stamina of the player
    /// </summary>
    public class StaminaAttribute : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the sensors
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the sensors")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// The states a primarily for debugging purpose
        /// </summary>
        private enum State {
            Idle,
            Stopped,
            Countdown,
            Recover
        }

        /// <summary>
        /// Max stamina
        /// </summary>
        public const int Max = 100;
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// How quickly the player's stamina recovers. 
        /// Lower value means faster recovery
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("How quickly the player's stamina recovers. " +
                 "Lower value means faster recovery")]
        [SerializeField]
        private float _recoveryRate = 1;

        /// <summary>
        /// How many units (points) of the stamina are recovered
        /// for each recoveryRate
        /// </summary>
        [Tooltip("How quickly the player's stamina recovers. " +
                 "Lower value means faster recovery")]
        [SerializeField]
        private float _recoveryUnit = 1;

        /// <summary>
        /// Draw gizmo stamina bar
        /// </summary>
        [Tooltip("Draw gizmo stamina bar")]
        [SerializeField]
        private bool _gizmo = true;

        /// <summary>
        /// Start the recovery after n seconds
        /// </summary>
        [Tooltip("Start the recovery after n seconds")]
        [SerializeField]
        private float _delay = 2;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The current stamina
        /// </summary>
        public float Current {
            get { return _current; }
            set { _current = value; }
        }
        #endregion PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The current stamina
        /// </summary>
        private float _current;

        /// <summary>
        /// Counter until the revoring starts
        /// </summary>
        private float _countdown;

        /// <summary>
        /// Used wit _recoveryRate to check if the
        /// stamina might be increased by the next unit
        /// </summary>
        private float _elapsedTime;

        /// <summary>
        /// Gets true and starts recovering if the countdown drops to 0
        /// </summary>
        private bool _waitToRecover;

        /// <summary>
        /// If the sensor is in recovering state
        /// </summary>
        private bool _isRecovering;

        /// <summary>
        /// The state the sensor is in.
        /// For debugging. To watch the state use the inspector in debug view
        /// </summary>
        private State _state;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);

            _current = Max;
            _countdown = _delay;
            _state = State.Idle;
        }

        /// <summary>
        /// Update eventt
        /// </summary>
        private void Update() {
            TryStartRecover();

            TryRecover();

            // it might happen that Current drops below zero
            if (Current < 0)
                _current = 0;
        }

        /// <summary>
        /// Show a provisional stamina bar at the bottom of the player
        /// </summary>
        private void OnDrawGizmos() {
            Gizmos.color = Color.yellow;
            // the position of the owners transform must be accessed
            // via _ownerGameObject.transform... It's not possible to cache 
            // _ownerGameObject.transform in a class field like "_t" or
            // something like that because Start() or Awake() are
            // (by default) not executed in the editor. And to keep things
            // simple we are accessing _ownerGameObject.transform directly in 
            // OnDrawGizmos
            if (_gizmo) {
                Vector2 from = new Vector2(
                    _ownerGameObject.transform.position.x - .5f, 
                    _ownerGameObject.transform.position.y - .1f);
                Vector2 to = new Vector2(
                    _ownerGameObject.transform.position.x + Current / 100 - .5f, 
                    _ownerGameObject.transform.position.y - .1f);
                Gizmos.DrawLine(from, to);
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Start the recovering 
        /// </summary>
        public void StartRecovering() {
            // if the recovering is already started just 
            // leave it running
            if (_state == State.Countdown || 
                _state == State.Recover)
                return;

            if (Current < Max) {
                _state = State.Countdown;
                _waitToRecover = true;
                _isRecovering = false;
                _elapsedTime = 0;
                _countdown = _delay;
            }
        }

        /// <summary>
        /// Stop the recovering 
        /// </summary>
        public void StopRecovering() {
            _state = State.Stopped;
            _waitToRecover = false;
            _isRecovering = false;
            _elapsedTime = 0;
            _countdown = _delay;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Check if the sensor is waiting for recover 
        /// and if so count down the timer and start 
        /// the recover process
        /// </summary>
        private void TryStartRecover() {
            if (_waitToRecover && !_isRecovering) {
                _state = State.Countdown;
                _countdown -= Time.deltaTime;
                if (_countdown <= 0) {
                    _waitToRecover = false;
                    _isRecovering = true;
                    _countdown = _delay;
                }
            }
        }

        /// <summary>
        /// Do the recovering
        /// </summary>
        private void TryRecover() {
            // if the stamina is not more then Max and
            // we are in the mode "_isRecovering" then
            // start the recovering
            if (Current < Max && _isRecovering) {
                _state = State.Recover;
                // ensure that one unit is restored
                // by the recovery rate
                _elapsedTime += Time.deltaTime;
                if (_elapsedTime >= _recoveryRate) {
                    _elapsedTime = 0;
                    _current += _recoveryUnit;
                }
            } else if (Current >= Max) {
                _isRecovering = false;
                _state = State.Idle;
            }
        }
        #endregion PRIVATE_METHODS
    }
}