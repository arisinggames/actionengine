﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.States {
    public class DodgeState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the state machine")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// The gameobject containing the sprite and the animator
        /// </summary>
        [Tooltip("The gameobject containing the sprite and the animator")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;

        /// <summary>
        /// Idle state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _idleStateGameObject;

        /// <summary>
        /// Walk state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _walkStateGameObject;

        /// <summary>
        /// Sprint state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _sprintStateGameObject;

        /// <summary>
        /// Stamina sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _staminaAttributeGameObject;

        /// <summary>
        /// The gameobject with the dust paticles
        /// </summary>
        [SerializeField]
        [Tooltip("The gameobject with the dust paticles")]
        [AssertReference]
        private GameObject _dustParticlesGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// Name of the animations
        /// </summary>
        private const string Animation = "Idle";
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// Sprint speed of player
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("Dodge speed of player")]
        [SerializeField]
        private float _speed = 35;

        /// <summary>
        /// Acceleration when sprinting
        /// </summary>
        [Tooltip("Acceleration when dodging")]
        [SerializeField]
        private float _acceleration = 100;

        /// <summary>
        /// How much stamina will be used when the player dodges
        /// </summary>
        [Tooltip("How much stamina will be used when the player dodges")]
        [SerializeField]
        private float _consume = 40f;

        /// <summary>
        /// The number of seconds the player will dodge for
        /// </summary>
        [Tooltip("The number of seconds the player will dodge for")]
        [SerializeField]
        private float _duration = .1f;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Incoming direction will be set from the calling state
        /// This is the dodging direction
        /// </summary>
        public PlayerHelper.Direction Dir {
            set { _dir = value; }
        }
        #endregion PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Incoming direction will be set from the calling state
        /// This is the dodging direction
        /// </summary>
        public PlayerHelper.Direction _dir;

        /// <summary>
        /// Counter til the dodge stops
        /// </summary>
        private float _countdown;

        /// <summary>
        /// Animator assigned to the game object
        /// which contains the sprite renderer
        /// </summary>
        private Animator _animator;

        /// <summary>
        /// The stamina script
        /// </summary>
        private StaminaAttribute _stamina;

        /// <summary>
        /// Reference to the controller
        /// </summary>
        private CharacterController2D _controller;

        /// <summary>
        /// The script of the particle system gameobject
        /// </summary>
        private ParticleSystem _dustParticles;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            _dustParticles.Play();

            _stamina.StopRecovering();
            _countdown = _duration;

            // based on the remaining stamina adjust the duration
            // example: if the dodge costs 40 stamina but only 20 
            // stamina are available and the regular dodge duration are 10 seconds
            // the dodge will only take 5 seconds
            float remainingStaminaPercent = _stamina.Current / StaminaAttribute.Max;
            _countdown = _duration * remainingStaminaPercent;

            // consume stamina
            _stamina.Current -= _consume;
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            // start countdown and move player
            if (_countdown > 0)
                _countdown -= Time.deltaTime;
            else {
                // after dodge has finished return to previous state
                // depending which keys the player is pressing

                // check if we have to switch to idle state
                // if so a (bool)true is returned so Update() 
                // itself can be left by a return to prevent
                // that the rest of Update() is executed
                if (PlayerHelper.TryToIdleState(_dir, _controller, transform.gameObject, _idleStateGameObject))
                    return;

                // if we have a direction (player is moving) 
                // but the sprint button is not pressed
                if (!Input.GetButton("SprintToggle") && _stamina.Current > 0) {
                    gameObject.SetActive(false);
                    _walkStateGameObject.SetActive(true);
                    return;
                }

                gameObject.SetActive(false);
                _walkStateGameObject.SetActive(true);
                return;
            }

            PlayerHelper.PlayAnimation(_animator, Animation);

            if (_countdown > 0) {
                Vector2 targetSpeed = PlayerHelper.CalcTargetSpeed(_dir, _speed);
                PlayerHelper.Move(_controller, targetSpeed, _acceleration);
            } else
                _controller.Stop();

        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _controller = _ownerGameObject
                .GetComponent(typeof(CharacterController2D)) as CharacterController2D;

            _animator = _spriteGameObject
                .GetComponent(typeof(Animator)) as Animator;

            _stamina = _staminaAttributeGameObject
                .GetComponent(typeof(StaminaAttribute)) as StaminaAttribute;

            _dustParticles = _dustParticlesGameObject.GetComponent(typeof(ParticleSystem)) as ParticleSystem;
        }
        #endregion PRIVATE_METHODS
    }
}