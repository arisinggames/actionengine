﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.CharacterController;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.States {
    /// <summary>
    /// Controller is inspired by the controller demonstrated
    /// at "2D Animation Methods in Unity" (gamasutra), extended
    /// by a rigidbody (http://answers.unity3d.com/questions/709877/how-to-make-2d-sprite-character-stop-jittering-whe.html)
    /// therefore using colliders
    /// </summary>
    public class SprintState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the state machine
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the state machine")]
        [AssertReference]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// The gameobject containing the sprite and the animator
        /// </summary>
        [Tooltip("The gameobject containing the sprite and the animator")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;

        /// <summary>
        /// Idle state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _idleStateGameObject;

        /// <summary>
        /// Walk state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _walkStateGameObject;

        /// <summary>
        /// Dodge state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _dodgeStateGameObject;

        /// <summary>
        /// Stamina sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _staminaAttributeGameObject;

        /// <summary>
        /// The gameobject with the dust paticles
        /// </summary>
        [Tooltip("The gameobject with the dust paticles")]
        [AssertReference]
        [SerializeField]
        private GameObject _dustParticlesGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// Names of the animations
        /// </summary>
        private const string Animation = "Walk";
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// Sprint speed of player
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("sprint speed of player")]
        [SerializeField]
        private float _speed = 15;

        /// <summary>
        /// Acceleration when sprinting
        /// </summary>
        [Tooltip("acceleration when sprinting")]
        [SerializeField]
        private float _acceleration = 60;

        /// <summary>
        /// How often the player's stamina depletes while sprinting. 
        /// Lower value means faster consumption
        /// </summary>
        [Tooltip("how often the player's stamina depletes while sprinting. " +
                 "Lower value means faster consumption")]
        [SerializeField]
        private float _consumeRate = .075f;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// Counter til the next unit stamina is used
        /// </summary>
        private float _countdown;

        /// <summary>
        /// The dodge script
        /// </summary>
        private States.DodgeState _dodge;

        /// <summary>
        /// Direction
        /// </summary>
        private PlayerHelper.Direction _dir;

        /// <summary>
        /// Animator assigned to the game object
        /// which contains the sprite renderer
        /// </summary>
        private Animator _animator;

        /// <summary>
        /// The stamina script
        /// </summary>
        private StaminaAttribute _stamina;

        /// <summary>
        /// Reference to the controller
        /// </summary>
        private CharacterController2D _controller;

        /// <summary>
        /// The script of the particle system gameobject
        /// </summary>
        private ParticleSystem _dustParticles;

        /// <summary>
        /// Input by the axis 
        /// </summary>
        private float _horzInput;
        private float _vertInput;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            _dustParticles.Play();
            _stamina.StopRecovering();
            _countdown = _consumeRate;
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            _horzInput = Input.GetAxisRaw("Horizontal");
            _vertInput = Input.GetAxisRaw("Vertical");

            _dir = PlayerHelper.DetermineDirection(_horzInput, _vertInput);

            // check if we have to switch to idle state
            // if so a (bool)true is returned so Update() 
            // itself can be left by a return to prevent
            // that the rest of Update() is executed
            if (PlayerHelper.TryToIdleState(_dir, _controller, transform.gameObject, _idleStateGameObject)) return;

            // if dodge button is pressed
            if (Input.GetButton("DodgeToggle") && _stamina.Current > 0) {
                gameObject.SetActive(false);
                _dodge.Dir = _dir;
                _dodgeStateGameObject.SetActive(true);
                return;
            }

            if (_stamina.Current <= 0) {
                gameObject.SetActive(false);
                _walkStateGameObject.SetActive(true);
                return;
            }

            // if we have a direction (player is moving) 
            // but the sprint button is not pressed
            if (!Input.GetButton("SprintToggle")) {
                gameObject.SetActive(false);
                _walkStateGameObject.SetActive(true);
                return;
            }

            // if player sprints decrease stamina
            ConsumeStamina();

            PlayerHelper.PlayAnimation(_animator, Animation);

            Vector2 targetSpeed = PlayerHelper.CalcTargetSpeed(_dir, _speed);

            PlayerHelper.Move(_controller, targetSpeed, _acceleration);
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _dodge = _dodgeStateGameObject
                .GetComponent(typeof(DodgeState)) as DodgeState;

            _animator = _spriteGameObject
                .GetComponent(typeof(Animator)) as Animator;

            _stamina = _staminaAttributeGameObject
                .GetComponent(typeof(StaminaAttribute)) as StaminaAttribute;

            _controller = _ownerGameObject
                .GetComponent(typeof(CharacterController2D)) as CharacterController2D;

            _dustParticles = _dustParticlesGameObject
                .GetComponent(typeof(ParticleSystem)) as ParticleSystem;
        }

        /// <summary>
        /// Decrease the stamina based on the consume rate
        /// </summary>
        private void ConsumeStamina() {
            _countdown -= Time.deltaTime;
            if (_countdown <= 0) {
                _stamina.Current--;
                _countdown = _consumeRate;
            }
        }
        #endregion PRIVATE_METHODS
    }
}