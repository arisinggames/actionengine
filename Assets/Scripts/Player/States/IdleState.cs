﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.States {
    /// <summary>
    /// Controller is inspired by the controller demonstrated
    /// at "2D Animation Methods in Unity" (gamasutra), extended
    /// by a rigidbody (http://answers.unity3d.com/questions/709877/how-to-make-2d-sprite-character-stop-jittering-whe.html)
    /// therefore using colliders
    /// </summary>
    public class IdleState : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The gameobject containing the sprite and the animator
        /// </summary>
        [Header("References")]
        [Tooltip("The gameobject containing the sprite and the animator")]
        [AssertReference]
        [SerializeField]
        private GameObject _spriteGameObject;

        /// <summary>
        /// Walk state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _walkStateGameObject;

        /// <summary>
        /// Sprint state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _sprintStateGameObject;

        /// <summary>
        /// Dodge state
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _dodgeStateGameObject;

        /// <summary>
        /// Stamina sensor
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _staminaAttributeGameObject;

        /// <summary>
        /// The gameobject with the dust paticles
        /// </summary>
        [Tooltip("The gameobject with the dust paticles")]
        [AssertReference]
        [SerializeField]
        private GameObject _dustParticlesGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// Names of the animations
        /// </summary>
        private const string Animation = "Idle";
        #endregion CONSTANTS

        #region PRIVATE_FIELDS
        /// <summary>
        /// Animator assigned to the game object
        /// which contains the sprite renderer
        /// </summary>
        private Animator _animator;

        /// <summary>
        /// The script of the particle system gameobject
        /// </summary>
        private ParticleSystem _dustParticles;

        /// <summary>
        /// The dodge script
        /// </summary>
        private DodgeState _dodge;

        /// <summary>
        /// The stamina script
        /// </summary>
        private StaminaAttribute _stamina;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            _animator.Play(Animation);
        }

        /// <summary>
        /// OnEnable event
        /// </summary>
        private void OnEnable() {
            _dustParticles.Stop();
            _animator.Play(Animation);
            _stamina.StartRecovering();
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            // if the user presses an axis button (wasd, the joystick...)
            if (!PlayerHelper.IsAxisCalm(h) || !PlayerHelper.IsAxisCalm(v)) {
                gameObject.SetActive(false);

                if (Input.GetButton("DodgeToggle") && _stamina.Current > 0) {
                    PlayerHelper.Direction dir = PlayerHelper.DetermineDirection(h, v);
                    _dodge.Dir = dir;
                    _dodgeStateGameObject.SetActive(true);
                } else if (Input.GetButton("SprintToggle") && _stamina.Current > 0) 
                    _sprintStateGameObject.SetActive(true);
                else 
                    _walkStateGameObject.SetActive(true);
            }
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _dodge = _dodgeStateGameObject
                .GetComponent(typeof(DodgeState)) as DodgeState;

            _animator = _spriteGameObject
                .GetComponent(typeof(Animator)) as Animator;

            _stamina = _staminaAttributeGameObject
                .GetComponent(typeof(StaminaAttribute)) as StaminaAttribute;

            _dustParticles = _dustParticlesGameObject.GetComponent(typeof(ParticleSystem)) as ParticleSystem;
        }
        #endregion PRIVATE_METHODS
    }
}