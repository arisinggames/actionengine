﻿using ArisingGames.Assets.Scripts.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player {
    /// <summary>
    /// Let the sprite face to the left or right
    /// </summary>
    public class FacingHandler : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The owner of the facing handler, usually the sprite game object
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the facing handler, usually the sprite game object")]
        [SerializeField]
        private GameObject _ownerGameObject;

        /// <summary>
        /// The camera
        /// </summary>
        [Tooltip("The camera")]
        [AssertReference]
        [SerializeField]
        private GameObject _cameraGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        /// <summary>
        /// Facing direction of the character
        /// </summary>
        public const int FacingRight = 1;
        public const int FacingLeft = -1;
        public const int NoFacingChange = 0;
        #endregion CONSTANTS

        #region PRIVATE_FIELDS
        /// <summary>
        /// Reference to SPRITES transform for caching & easier access
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The camera component of the camera gameobject
        /// </summary>
        private UnityEngine.Camera _c;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            // get the mouse position in units
            Vector3 mouse = _c.ScreenToWorldPoint(Input.mousePosition);

            Handle(_t.position, mouse);
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS 
        /// <summary>
        /// Fetermine the facing based on 2 vectors. If the targets x position
        /// is smaller then the sources x position let the sprite face to the left
        /// otherwise to the right
        /// 
        /// Used in conjunction with the camera where target is the mouse position,
        /// source the center 
        /// </summary>
        public void Handle(Vector3 source, Vector3 target) {
            // the player (and with it the sprite) might have
            // been destroyed by a nasty enemy attack
            if (_t == null)
                return;

            int facing;
            if (target.x < source.x)
                facing = FacingLeft;
            else
                facing = FacingRight;

            _t.localScale = Face(_t.localScale, facing);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = _ownerGameObject.transform;
            _c = _cameraGameObject.GetComponent<UnityEngine.Camera>();
        }

        /// <summary>
        /// Face into certain direction: -1 for left or 1 for right
        /// </summary>
        /// <param name="scale">the current localScale</param>
        /// <param name="dir">new facing direction</param>
        /// <returns></returns>
        private Vector3 Face(Vector3 scale, int dir) {
            if (dir == NoFacingChange)
                return scale;

            // if the direction is already left and the new one
            // left - do nothing
            if (dir == FacingLeft && (int)scale.x == FacingLeft)
                return scale;
            // if the direction is already left and the new one
            // right it has to be multiplied by -1
            if (dir == FacingLeft && (int)scale.x == FacingRight)
                return new Vector3(scale.x * -1, scale.y, scale.z);
            // same process for right
            if (dir == FacingRight && (int)scale.x == FacingRight)
                return scale;
            if (dir == FacingRight && (int)scale.x == FacingLeft)
                return new Vector3(scale.x * -1, scale.y, scale.z);

            return scale;
        }
        #endregion PRIVATE_METHODS
    }
}
