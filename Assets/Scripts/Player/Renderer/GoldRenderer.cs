﻿using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Player.Renderer {
	public class GoldRenderer : MonoBehaviour {
        #region PRIVATE_FIELDS
        /// <summary>
        /// The amount of gold
        /// </summary>
	    private Text _text;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS	
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();
            InitEventListeners();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _text = GetComponent(typeof(Text)) as Text;
            if (_text == null)
                throw new ComponentNotFoundException("Text", this);
        }

        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            // subscribe to the gold changed event
            GoldAttribute.GoldChanged += (sender, e) => {
                _text.text = e.Amount.ToString();
            };
        }
        #endregion PRIVATE_METHODS
    }
}