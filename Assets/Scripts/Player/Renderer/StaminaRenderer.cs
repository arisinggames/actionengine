﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.Renderer {
	public class StaminaRenderer : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Stamina sensor
        /// </summary>
        [Header("References")]
        [AssertReference]
        [SerializeField]
        private GameObject _staminaAttributeGameObject;

        /// <summary>
        /// Stamina bar
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _barGameObject;

        /// <summary>
        /// Stamina bar background
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _backgroundGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The stamina script
        /// </summary>
        private StaminaAttribute _stamina;

        /// <summary>
        /// Are the children Bar and Background activated?
        /// </summary>
	    private bool _areActivated;

        /// <summary>
        /// Reference to the BAR transform for caching & easier access
        /// </summary>
        private Transform _t;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS	
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// Start event
        /// </summary>
	    private void Start() {
	        Hide();
	    }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            // if the player has full stamina hide the stamina
            // bar completely. Problem: There is no direct
            // way to hide game objects
            // Therefore set the children active (or not)
            // Of course we could also dismiss _areActivated
            // but in this case Update() would activate / deactivate
            // every frame the game objects
            // we could also fetch in Awake() the children 
            // but we need to the gameobject "Bar" direct access
            // anyway. That's the reason why Bar and Background
            // are handed to this script as references 
            if (_stamina.Current < StaminaAttribute.Max) {
                if (!_areActivated) {
                    _barGameObject.gameObject.SetActive(true);
                    _backgroundGameObject.gameObject.SetActive(true);
                    _areActivated = true;
                }

                _t.localScale = new Vector3(_stamina.Current * .01f, _t.localScale.y);
            } else if (_areActivated) 
                Hide();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _stamina = _staminaAttributeGameObject
                .GetComponent(typeof(StaminaAttribute)) as StaminaAttribute;

            _t = _barGameObject.transform;
        }

        /// <summary>
        /// Hide the stamina bar
        /// </summary>
	    private void Hide() {
            _barGameObject.gameObject.SetActive(false);
            _backgroundGameObject.gameObject.SetActive(false);
            _areActivated = false;
        }
        #endregion PRIVATE_METHODS
    }
}