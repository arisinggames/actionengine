﻿using System;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player.Renderer {
    /// <summary>
    /// Show the health as a nice row of hearts
    /// </summary>
	public class HealthRenderer : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Health attribute
        /// </summary>
        [Header("References")]
        [AssertReference]
        [SerializeField]
        private GameObject _healthAttributeGameObject;

        /// <summary>
        /// The full heart prefab
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _fullHeartGameObject;

        /// <summary>
        /// The half heart prefab
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _halfHeartGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The health script
        /// </summary>
        private HealthAttribute _health;

        /// <summary>
        /// The arrays with full and half hearts
        /// They will be activated / deactivated
        /// depending on the current health
        /// </summary>
        private GameObject[] _fullHearts;
        private GameObject[] _halfHearts;

        /// <summary>
        /// The max number of hearts displayed
        /// converted from HealthAttribute.Max
        /// </summary>
        private int _max;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS	
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
            InitEventListeners();

            InitArrays();
            Populate();
            RenderFullHearts();
            TryRenderHalfHeart();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _health = _healthAttributeGameObject
                .GetComponent(typeof(HealthAttribute)) as HealthAttribute;
            if (_health == null)
                throw new ComponentNotFoundException("HealthAttribute", _healthAttributeGameObject);
        }

        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            // subscribe to the health event
            HealthAttribute.HealthChanged += HealthChanged;
        }

        /// <summary>
        /// Init the heart arrays
        /// </summary>
        private void InitArrays() {
            _max = Mathf.CeilToInt(_health.Max);
            _fullHearts = new GameObject[_max];
            _halfHearts = new GameObject[_max];
        }

        /// <summary>
        /// Populate the heart arrays with game object
        /// </summary>
        private void Populate() {
            GameObject heart;
            for (int i = 0; i < _max; i++) {
                heart = Instantiate(_fullHeartGameObject);
                heart.name = "FullHeart " + i;
                heart.transform.SetParent(gameObject.transform, false);
                heart.SetActive(false);
                _fullHearts[i] = heart;

                heart = Instantiate(_halfHeartGameObject);
                heart.name = "HalfHeart " + i;
                heart.transform.SetParent(gameObject.transform, false);
                heart.SetActive(false);
                _halfHearts[i] = heart;
            }
        }

        /// <summary>
        /// Render the full hearts based on the current players health
        /// </summary>
        private void RenderFullHearts() {
            // depending on the current health show / hide hearts
            for (int i = 0; i < _max; i++) {
                // if the current heart index is intersecting the current
                // players health show it, otherwise hide it
                if (i + 1 <= _health.Current)
                    _fullHearts[i].SetActive(true);
                else
                    _fullHearts[i].SetActive(false);

                // we have to disable all half hearts
                _halfHearts[i].SetActive(false);
            }
        }

        /// <summary>
        /// Try to display a half heart depending on the current
        /// players health
        /// </summary>
        private void TryRenderHalfHeart() {
            // if we have a floating value this means we show a half heart
            // at the last position of the players current health
            float mod = _health.Current % 1;
            if (mod.Equals(.5f)) {
                int index = (int)_health.Current;
                _halfHearts[index].SetActive(true);
            }
        }

        /// <summary>
        /// On health changed re-render the health bar
        /// </summary>
        private void HealthChanged(object sender, EventArgs e) {
            RenderFullHearts();
            TryRenderHalfHeart();
        }
        #endregion PRIVATE_METHODS
    }
}