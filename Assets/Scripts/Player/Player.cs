﻿using ArisingGames.Assets.Scripts.Owner;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Player {
    public class Player : MonoBehaviour, IOwner {
        #region CONFIGURATION
        /// <summary>
        /// The owner id
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("The owner id")]
        [SerializeField]
        private Owners _ownerId;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Getter for _playerSaveData
        /// </summary>
        public PlayerSaveData Data {
            get { return _playerSaveData; }
        }

        /// <summary>
        /// The owner id
        /// </summary>
        [SerializeField]
        public Owners OwnerId {
            get { return _ownerId; }
        }
        #endregion PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Don’t expose state in inspector. State is not tweakable.
        /// </summary>
        [SerializeField]
        private PlayerSaveData _playerSaveData;

        /// <summary>
        /// reference to sprites facing handler
        /// </summary>
        private FacingHandler _facingHandler;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            HealthAttribute.Death += (sender, e) => {
                if (e.Owner.OwnerId == Owners.Player) 
                    transform.gameObject.SetActive(false);
            };
        }
        #endregion MONO_EVENTS
    }
}