﻿using System;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Map;
using ArisingGames.Assets.Scripts.Misc;
using ArisingGames.Assets.Scripts.Player;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Camera {
    /// <summary>
    /// Camera extends to the mouse position, based on the screen coordinates,
    /// locked to a radius
    /// 
    /// The camera also follows the player respecting a certain margin
    /// around the player and respecting the map boundaries
    /// 
    /// It has also a lock target system and a camera shake
    /// 
    /// For inspiration see
    /// https://www.youtube.com/watch?v=u67fbxe8xxY&list=PLt_Y3Hw1v3QSFdh-evJbfkxCK_bjUD37n&index=15
    /// https://www.toptal.com/unity-unity3d/2d-camera-in-unity
    /// </summary>
    public class CameraTracking : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The tracking target, probably the player
        /// </summary>
        [Header("References")]
        [Tooltip("The target, probably the player")]
        [AssertReference]
        [SerializeField]
        private GameObject _targetGameObject;

        /// <summary>
        /// The camera
        /// </summary>
        [Tooltip("The camera")]
        [AssertReference]
        [SerializeField]
        private GameObject _cameraGameObject;

        /// <summary>
        /// The GameObject containing the boundaries for the camera
        /// so that the camera viewport cannot exceed the map
        /// </summary>
        [Tooltip("The GameObject containing the boundaries for the camera " +
                 "so that the camera viewport cannot exceed the map")]
        [AssertReference]
        [SerializeField]
        private GameObject _cameraBoundariesGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// The damping of the camera movement to make it smoother
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("The damping of the camera movement to make it smoother")]
        [SerializeField]
        private float _damping = 12;

        /// <summary>
        /// The radius the camera movement will be constrained to
        /// </summary>
        [Tooltip("The radius the camera movement will be constrained to")]
        [SerializeField]
        private float _radius = 3;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        /// <summary>
        /// The transform of the target
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The transform of the camera
        /// </summary>
        private Transform _ct;

        /// <summary>
        /// Camera component
        /// </summary>
        private UnityEngine.Camera _c;

        /// <summary>
        /// velocity of the camera
        /// </summary>
        private Vector2 _velocity;

        /// <summary>
        /// calculated bounds of the camera
        /// </summary>
        private Vector2 _boundsMin = Vector2.zero;
        private Vector2 _boundsMax = Vector2.zero;

#if DEBUG
        public Vector3 _mousePosInWorld;

        /// <summary>
        /// The new position of the camera. Will be populated only
        /// in debug build to avoid unnecessary class field population
        /// which is (in fact a very, very small) performance optimization
        /// </summary>
        public Vector3 _debugPos;
#endif 
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Assert event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
            CacheComponents();
            InitBoundaries();
        }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
            Vector3 target = MoveViewport();
            target = Clamp(target);

            // move the camera smoothly to the target position
            _ct.position = Vector3.Lerp(_ct.position, target, Time.deltaTime * _damping);
        }

#if DEBUG
        /// <summary>
        /// Show the max radius from the player to the camera
        /// </summary>
        private void OnDrawGizmos() {
            // gray circle: the max radius of the camera around the player
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(_targetGameObject.transform.position, _radius);

            // green cirle: the mouse position
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(_mousePosInWorld, 1f);

            // camera position
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(_debugPos, .5f);
        }
#endif 
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _c = _cameraGameObject.GetComponent<UnityEngine.Camera>();
            _t = _targetGameObject.transform;
            _ct = _cameraGameObject.transform;
        }

        /// <summary>
        /// Move the viewport around the player
        /// 
        /// The idea: get the distance between the mouse and the screen center
        /// expressed in units
        /// add this distance to the cameras position, which is the players 
        /// position by default
        /// </summary>
        private Vector3 MoveViewport() {
            // get the center in units 
            Vector3 center = _c.ViewportToWorldPoint(new Vector2(.5f, .5f));

            // get the mouse position in units
            Vector3 mouse = _c.ScreenToWorldPoint(Input.mousePosition);

            // get the distance vector to use as camera offset
            Vector3 offset = mouse - center;

            // clamp the offset to the max radius
            // to compare if the distance between mouse and player > radius
            // we are using the square roots (it's faster)
            if (offset.sqrMagnitude > _radius * _radius)
                offset = Vector3.ClampMagnitude(offset, _radius);

            // the camera = players position 
            // add the distance as offset to get the cameras target position
            // to move to
            Vector3 target = _t.position + offset;

            // keep depth
            target.z = _ct.position.z;

#if DEBUG
            _debugPos = target;
            _mousePosInWorld = mouse;
#endif

            return target;
        }

        /// <summary>
        /// Constrain the camera to the map boundaries
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private Vector3 Clamp(Vector3 target) {
            target.x = Mathf.Clamp(target.x, _boundsMin.x, _boundsMax.x);
            target.y = Mathf.Clamp(target.y, _boundsMin.y, _boundsMax.y);
            return target;
        }

        /// <summary>
        /// Calculate the "real" boundaries for the camera = where the camera
        /// is allowed to move to (it's not the mapboundary itself)
        /// </summary>
        private void InitBoundaries() {
            float verticalSize = _c.orthographicSize;

            float horizontalSize = verticalSize * Screen.width / Screen.height;

            _boundsMin = new Vector2(
                _cameraBoundariesGameObject.GetComponent<MapBoundaries>().XMin + horizontalSize,
                _cameraBoundariesGameObject.GetComponent<MapBoundaries>().YMin + verticalSize
            );

            _boundsMax = new Vector2(
                _cameraBoundariesGameObject.GetComponent<MapBoundaries>().XMax - horizontalSize,
                _cameraBoundariesGameObject.GetComponent<MapBoundaries>().YMax - verticalSize
            );
        }
        #endregion PRIVATE_METHODS
    }
}