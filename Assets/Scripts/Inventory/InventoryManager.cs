﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Inventory.Slots;
using ArisingGames.Assets.Scripts.Inventory.Slots.Strategies;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Inventory {
    /// <summary>
    /// For an explanation of the InventoryManager and the interaction with
    /// the other components please have a look at the readme.md
    /// </summary>
    public class InventoryManager : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Box image for an activated item 
        /// </summary>
        [Header("References")]
        [Tooltip("Box image for an activated item")]
        [AssertReference]
        [SerializeField]
        private Sprite _boxActivated;

        /// <summary>
        /// Box image if a box is deactivated
        /// </summary>
        [Tooltip("Box image if a box gets deactivated")]
        [AssertReference]
        [SerializeField]
        private Sprite _boxDeactivated;

        /// <summary>
        /// The prefab of default item to be placed inside a box
        /// </summary>
        [Tooltip("The prefab of default item to be placed inside a box")]
        [AssertReference]
        [SerializeField]
        private GameObject _defaultItemGameObject;

        /// <summary>
        /// The prefab for a default item displaying a stack number
        /// </summary>
        [Tooltip("The prefab for a default item displaying a stack number")]
        [AssertReference]
        [SerializeField]
        private GameObject _stackItemGameObject;

        /// <summary>
        /// The weapon manager
        /// </summary>
        [Tooltip("The weapon manager")]
        [AssertReference]
        [SerializeField]
        private GameObject _weaponManagerGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Get the box image for activated box
        /// </summary>
        public Sprite BoxActivated {
            get { return _boxActivated; }
        }

        /// <summary>
        /// Get the box image for deactivated box
        /// </summary>
        public Sprite BoxDeactivated {
            get { return _boxDeactivated; }
        }

        /// <summary>
        /// Get all slots
        /// </summary>
        public List<GameObject> Slots {
            get { return _slots; }
        }

        /// <summary>
        /// Get the prefab for the default item
        /// </summary>
        public GameObject DefaultItemGameObject {
            get { return _defaultItemGameObject; }
        }

        /// <summary>
        /// The prefab for a default item displaying a stack number
        /// </summary>
        public GameObject StackItemGameObject {
            get { return _stackItemGameObject; }
        }

        /// <summary>
        /// The weapon manager
        /// </summary>
        public GameObject WeaponManagerGameObject {
            get { return _weaponManagerGameObject; }
        }
        #endregion

        #region PRIVATE_FIELDS
        /// <summary>
        /// All slots = the children in the hierarchy beneath the inventory manager
        /// </summary>
        private List<GameObject> _slots;

        /// <summary>
        /// Dictionary with all slots that have a hotkey
        /// for faster access
        /// </summary>
        private Dictionary<string, GameObject> _hotkeysToSlots;

        /// <summary>
        /// Array with the button names only to check during Update()
        /// if a key has been pressed (array faster the dictionary)
        /// see performance comparison http://jacksondunstan.com/articles/3058
        /// </summary>
        private string[] _hotkeys;

        /// <summary>
        /// The transform of the inventory manager gameobject
        /// </summary>
        private Transform _t;

        /// <summary>
        /// The strategies for the slot types
        /// </summary>
        private Dictionary<SlotTypes, IInventoryStrategy> _slotsToStrategies;

        /// <summary>
        /// The strategies
        /// </summary>
        private IInventoryStrategy _ammoStrategy;
        private IInventoryStrategy _commonStrategy;
        private IInventoryStrategy _potionStrategy;
        private IInventoryStrategy _weaponStrategy;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();

            InitStrategies();

            _slots = new List<GameObject>();
            PopulateSlots();

            _hotkeysToSlots = new Dictionary<string, GameObject>();
            PopulateHotkeysToSlots();

            _hotkeys = _hotkeysToSlots.Keys.ToArray();

            _slotsToStrategies = new Dictionary<SlotTypes, IInventoryStrategy>();
            PopulateStrategiesToSlots();

            DisplayHotkeys();

            InitEventListeners();
        }

        /// <summary>
        /// Update: check if a hotkey has been pressed
        /// </summary>
        private void Update() {
            // to check if a hotkey has been pressed there seems to be only one solution
            // which is horrible ...
            // assign to the slots the button names as defined in the input manager
            // check in each frame if one of these buttons has been pressed...
            for (int i = 0; i < _hotkeys.Length; i++) {
                string hotkey = _hotkeys[i];
                if (Input.GetKeyDown(hotkey))
                    ApplyStrategy(hotkey);
            }
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Used by the UI to catch a click onto a slot
        /// </summary>
        /// <param name="slot"></param>
        public void OnSlotClick(GameObject slot) {
            ApplyStrategy(slot);
        }

        /// <summary>
        /// create for the default item prefab a gameobject
        /// used by the strategies as they dont implement MonoBehaviour
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject CreateDefaultItem(Transform parent, string name) {
            GameObject go = Instantiate(_defaultItemGameObject);
            go.transform.SetParent(parent, false);
            go.name = name;
            go.SetActive(enabled);
            return go;
        }

        /// <summary>
        /// create an item with display of stack number
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject CreateStackableItem(Transform parent, string name, int size) {
            GameObject go = Instantiate(_stackItemGameObject);
            go.transform.SetParent(parent, false);
            go.name = name;

            // the stack item should have one child that has a text component
            if (go.transform.childCount != 1)
                throw new Exception("It is expected that the stackItem prefab contains one " +
                                    "single child with a text component");
            Text stack = go.GetComponentInChildren(typeof(Text)) as Text;
            stack.text = size.ToString();

            go.SetActive(enabled);
            return go;
        }

        /// <summary>
        /// destroy an item
        /// used by the strategies as they dont implement MonoBehaviour
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public void DestroyItem(Transform parent) {
            Transform item = parent.GetChild(0);
            Destroy(item.gameObject);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _t = transform;
        }

        /// <summary>
        /// Init strategies
        /// </summary>
        private void InitStrategies() {
            _commonStrategy = new CommonStrategy(this);
            _potionStrategy = new PotionStrategy(this);
            _weaponStrategy = new WeaponStrategy(this);
        }

        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            CommonCollectable.Collected += (sender, e) => {
                _commonStrategy.Add(sender);
            };
            
            PotionCollectable.Collected += (sender, e) => {
                _potionStrategy.Add(sender);
            };
            
            WeaponCollectable.Collected += (sender, e) => {
                _weaponStrategy.Add(sender);
            };
        }

        /// <summary>
        /// Apply strategy based on hotkey press
        /// </summary>
        /// <param name="hotkey"></param>
        private void ApplyStrategy(string hotkey) {
            // get the slot to work on
            GameObject go = _hotkeysToSlots[hotkey];

            // the type of slot
            ISlot slot = go.GetComponent(typeof(ISlot)) as ISlot;
            if (slot == null)
                throw new ComponentNotFoundException("ISlot", go);

            // check if the box of the slot has an item assigned, otherwise cancel
            if (slot.BoxGameObject.transform.childCount == 1) {
                IInventoryStrategy strategy = _slotsToStrategies[slot.Type];
                strategy.Use(slot);
            }
        }

        /// <summary>
        /// Apply strategy based on mouse click onto a slot
        /// </summary>
        /// <param name="slotGameObject"></param>
        private void ApplyStrategy(GameObject slotGameObject) {
            ISlot slot = slotGameObject.GetComponent(typeof(ISlot)) as ISlot;
            if (slot == null)
                throw new ComponentNotFoundException("ISlot", slotGameObject);

            // check if the box of the slot has an item assigned, otherwise cancel
            if (slot.BoxGameObject.transform.childCount == 1) {
                IInventoryStrategy strategy = _slotsToStrategies[slot.Type];
                strategy.Use(slot);
            }
        }

        /// <summary>
        /// Populate the slot list
        /// </summary>
        private void PopulateSlots() {
            int children = _t.childCount;

            for (int i = 0; i < children; ++i) {
                Transform c = _t.GetChild(i);
                ISlot slot = c.GetComponent(typeof(ISlot)) as ISlot;
                if (slot == null) {
                    Debug.Log("Error: no ISlot component found for " + c.name);
                    continue;
                }

                _slots.Add(c.gameObject);
            }
        }

        /// <summary>
        /// Populate the hotkeys list
        /// </summary>
        private void PopulateHotkeysToSlots() {
            for (int i = 0; i < _slots.Count; ++i) {
                ISlot slot = _slots[i].GetComponent(typeof(ISlot)) as ISlot;

                if (slot != null && slot.Hotkey.Length > 0) {
                    if (_hotkeysToSlots.ContainsKey(slot.Hotkey))
                        throw new Exception("The hotkey \"" + slot.Hotkey + "\" is assigned at least twice");

                    _hotkeysToSlots.Add(slot.Hotkey, _slots[i]);
                }
            }
        }

        /// <summary>
        /// Get from each slot the assigned hotkey and display it
        /// above the box
        /// </summary>
        private void DisplayHotkeys() {
            for (int i = 0; i < _slots.Count; ++i) {
                GameObject go = _slots[i];
                ISlot slot = go.GetComponent(typeof(ISlot)) as ISlot;

                string hotkey = "";
                if (slot != null && slot.Hotkey.Length > 0)
                    hotkey = slot.Hotkey;

                if (slot != null) {
                    Text hotkeyText = slot.HotkeyGameObject.GetComponent(typeof(Text)) as Text;
                    if (hotkeyText != null)
                        hotkeyText.text = hotkey;
                }
            }
        }

        /// <summary>
        /// Init the slot strategies
        /// </summary>
        private void PopulateStrategiesToSlots() {
            _slotsToStrategies.Add(SlotTypes.WeaponLeft, _weaponStrategy);
            _slotsToStrategies.Add(SlotTypes.WeaponRight, _weaponStrategy);
            _slotsToStrategies.Add(SlotTypes.Health, _potionStrategy);
            _slotsToStrategies.Add(SlotTypes.Common, _commonStrategy);
        }
        #endregion PRIVATE_METHODS
    }
}