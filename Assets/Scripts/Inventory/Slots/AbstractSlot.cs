﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Inventory.Slots {
    /// <summary>
    /// Abstract slot
    /// </summary>
	public abstract class AbstractSlot : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The text game object for hotkey displayment
        /// </summary>
        [Header("References")]
        [Tooltip("The text game object for hotkey displayment")]
        [AssertReference]
        [SerializeField]
        protected GameObject _hotkeyGameObject;

        /// <summary>
        /// The box game object
        /// </summary>
        [Tooltip("The box game object")]
        [AssertReference]
        [SerializeField]
        protected GameObject _boxGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The slot type. Multiple slots may have the same type
        /// </summary>
        [SerializeField]
        [Tooltip("The slot type. Multiple slots may have the same type")]
        protected SlotTypes _type;

        /// <summary>
        /// The hotkey for the slot.
        /// Might be left empty
        /// </summary>
        [SerializeField]
        [Tooltip("The hotkey for the slot. Might be left empty")]
        protected string _hotkey;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The text game object for hotkey displayment
        /// </summary>
        public GameObject HotkeyGameObject {
            get { return _hotkeyGameObject;  }
        }

        /// <summary>
        /// The box of the slot
        /// </summary>
        public GameObject BoxGameObject {
            get { return _boxGameObject; }
        }
        #endregion

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected void Awake() {
            CommonHelper.AssertReferences(this);
        }
        #endregion MONO_EVENTS
    }
}