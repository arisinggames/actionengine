﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Inventory.Slots {
    /// <summary>
    /// All slot types
    /// </summary>
    public enum SlotTypes {
        Common,
        WeaponLeft,
        WeaponRight,
        Health
    }

    /// <summary>
    /// Slot Interface
    /// </summary>
	public interface ISlot {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the slot. Multiple slots may have the same type
        /// </summary>
        SlotTypes Type {
            get;
        }

        /// <summary>
        /// The hotkey for the slot. Might be left empty
        /// </summary>
        string Hotkey {
            get;
        }

        /// <summary>
        /// The text game object for hotkey displayment
        /// </summary>
        GameObject HotkeyGameObject {
            get;
        }

        /// <summary>
        /// The box of the slot
        /// </summary>
        GameObject BoxGameObject {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}