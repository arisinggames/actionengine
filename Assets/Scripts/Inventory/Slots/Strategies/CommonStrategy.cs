﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Inventory.Slots.Strategies {
    /// <summary>
    /// Handle common slot elements. This means: look for an empty slot in the
    /// inventory, add the icon and that's it
    /// </summary>
    class CommonStrategy : IInventoryStrategy {
        #region PRIVATE_FIELDS
        /// <summary>
        /// Reference to the inventory manager
        /// </summary>
        private InventoryManager _manager;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        public CommonStrategy(InventoryManager manager) {
            _manager = manager;
        }

        /// <summary>
        /// Add an item (ICollectable) to the inventory
        /// </summary>
        /// <param name="sender"></param>
        public void Add(object sender) {
            ICommonCollectable c = sender as ICommonCollectable;
            GameObject box = FindEmptyBox(_manager.Slots);
            if (box != null) {
                GameObject go = _manager.CreateDefaultItem(box.transform, c.Type.ToString());
                Image renderer = go.GetComponent(typeof(Image)) as Image;
                if (renderer == null)
                    throw new ComponentNotFoundException("Image", _manager.DefaultItemGameObject);

                renderer.sprite = c.Icon;
            }
        }

        /// <summary>
        /// Activate the item in the inventory: consume potion, activate sword...
        /// </summary>
        /// <param name="slot"></param>
        public void Use(ISlot slot) {
            throw new global::System.NotImplementedException();
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Find the box of an empty slot in the slot list of the slot type "common"
        /// </summary>
        /// <returns></returns>
        private GameObject FindEmptyBox(List<GameObject> slots) {
            for (int i = 0; i < slots.Count; i++) {
                ISlot slot = slots[i].GetComponent(typeof(ISlot)) as ISlot;
                // check if the slot gameobject is of the type "common"
                // and if the box does NOT have an item
                if (slot != null && 
                    slot.Type == SlotTypes.Common &&
                    slot.BoxGameObject.transform.childCount == 0) {
                    return slot.BoxGameObject;
                }
            }
            return null;
        }
        #endregion PRIVATE_METHODS
    }
}
