﻿namespace ArisingGames.Assets.Scripts.Inventory.Slots.Strategies {
    /// <summary>
    /// Any kind of bullet that is consumable by weapons
    /// </summary>
    public enum SlotStrategies {
        Ammo,
        Common,
        Health,
        Weapon
    }

    /// <summary>
    /// Inventory strategy to handle the events of the various slot types
    /// </summary>
	public interface IInventoryStrategy {
        #region PUBLIC_METHODS
        /// <summary>
        /// Add an item (ICollectable) to the inventory
        /// </summary>
        /// <param name="sender"></param>
        void Add(object sender);

        /// <summary>
        /// Activate the item in the inventory: consume potion, activate sword...
        /// </summary>
        /// <param name="slot"></param>
        void Use(ISlot slot);
        #endregion PUBLIC_METHODS
    }
}