﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Inventory.Slots.Strategies {
    /// <summary>
    /// Handle weapons: add them to the first or second slot, 
    /// replace them with new weapons
    /// </summary>
    class WeaponStrategy : IInventoryStrategy {
        #region CONSTANTS
        /// <summary>
        /// To store which weapon(hand) is active
        /// </summary>
        private enum ActiveHand {
            None,
            Left,
            Right
        }
        #endregion CONSTANTS

        #region PRIVATE_FIELDS
        /// <summary>
        /// Which hand is active
        /// </summary>
        private ActiveHand _active;

        /// <summary>
        /// The weapon in the right hand
        /// </summary>
        private WeaponTypes _left;

        /// <summary>
        /// The weapon in the right hand
        /// </summary>
        private WeaponTypes _right;

        /// <summary>
        /// The "stack" contains the number of the remaining ammo
        /// 
        /// The text object is set if a weapon is added to the right
        /// or left box that is an AmmoConsumer and it will be removed
        /// if the weapon is replaced inside the box
        /// 
        /// The fields are simply for caching and to avoid searching
        /// for the stack Text on every shot
        /// </summary>
        private Text _stackLeftBox;
        private Text _stackRightBox;

        /// <summary>
        /// Reference to the inventory manager
        /// </summary>
        private readonly InventoryManager _manager;

        /// <summary>
        /// Cache the weapon manager
        /// </summary>
        private readonly WeaponManager _weaponManager;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        public WeaponStrategy(InventoryManager manager) {
            _active = ActiveHand.None;
            _manager = manager;
            _weaponManager = _manager.WeaponManagerGameObject.GetComponent(typeof(WeaponManager)) as WeaponManager;

            InitEventListeners();
        }

        /// <summary>
        /// If player hasn't collected so far any weapon add it to the left hand
        /// </summary>
        /// <param name="sender"></param>
        public void Add(object sender) {
            IWeaponCollectable w = sender as IWeaponCollectable;

            if (_active == ActiveHand.None)
                AddWeapon(SlotTypes.WeaponLeft, w);
            else if (IsWeaponSlotEmpty(SlotTypes.WeaponRight)) {
                AddWeapon(SlotTypes.WeaponRight, w);
                SwapBoxActivityImage(SlotTypes.WeaponLeft);
            } else
                ReplaceWeapon(w);
        }

        /// <summary>
        /// Activate the item in the inventory: consume potion, activate sword...
        /// </summary>
        /// <param name="slot"></param>
        public void Use(ISlot slot) {
            // WeaponStrategy.Use() will be called if the user wants to switch 
            // the weapon
            // but this will only be allowed if the alternate weapon slot contains a weapon
            if (_active == ActiveHand.None)
                return;
            if (_active == ActiveHand.Left && 
                _right == WeaponTypes.None)
                return;
            if (_active == ActiveHand.Right && 
                _left == WeaponTypes.None)
                return;

            // 1st step: display all weapons as inactive
            DeactivateWeaponSlots();
            
            // 2nd step: as it is a weapon change the image of the box 
            // to mark it as active
            ActivateWeaponSlot(slot);
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Handle the collected events
        /// </summary>
        private void InitEventListeners() {
            WeaponManager.Consumed += (sender, e) => {
                // update the number of ammo inside the slot
                if (_left == e.WeaponType && 
                    _stackLeftBox != null)
                    _stackLeftBox.text = e.Size.ToString();
                else if (_right == e.WeaponType && 
                         _stackRightBox != null)
                    _stackRightBox.text = e.Size.ToString();
            };
        }

        /// <summary>
        /// Find the box for a weapon type
        /// </summary>
        /// <returns></returns>
        private GameObject FindSlotByWeapon(List<GameObject> slots, SlotTypes type) {
            for (int i = 0; i < slots.Count; i++) {
                ISlot slot = slots[i].GetComponent(typeof(ISlot)) as ISlot;
                if (slot != null &&
                    slot.Type == type) {
                    return slot.BoxGameObject;
                }
            }
            return null;
        }

        /// <summary>
        /// If no weapon is active create the first one and
        /// assign it to the left hand
        /// </summary>
        /// <param name="slotType"></param>
        /// <param name="w"></param>
        private void AddWeapon(SlotTypes slotType, IWeaponCollectable w) {
            GameObject box = FindBox(_manager.Slots, slotType);
            if (box != null) {
                GameObject go = CreateItem(box, w);

                SwapBoxActivityImage(box);

                if (slotType == SlotTypes.WeaponLeft) {
                    _active = ActiveHand.Left;
                    _left = w.Type;
                    CacheOrClearStack(go, ActiveHand.Left);
                } else if (slotType == SlotTypes.WeaponRight) {
                    _active = ActiveHand.Right;
                    _right = w.Type;
                    CacheOrClearStack(go, ActiveHand.Right);
                }

                _weaponManager.ChangeWeapon(w.Type);
            }
        }

        /// <summary>
        /// Try to cache the Stack Text object of the box
        /// if the box doesnt contain a Text Object clear
        /// the stack cache
        /// </summary>
        /// <param name="box"></param>
        /// <param name="active"></param>
        private void CacheOrClearStack(GameObject box, ActiveHand active) {
            Text stack = box.GetComponentInChildren(typeof(Text)) as Text;
            if (active == ActiveHand.Left)
                _stackLeftBox = stack;
            else
                _stackRightBox = stack;
        }

        /// <summary>
        /// Based on the field "IsAmmoDiplayed" create a stackable item or a default on
        /// </summary>
        /// <param name="box"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        private GameObject CreateItem(GameObject box, IWeaponCollectable w) {
            GameObject go;
            if (w.ShowAmmo) {
                int size = _weaponManager.GetRemainingAmmo(w.Type);
                go = _manager.CreateStackableItem(box.transform, w.Type.ToString(), size);
            } else
                go = _manager.CreateDefaultItem(box.transform, w.Type.ToString());

            Image renderer = go.GetComponent(typeof(Image)) as Image;
            if (renderer == null)
                throw new ComponentNotFoundException("Image", _manager.DefaultItemGameObject);

            renderer.sprite = w.Icon;

            return go;
        }

        /// <summary>
        /// Based on the weapon state show another background image in the box
        /// </summary>
        /// <param name="box"></param>
        private void SwapBoxActivityImage(GameObject box) {
            Image renderer = box.GetComponent(typeof(Image)) as Image;
            if (renderer == null)
                throw new ComponentNotFoundException("Image", _manager.DefaultItemGameObject);

            if (renderer.sprite == _manager.BoxActivated)
                renderer.sprite = _manager.BoxDeactivated;
            else
                renderer.sprite = _manager.BoxActivated;
        }

        /// <summary>
        /// Based on slot type show another background image in the box
        /// </summary>
        /// <param name="slotType"></param>
        private void SwapBoxActivityImage(SlotTypes slotType) {
            GameObject box = FindBox(_manager.Slots, slotType);
            SwapBoxActivityImage(box);
        }

        /// <summary>
        /// Swap the active hand
        /// </summary>
        private void SwapActiveHand() {
            if (_active == ActiveHand.Left) {
                _active = ActiveHand.Right;
                _weaponManager.ChangeWeapon(_right);
            } else if (_active == ActiveHand.Right) {
                _active = ActiveHand.Left;
                _weaponManager.ChangeWeapon(_left);
            }
        }

        /// <summary>
        /// If a weapon has been collected and the active hand contains already one - 
        /// drop it
        /// </summary>
        /// <param name="w"></param>
        private void ReplaceWeapon(IWeaponCollectable w) {
            SlotTypes slotType;
            if (_active == ActiveHand.Left) {
                slotType = SlotTypes.WeaponLeft;
                _left = w.Type;
            } else {
                slotType = SlotTypes.WeaponRight;
                _right = w.Type;
            }
                
            GameObject box = FindBox(_manager.Slots, slotType);

            if (box != null) {
                _manager.DestroyItem(box.transform);
                GameObject go = CreateItem(box, w);

                if (_active == ActiveHand.Left)
                    CacheOrClearStack(go, ActiveHand.Left);
                else
                    CacheOrClearStack(go, ActiveHand.Right);

                _weaponManager.ChangeWeapon(w.Type);
            }
        }

        /// <summary>
        /// Check if the right weapon slot contains a weapon
        /// </summary>
        /// <param name="slotType"></param>
        /// <returns></returns>
        private bool IsWeaponSlotEmpty(SlotTypes slotType) {
            GameObject box = FindBox(_manager.Slots, slotType);

            if (box != null && 
                box.transform.childCount == 1)
                return false;

            return true;
        }

        /// <summary>
        /// Temporarily deactivate all weapon slots
        /// </summary>
        private void DeactivateWeaponSlots() {
            for (int i = 0; i < _manager.Slots.Count; i++) {
                GameObject go = _manager.Slots[i];
                ISlot slot = go.GetComponent(typeof(ISlot)) as ISlot;
                if (slot == null)
                    throw new ComponentNotFoundException("ISlot", go);

                Image image = slot.BoxGameObject.GetComponent(typeof(Image)) as Image;
                if (image != null)
                    image.sprite = _manager.BoxDeactivated;
            }
        }

        /// <summary>
        /// Display the selected weapon slot as activated
        /// </summary>
        /// <param name="slot"></param>
        private void ActivateWeaponSlot(ISlot slot) {
            Image image = slot.BoxGameObject.GetComponent(typeof(Image)) as Image;
            if (image != null)
                image.sprite = _manager.BoxActivated;

            SwapActiveHand();
        }

        /// <summary>
        /// Find the left or right hand box
        /// </summary>
        /// <todo>Merge with PotionSlotStratedy.FindBox()</todo>
        /// <returns></returns>
        private GameObject FindBox(List<GameObject> slots, SlotTypes type) {
            for (int i = 0; i < slots.Count; i++) {
                ISlot slot = slots[i].GetComponent(typeof(ISlot)) as ISlot;
                if (slot != null &&
                    slot.Type == type) 
                    return slot.BoxGameObject;
            }
            return null;
        }
        #endregion PRIVATE_METHODS
    }
}