﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;
using UnityEngine.UI;

namespace ArisingGames.Assets.Scripts.Inventory.Slots.Strategies {
    /// <summary>
    /// Handle potions: stack potions to the right slot, consume a potion
    /// </summary>
    class PotionStrategy : IInventoryStrategy {
        #region CONSTANTS
        /// <summary>
        /// The number of potions at the first collection
        /// </summary>
        private const int StartSize = 1;
        #endregion CONSTANTS

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: potion has been consumed
        /// </summary>
        public delegate void PotionConsumedHandler(object sender, PotionConsumedEventArgs e);
        public static event PotionConsumedHandler Consumed;
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// Mapping between potion types and slot types
        /// </summary>
        private readonly Dictionary<PotionTypes, SlotTypes> _potionsToSlots;

        /// <summary>
        /// Reference to the inventory manager
        /// </summary>
        private InventoryManager _manager;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        public PotionStrategy(InventoryManager manager) {
            _manager = manager;
            _potionsToSlots = new Dictionary<PotionTypes, SlotTypes>();
            _potionsToSlots.Add(PotionTypes.Health, SlotTypes.Health);
        }

        /// <summary>
        /// Add an item (ICollectable) to the inventory
        /// </summary>
        /// <param name="sender"></param>
        public void Add(object sender) {
            IPotionCollectable c = sender as IPotionCollectable;
            SlotTypes slotType = _potionsToSlots[c.Type];
            GameObject box = FindBox(_manager.Slots, slotType);
            if (box != null) {
                // if the box has no item - create one, otherwise increase
                // the stack number
                if (box.transform.childCount == 0) {
                    GameObject go = _manager.CreateStackableItem(box.transform, c.Type.ToString(), StartSize);
                    Image renderer = go.GetComponent(typeof(Image)) as Image;
                    if (renderer == null)
                        throw new ComponentNotFoundException("Image", _manager.DefaultItemGameObject);

                    renderer.sprite = c.Icon;
                } else {
                    Text stack = box.GetComponentInChildren(typeof(Text)) as Text;
                    if (stack == null)
                        throw new Exception("The stackItem prefab does not implement a text component");
                    int tmpStackNumber = int.Parse(stack.text) + 1;
                    stack.text = tmpStackNumber.ToString();
                }
            }
        }

        /// <summary>
        /// Activate the item in the inventory: consume potion, activate sword...
        /// </summary>
        /// <param name="slot"></param>
        public void Use(ISlot slot) {
            // to consume a potion: due the type of slot the potion type is known
            // and with the text displayed in the slot also the number of potions
            // 1st: get the potion type
            PotionTypes type = RetrievePotionType(slot.Type);

            // 2nd: get the number of potions
            Text stack = slot.BoxGameObject.GetComponentInChildren(typeof(Text)) as Text;
            if (stack == null)
                throw new Exception("The stackItem prefab does not implement a text component");
            int size = int.Parse(stack.text);
            if (size > 0) {
                PotionConsumedEventArgs args = new PotionConsumedEventArgs(type, size);
                OnPotionConsumed(args);

                size--;
                // if there is no potion left - destroy the item
                if (size <= 0)
                    _manager.DestroyItem(slot.BoxGameObject.transform);
                else 
                    stack.text = size.ToString();
            }
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Find the box for the potion based on a potion type
        /// </summary>
        /// <returns></returns>
        private GameObject FindBox(List<GameObject> slots, SlotTypes type) {
            for (int i = 0; i < slots.Count; i++) {
                ISlot slot = slots[i].GetComponent(typeof(ISlot)) as ISlot;
                if (slot != null &&
                    slot.Type == type) {
                    return slot.BoxGameObject;
                }
            }
            return null;
        }

        /// <summary>
        /// Retrieve the potion type for a slot type
        /// </summary>
        /// <returns></returns>
        private PotionTypes RetrievePotionType(SlotTypes type) {
            foreach (var pair in _potionsToSlots) {
                if (type == pair.Value)
                    return pair.Key;
            }
            throw new Exception("Slot type does not exist in _potionsToSlots");
        }

        /// <summary>
        /// Raise the event
        /// </summary>
        protected void OnPotionConsumed(PotionConsumedEventArgs args) {
            // Check if there are any Subscribers
            if (Consumed != null)
                Consumed(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}
