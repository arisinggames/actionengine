﻿namespace ArisingGames.Assets.Scripts.Inventory.Slots {
    /// <summary>
    /// Slot containing by default all kind of items (potions, weapons, common 
    /// items) as long as they don't need any special treatment and must
    /// therefore be inherited
    /// </summary>
    public class DefaultSlot : AbstractSlot, ISlot {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the slot. Multiple slots may have the same type
        /// </summary>
        public SlotTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The button name for the slot as assigned in the input manager. 
        /// Might be left empty
        /// </summary>
        public string Hotkey {
            get { return _hotkey; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}