﻿using System;
using ArisingGames.Assets.Scripts.Owner;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the health changed event
    /// </summary>
    public class HealthChangedEventArgs : EventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The new size of health
        /// </summary>
        public float Size {
            get { return _size; }
        }

        /// <summary>
        /// The owner of the health attribute
        /// </summary>
        public IOwner Owner {
            get { return _owner; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// The new size of health
        /// </summary>
        private float _size;

        /// <summary>
        /// The owner of the health attribute
        /// </summary>
        private IOwner _owner;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size"></param>
        /// <param name="owner"></param>
        public HealthChangedEventArgs(float size, IOwner owner) {
            _size = size;
            _owner = owner;
        }
        #endregion PUBLIC_METHODS
    }
}