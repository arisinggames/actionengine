﻿using System;
using ArisingGames.Assets.Scripts.Owner;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the death event
    /// </summary>
    public class DeathEventArgs : EventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The owner of the health attribute
        /// </summary>
        public IOwner Owner {
            get { return _owner; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// The owner of the health attribute
        /// </summary>
        private IOwner _owner;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="owner"></param>
        public DeathEventArgs(IOwner owner) {
            _owner = owner;
        }
        #endregion PUBLIC_METHODS
    }
}