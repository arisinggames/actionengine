﻿using System;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the gold changed event
    /// </summary>
    public class GoldChangedEventArgs : EventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The new amout of gold
        /// </summary>
        public int Amount {
            get { return _amount; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// The new amout of gold
        /// </summary>
        private int _amount;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="amount"></param>
        public GoldChangedEventArgs(int amount) {
            _amount = amount;
        }
        #endregion PUBLIC_METHODS
    }
}