﻿using System;
using ArisingGames.Assets.Scripts.Collectables;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the potion consumed event
    /// </summary>
    public class PotionConsumedEventArgs : EventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The size determines how much the attribute affected by the 
        /// potion will be increased
        /// </summary>
        public int Size {
            get { return _size; }
        }

        /// <summary>
        /// The potion type
        /// </summary>
        public PotionTypes Type {
            get { return _type; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// The potion type
        /// </summary>
        private PotionTypes _type;

        /// <summary>
        /// The size determines how much the attribute affected by the 
        /// potion will be increased
        /// </summary>
        private int _size;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="size"></param>
        public PotionConsumedEventArgs(PotionTypes type, int size) {
            _type = type;
            _size = size;
        }
        #endregion PUBLIC_METHODS
    }
}