﻿using System;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// Instead of passing EventsArgs.Empty Microsoft recommends
    /// to use an EventArgs object with no params so it
    /// can be extended later
    /// </summary>
    public class ItemCollectedEventArgs : EventArgs { }
}