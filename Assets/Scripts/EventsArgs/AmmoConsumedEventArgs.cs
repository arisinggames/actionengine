﻿using System;
using ArisingGames.Assets.Scripts.Collectables;
using ArisingGames.Assets.Scripts.Weapons;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the ammo consumed event
    /// </summary>
    public class AmmoConsumedEventArgs : EventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The new size of the ammo
        /// </summary>
        public int Size {
            get { return _size; }
        }

        /// <summary>
        /// The ammo type
        /// </summary>
        public AmmoTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The weapon type
        /// </summary>
        public WeaponTypes WeaponType {
            get { return _weaponType; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// The ammo type
        /// </summary>
        private AmmoTypes _type;

        /// <summary>
        /// The new size of the ammo
        /// </summary>
        private int _size;

        /// <summary>
        /// The weapon type
        /// </summary>
        private WeaponTypes _weaponType;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="weaponType"></param>
        /// <param name="size"></param>
        public AmmoConsumedEventArgs(AmmoTypes type, WeaponTypes weaponType, int size) {
            _type = type;
            _size = size;
            _weaponType = weaponType;
        }
        #endregion PUBLIC_METHODS
    }
}