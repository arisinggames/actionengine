﻿using ArisingGames.Assets.Scripts.Inventory.Slots.Strategies;

namespace ArisingGames.Assets.Scripts.EventsArgs {
    /// <summary>
    /// The arguments for the item collected event
    /// The most important:
    /// Strategy must be specified to select the correct strategy in
    /// the inventory manager to process the item
    /// </summary>
    public class AmmoCollectedEventArgs : ItemCollectedEventArgs {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// _strategy must be specified to select the correct strategy in
        /// the inventory manager
        /// </summary>
        public SlotStrategies Strategy {
            get { return _strategy; }
        }

        /// <summary>
        /// The number of bullets the weapon will be increased by
        /// </summary>
        public int Size {
            get { return _size; }
        }
        #endregion

        #region PRIVATE_FIELDS        
        /// <summary>
        /// _strategy must be specified to select the correct strategy in
        /// the inventory manager
        /// </summary>
        private SlotStrategies _strategy;

        /// <summary>
        /// The number of bullets the weapon will be increased by
        /// </summary>
        private int _size;
        #endregion PRIVATE_FIELDS

        #region PUBLIC_METHODS
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strategy"></param>
        /// <param name="size"></param>
        public AmmoCollectedEventArgs(SlotStrategies strategy, int size) {
            _strategy = strategy;
            _size = size;
        }
        #endregion PUBLIC_METHODS
    }
}
