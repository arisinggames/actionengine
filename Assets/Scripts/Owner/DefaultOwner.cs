﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Owner {
    /// <summary>
    /// Default implementation of IOwner to assign gameobjects
    /// an owner that have no other mono behaviour
    /// </summary>
	public class DefaultOwner : MonoBehaviour, IOwner {
        #region CONFIGURATION
        /// <summary>
        /// The owner id
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("The owner id")]
        [SerializeField]
        private Owners _ownerId;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The owner id
        /// </summary>
        [SerializeField]
        public Owners OwnerId {
            get { return _ownerId; }
        }
        #endregion PROPERTIES
    }
}