﻿namespace ArisingGames.Assets.Scripts.Owner {
    /// <summary>
    /// All the owners that are used in the game
    /// </summary>
    public enum Owners {
        None,
        Player,
        Slime,
        Tree
    }

    /// <summary>
    /// IOwner is a replacement for the tag system
    /// 
    /// At the moment of creation the Death-Event must 
    /// ship with the information who has died: the player,
    /// a tree, an enemy etc.
    /// There are various possibilities to assign an id to 
    /// an object:
    /// - somewhere a string field ... not a good idea, as
    /// it is "somewhere" & it isn't sure that the field is
    /// available and set!
    /// - use for identification a tag. Also not a good idea:
    /// tags might change and it's very easy to forget to
    /// change the tag name inside the code. 
    /// A comparison for the tag "Player" is hard coded!
    /// So it would have to changed in multiple places...
    /// - an interface with an enum solves this problem:
    /// the class that handles the death event can check
    /// if the emitter implements the "IOwner" interface
    /// and can throw an exception on failure. 
    /// To change the ID at a central place and support
    /// intellisense an enum comes to the rescue
    /// </summary>
    public interface IOwner {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The id of the owner
        /// </summary>
        Owners OwnerId {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}