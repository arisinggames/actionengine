﻿using System;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Exceptions {
    [Serializable]
    public class InspectorMissingReferenceException : Exception {
        #region PUBLIC_METHODS
        public InspectorMissingReferenceException() { }

        public InspectorMissingReferenceException(string fieldName, MonoBehaviour owner)
            : base(BuildMessage(fieldName, owner)) { }

        public InspectorMissingReferenceException(string message, Exception innerException)
            : base(message, innerException) { }

        public InspectorMissingReferenceException(string message)
            : base(message) { }

        private static string BuildMessage(string fieldName, MonoBehaviour obj) {
            string msg = "The class field \"{0}\" is a reference in the " +
                         "class \"{1}\", assigned to the GameObject \"{2}\" " +
                         "and must not be null (it must be set via the inspector)";
            return string.Format(msg, fieldName, obj.GetType().Name, obj.name);
        }
        #endregion PUBLIC_METHODS
    }
}