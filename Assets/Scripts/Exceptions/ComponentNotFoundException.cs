﻿using System;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Exceptions {
    [Serializable]
    public class ComponentNotFoundException : Exception {
        public ComponentNotFoundException() { }

        public ComponentNotFoundException(string component, GameObject go)
            : base(BuildMessage(component, go)) { }

        public ComponentNotFoundException(string component, MonoBehaviour go)
            : base(BuildMessage(component, go)) { }

        public ComponentNotFoundException(string message)
            : base(message) { }

        public ComponentNotFoundException(string message, Exception innerException)
            : base(message, innerException) { }

        private static string BuildMessage(string component, GameObject go) {
            return string.Format("The GameObject \"{1}\" does not implement the component \"{0}\"", component, go.name);
        }

        private static string BuildMessage(string component, MonoBehaviour go) {
            return string.Format("The MonoBehaviour \"{1}\" does not implement the component \"{0}\"", component, go.name);
        }
    }
}
