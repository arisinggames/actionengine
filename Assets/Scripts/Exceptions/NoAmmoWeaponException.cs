﻿using System;
using ArisingGames.Assets.Scripts.Weapons;

namespace ArisingGames.Assets.Scripts.Exceptions {
    [Serializable]
    public class NoAmmoWeaponException : Exception {
        public NoAmmoWeaponException() { }

        public NoAmmoWeaponException(WeaponTypes w)
            : base(BuildMessage(w)) { }

        public NoAmmoWeaponException(string message)
            : base(message) { }

        public NoAmmoWeaponException(string message, Exception innerException)
            : base(message, innerException) { }

        private static string BuildMessage(WeaponTypes w) {
            return string.Format("The weapon \"{0}\" does not support consumeable ammo", w);
        }
    }
}
