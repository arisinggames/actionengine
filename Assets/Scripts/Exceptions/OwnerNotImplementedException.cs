﻿using System;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Exceptions {
    [Serializable]
    public class OwnerNotImplementedException : Exception {
        public OwnerNotImplementedException() { }

        public OwnerNotImplementedException(GameObject go)
            : base(BuildMessage(go)) { }

        public OwnerNotImplementedException(string message)
            : base(message) { }

        public OwnerNotImplementedException(string message, Exception innerException)
            : base(message, innerException) { }

        private static string BuildMessage(GameObject go) {
            return string.Format("The gameobject \"{0}\" does not implement IOwner", go.name);
        }
    }
}
