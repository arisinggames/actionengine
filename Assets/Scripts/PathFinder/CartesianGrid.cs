﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Map;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ArisingGames.Assets.Scripts.PathFinder {
    /// <summary>
    /// Erklären was das soll:
    /// basierend auf Sebastian Lange
    /// Das Action Template verwendet wie Unity als Grid ein kartesisches
    /// doch EF ein anderes 
    /// 
    /// Warum nicht BasicGrid von EF erweitern? 
    /// Weil dieses kartesisches Grid nicht zwingend für Pathfinding verwendet werden muss
    /// und z.B. wird ein anderer Serarch Algo eingesetzt der dann wieder ein kartesisches
    /// System erfordert
    /// 
    /// Vielleicht ein readme.md?
    /// Erwähnen: Basiert auf Sebastians Code
    /// </summary>

    public class CartesianGrid : MonoBehaviour {
        #region REFERENCES

        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// Gameobject containing the character boundaries.
        /// Used to clamp the waypoints to the map boundaries
        /// </summary>
        [Tooltip("Gameobject containing the character boundaries. " +
                 "Used to clamp the grid to the map boundaries")]
        [AssertReference]
        [SerializeField]
        private GameObject _characterBoundariesGameObject;
#pragma warning restore 0649

        #endregion REFERENCES


        public float gridWorldMaxX;
        public Vector2 worldTopRight;

        public LayerMask unwalkableMask;
        public Vector2 gridWorldSize;
        public float nodeRadius;
        Node[,] grid;

        public int finalX;

        public Vector2 lo;

        public Node[,] Grid {
            get { return grid; }
        }

        public bool inside;


        float nodeDiameter;
        int width, height;

        public int Width {
            get { return width; }
        }
        public int Height {
            get { return height; }
        }

        public GameObject Blocked;
        public GameObject Walkable;

        public bool drawGrid = true;
        Vector3 worldBottomLeft;

        /// <summary>
        /// Boundary configuration in game object "_characterBoundariesGameObject"
        /// </summary>
        private MapBoundaries _boundaries;



        void Awake() {
            CacheComponents();

            worldBottomLeft = new Vector3(_boundaries.XMin, _boundaries.YMin);

            nodeDiameter = nodeRadius * 2;
            width = Mathf.RoundToInt(gridWorldSize.x/nodeDiameter);
            height = Mathf.RoundToInt(gridWorldSize.y/nodeDiameter);
            CreateGrid();
            DrawGrid();
        }

        public Node GetNodeByGridPos(int x, int y) {
            int newY = height - y - 1;
            return grid[x, newY];
        }

        /// <summary>
        /// Draw grid - but only in debug mode
        /// </summary>
        public void DrawGrid() {
#if DEBUG
            foreach (Transform child in transform)
                Destroy(child.gameObject);

            if (drawGrid && grid != null)
                foreach (Node n in grid) {
                    GameObject go;
                    if (n.walkable)
                        go = Instantiate(Walkable);
                    else
                        go = Instantiate(Blocked);

                    go.transform.position = n.worldPosition;
                    go.transform.localScale = Vector3.one * (nodeDiameter - .1f);
                    go.transform.parent = transform;
                    go.name = go.name + " x:" + n.gridX + ",y:" + n.gridY;
                    go.SetActive(enabled);
                }
#endif
        }

        public void CreateGrid() {
            grid = new Node[width, height];
            //Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
            
            /////////// hier kommen map boundaries, oder ein neues GO A+ Boundaries
             

            float tpx = transform.position.x + worldBottomLeft.x + width* nodeDiameter;
            float tpy = transform.position.y + worldBottomLeft.y + height* nodeDiameter;
            worldTopRight = new Vector2(tpx, tpy);


            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Vector3 worldPoint = worldBottomLeft
                                         + Vector3.right*(x*nodeDiameter + nodeRadius)
                                         + Vector3.up*(y*nodeDiameter + nodeRadius);
                    bool walkable = !(Physics2D.OverlapCircle(worldPoint, nodeRadius, unwalkableMask));


                    grid[x, y] = new Node(walkable, worldPoint, x, y);
                }
            }
        }


        /// <summary>
        /// check if the player is inside the grid
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public bool IsInsideGrid(Vector3 worldPosition) {
            if (worldPosition.x >= worldBottomLeft.x &&
                worldPosition.y >= worldBottomLeft.y &&
                worldPosition.x <= worldTopRight.x &&
                worldPosition.y <= worldTopRight.y) {
                inside = true;
                return true;
            }
            inside = false;
            return false;
        }

        /// <summary>
        /// get the position of the player as local
        /// position inside the grid
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        Vector2 LocalPos(Vector2 worldPosition) {
            return worldPosition - (Vector2)worldBottomLeft;

        }

        /// <summary>
        /// Get a random node that is walkable
        /// </summary>
        /// <param name="currentPositon"></param>
        /// <returns></returns>
        public Node FindRandomWalkableNode(Vector3 currentPositon) {
            Node currentNode = RetrieveNode(currentPositon);

            while (true) {
                int x = Mathf.RoundToInt(Random.Range(0, width - 1));
                int y = Mathf.RoundToInt(Random.Range(0, height - 1));

                Node randomNode = grid[x, y];

                if (randomNode != null &&
                    randomNode.walkable &&
                    !randomNode.Equals(currentNode)) 
                    return randomNode;
            }
        }

        /// <summary>
        /// Check if the supplied world position is inside
        /// the grid and return on success the node
        /// from the cartesian grid
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public Node RetrieveNode(Vector3 worldPosition) {
            if (IsInsideGrid(worldPosition)) {
                lo = LocalPos(worldPosition);

                int x = Mathf.FloorToInt(lo.x / nodeDiameter);
                int y = Mathf.FloorToInt(lo.y / nodeDiameter);
                return grid[x, y];
            }

            string msg = string.Format(
                    "Node in the grid for world positon {0} not found. " +
                    "Is the world position (and with it the object) inside the grid?",
                    worldPosition);
            throw new Exception(msg);
        }

        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _boundaries = _characterBoundariesGameObject
                .GetComponent(typeof(MapBoundaries)) as MapBoundaries;
            if (_boundaries == null)
                throw new ComponentNotFoundException("MapBoundaries", _characterBoundariesGameObject);
        }
    }
}

