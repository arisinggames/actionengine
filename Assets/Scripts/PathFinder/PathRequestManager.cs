﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.PathFinder {
    public class PathRequestManager : MonoBehaviour {

        struct PathRequest {
            public Vector3 start;
            public Vector3 target;
            public Action<Vector3[]> callback;

            public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[]> _callback) {
                start = _start;
                target = _end;
                callback = _callback;
            }
        }

        Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
        PathRequest currentPathRequest;

        static PathRequestManager instance;

        bool isProcessingPath;

        public GameObject proxyGameObject;
        private JumpPointPathProxy proxy;

        void Awake() {
            instance = this;

            // init componenten und sicherstellen, dass sie da sind ... klar

            proxy = proxyGameObject.GetComponent<JumpPointPathProxy>();
        }

        public static void RequestJumpPointPath(Vector3 start, Vector3 target, Action<Vector3[]> callback) {
            PathRequest newRequest = new PathRequest(start, target, callback);
            instance.pathRequestQueue.Enqueue(newRequest);
            instance.TryProcessNext();
        }

        private void TryProcessNext() {
            if (!isProcessingPath && pathRequestQueue.Count > 0) {
                currentPathRequest = pathRequestQueue.Dequeue();
                isProcessingPath = true;
                proxy.StartFindPath(currentPathRequest.start, currentPathRequest.target, FinishProcessingPathCallback);
            }
        }

        public void FinishProcessingPathCallback(Vector3[] path) {
            currentPathRequest.callback(path);
            isProcessingPath = false;
            TryProcessNext();
        }
    }
}
