﻿using System;
using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Vendor.EpPathFinding.cs.PathFinder;
using ArisingGames.Assets.Vendor.EpPathFinding.cs.PathFinder.Grid;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.PathFinder {
    public class JumpPointPathProxy : MonoBehaviour {

        /// <summary>
        /// The gameobject containing the grid for the pathfinding
        /// </summary>
        [AssertReference]
        [Tooltip("The gameobject containing the grid for the pathfinding")]
        [SerializeField]
        public GameObject _cartesianGridGameObject; 

        private JumpPointParam jpParam;

        private CartesianGrid _cartesianGrid;
        private BaseGrid jumpPointSearchGrid;
        private Action<Vector3[]> _callback;
        private List<GridPos> matrixWaypoints;
        private bool _isProcessing;

        void Awake() {
            _cartesianGrid = _cartesianGridGameObject
                .GetComponent(typeof(CartesianGrid)) as CartesianGrid;

            InitjumpPointSearchMatrix();
        }

        void InitjumpPointSearchMatrix() {
            bool[][] matrix = BuildEfMoveableMatrix();
            jumpPointSearchGrid = new StaticGrid(_cartesianGrid.Width, _cartesianGrid.Height, matrix);
        }

        /// <summary>
        /// The EF Pathfinder requires a grid and a matrix with
        /// the grid tiles that are walkable
        /// But in contrary to the grid created by CreateGrid()
        /// EF pathfinder starts counting from the top to the bottom
        /// This means the row with the highest number in the 
        /// "source grid" is the lowest number (= 0) in the EF 
        /// Pathfinder grid
        /// </summary>
        bool[][] BuildEfMoveableMatrix() {

            bool[][] matrix = new bool[_cartesianGrid.Width][];
            for (int i = 0; i < _cartesianGrid.Width; i++)
                matrix[i] = new bool[_cartesianGrid.Height];

            Node[,] grid = _cartesianGrid.Grid;

            for (int i = 0; i < grid.GetLength(0); i++) 
                for (int j = 0; j < grid.GetLength(1); j++) {
                    Node n = grid[i, j];

                    int x = n.gridX;
                    int y = _cartesianGrid.Height - n.gridY - 1;

                    if (n.walkable)
                        matrix[x][y] = true;
                    else
                        matrix[x][y] = false;
                }

            return matrix;
        }

        public void StartFindPath(Vector3 startPos, Vector3 targetPos, Action<Vector3[]> callback) {
            _callback = callback;

            // pos konvertieren
            GridPos start = ConvertWorldPosGridPos(startPos);
            GridPos target = ConvertWorldPosGridPos(targetPos);

            // restart the finder process
            _isProcessing = true;
            matrixWaypoints = null;

            if (jpParam == null)
                jpParam = new JumpPointParam(jumpPointSearchGrid, start, target);
            else 
                jpParam.Reset(start, target);
            matrixWaypoints = JumpPointFinder.FindPath(jpParam);

            // ...Update() tracks the process and as soon as the
            // path list is filled convert it to a list of vector 
            // and cancel the processing
        }


        /// <summary>
        /// Check if the matrixWaypoints has been populated.
        /// In this case convert it to a list of vectors, cancel the processing 
        /// and return the result to the caller
        /// </summary>
        void Update() {
            if (matrixWaypoints != null && _isProcessing) {
                Vector3[] path = ConvertGridPosToVectorPath();

                _isProcessing = true;
                matrixWaypoints = null;

                // finish the processing of the path
                _callback(path);
            }
        }

        private Vector3[] ConvertGridPosToVectorPath() {
            Vector3[] path = new Vector3[matrixWaypoints.Count];

            for (int i = 0; i < matrixWaypoints.Count; i++) {
                GridPos waypoint = matrixWaypoints[i];
                Node n = _cartesianGrid.GetNodeByGridPos(waypoint.x, waypoint.y);
                path[i] = n.worldPosition;
            }

            return path;
        }

        private Vector3[] ConvertGridPosToVectorPath(List<GridPos> waypoints) {
            Vector3[] path = new Vector3[waypoints.Count];

            for (int i = 0; i < waypoints.Count; i++) {
                GridPos waypoint = waypoints[i];
                Node n = _cartesianGrid.GetNodeByGridPos(waypoint.x, waypoint.y);
                path[i] = n.worldPosition;
            }

            return path;
        }

        /// <summary>
        /// Convert a world position into a jump point search matrix position 
        /// This means:
        /// - get the node from the original CartesianGrid 
        /// - convert the coordinates from the node into the jump point search matrix
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        GridPos ConvertWorldPosGridPos(Vector3 pos) {
            Node n = _cartesianGrid.RetrieveNode(pos);

            GridPos gridPos = new GridPos(n.gridX, _cartesianGrid.Height - n.gridY - 1);
            return gridPos;
        }
    }
}
