﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Abstract collectable
    /// </summary>
	public abstract class AbstractCollectable : MonoBehaviour {
        #region MONO_EVENTS
        /// <summary>
        /// Create an event that the item has been collected
        /// and destroy it
        /// </summary>s
        /// <param name="c"></param>
        protected virtual void OnTriggerEnter2D(Collider2D c) {
            OnItemCollected();
            Destroy(gameObject);
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Raise the event
        /// </summary>
        protected abstract void OnItemCollected();
        #endregion PRIVATE_METHODS
    }
}