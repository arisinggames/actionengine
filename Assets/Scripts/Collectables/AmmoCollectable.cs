﻿using System;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Weapons;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// One or more bullets of a weapon
    /// </summary>
	public class AmmoCollectable : AbstractCollectable, IAmmoCollectable {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The ammo type
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("The ammo type")]
        private AmmoTypes _type;

        /// <summary>
        /// The weapon type
        /// </summary>
        [SerializeField]
        [Tooltip("The weapon type")]
        private WeaponTypes _weaponType;

        /// <summary>
        /// The number of bullets the weapon will be increased by
        /// </summary>
        [SerializeField]
        [Tooltip("The number of bullets the weapon will be increased by")]
        private int _size;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: Item has been collected
        /// </summary>
        public delegate void AmmoCollectedHandler(object sender, ItemCollectedEventArgs e);
        public static event AmmoCollectedHandler Collected;

        /// <summary>
        /// The type of the ammo. Multiple ammo gameobjects may have the same
        /// type
        /// </summary>
        public AmmoTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The weapon type
        /// </summary>
        public WeaponTypes WeaponType {
            get { return _weaponType; }
        }

        /// <summary>
        /// The number of bullets the weapon will be increased by
        /// </summary>
        public int Size {
            get { return _size; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_METHODS
        /// <summary>
        /// Raise the event
        /// </summary>
        protected override void OnItemCollected() {
            ItemCollectedEventArgs args = new ItemCollectedEventArgs();
            // Check if there are any Subscribers
            if (Collected != null)
                Collected(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}