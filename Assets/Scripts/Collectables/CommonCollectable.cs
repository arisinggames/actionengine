﻿using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// A common collectable like a sheep with no specific rules, it will be simply added to
    /// the inventory
    /// </summary>
    public class CommonCollectable : AbstractCollectable, ICommonCollectable {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The type of the item
        /// </summary>
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("The type of the item")]
        private CommonCollectableTypes _type;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: Item has been collected
        /// </summary>
        public delegate void CommonCollectedHandler(object sender, ItemCollectedEventArgs e);
        public static event CommonCollectedHandler Collected;

        /// <summary>
        /// The type of the item
        /// </summary>
        public CommonCollectableTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The icon of the item
        /// </summary>
        public Sprite Icon {
            get { return _icon; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The icon of the item
        /// </summary>
        private Sprite _icon;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            SpriteRenderer r = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (r == null)
                throw new ComponentNotFoundException("SpriteRenderer", this);
            _icon = r.sprite;
        }

        /// <summary>
        /// Taise the event
        /// </summary>
        protected override void OnItemCollected() {
            ItemCollectedEventArgs args = new ItemCollectedEventArgs();
            // Check if there are any Subscribers
            if (Collected != null)
                Collected(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}