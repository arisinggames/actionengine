﻿using ArisingGames.Assets.Scripts.EventsArgs;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Treasures to increase the gold counter
    /// </summary>
	public class TreasureCollectable : AbstractCollectable, ITreasureCollectable {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The treasure value
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("The treasure value")]
        private int _value;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: Item has been collected
        /// </summary>
        public delegate void TreasureCollectedHandler(object sender, ItemCollectedEventArgs e);
        public static event TreasureCollectedHandler Collected;

        /// <summary>
        /// The treasure value
        /// </summary>
        public int Value {
            get { return _value; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_METHODS
        /// <summary>
        /// Raise the event
        /// </summary>
        protected override void OnItemCollected() {
            ItemCollectedEventArgs args = new ItemCollectedEventArgs();
            // Check if there are any Subscribers
            if (Collected != null)
                Collected(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}