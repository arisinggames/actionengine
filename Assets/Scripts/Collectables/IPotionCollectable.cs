﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Potion types like health, mana
    /// </summary>
    public enum PotionTypes {
        Health
    }

    /// <summary>
    /// Interface for potions
    /// </summary>
	public interface IPotionCollectable {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the potion. Multiple potion gameobjects may have the same
        /// type
        /// </summary>
        PotionTypes Type {
            get;
        }

        /// <summary>
        /// The size of the potion
        /// </summary>
        int Size {
            get;
        }

        /// <summary>
        /// The image of the collectable
        /// </summary>
        Sprite Icon {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}