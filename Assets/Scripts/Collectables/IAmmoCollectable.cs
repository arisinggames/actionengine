﻿using ArisingGames.Assets.Scripts.Weapons;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Any kind of bullet that is consumable by weapons
    /// </summary>
    public enum AmmoTypes {
        Arrow
    }

    /// <summary>
    /// Interface for ammo (bullets, rockets, arrows)
    /// </summary>
	public interface IAmmoCollectable {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the ammo. Multiple ammo gameobjects may have the same
        /// type
        /// </summary>
        AmmoTypes Type {
            get;
        }

        /// <summary>
        /// The type of the weapon
        /// </summary>
        WeaponTypes WeaponType {
            get;
        }

        /// <summary>
        /// The number of bullets the weapon will be increased by
        /// </summary>
        int Size {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}