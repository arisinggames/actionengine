﻿using System;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// A potion
    /// </summary>
	public class PotionCollectable : AbstractCollectable, IPotionCollectable {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The potion type
        /// </summary>
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("The potion type")]
        private PotionTypes _type;

        /// <summary>
        /// The size of the potion the _affected object will be increased by
        /// </summary>
        [SerializeField]
        [Tooltip("The size of the potion the _affected object will be increased by")]
        private int _size;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: Item has been collected
        /// </summary>
        public delegate void PotionCollectedHandler(object sender, ItemCollectedEventArgs e);
        public static event PotionCollectedHandler Collected;

        /// <summary>
        /// the type of the potion. Multiple potion gameobjects may have the same
        /// type
        /// </summary>
        public PotionTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The size of the potion
        /// </summary>
        public int Size {
            get { return _size; }
        }

        /// <summary>
        /// the icon of the item
        /// </summary>
        public Sprite Icon {
            get { return _icon; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The icon of the item
        /// </summary>
        private Sprite _icon;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CacheComponents();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            SpriteRenderer r = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (r == null)
                throw new ComponentNotFoundException("SpriteRenderer", this);

            _icon = r.sprite;
        }

        /// <summary>
        /// Raise the event
        /// </summary>
        protected override void OnItemCollected() {
            ItemCollectedEventArgs args = new ItemCollectedEventArgs();
            // Check if there are any Subscribers
            if (Collected != null)
                Collected(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}