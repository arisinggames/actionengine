﻿using System;
using ArisingGames.Assets.Scripts.EventsArgs;
using ArisingGames.Assets.Scripts.Exceptions;
using ArisingGames.Assets.Scripts.Weapons;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// The representation of a weapon to collect
    /// </summary>
	public class WeaponCollectable : AbstractCollectable, IWeaponCollectable {
        #region CONFIGURATION
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The weapon type
        /// </summary>
        [Header("Configuration")]
        [SerializeField]
        [Tooltip("The weapon type")]
        private WeaponTypes _type;

        /// <summary>
        /// Show the number of (remaining) ammo?
        /// </summary>
        [SerializeField]
        [Tooltip("Show the number of (remaining) ammo?")]
        private bool _showAmmo = false;
#pragma warning restore 0649
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// Event: Item has been collected
        /// </summary>
        public delegate void WeaponCollectedHandler(object sender, ItemCollectedEventArgs e);
        public static event WeaponCollectedHandler Collected;

        /// <summary>
        /// The type of the weapon. Multiple weapon gameobjects may have the same
        /// type
        /// </summary>
        public WeaponTypes Type {
            get { return _type; }
        }

        /// <summary>
        /// The icon of the item
        /// </summary>
        public Sprite Icon {
            get { return _icon; }
        }

        /// <summary>
        /// Show the number of (remaining) ammo?
        /// </summary>
        public bool ShowAmmo {
            get { return _showAmmo; }
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES

        #region PRIVATE_FIELDS
        /// <summary>
        /// The icon of the item
        /// </summary>
        private Sprite _icon;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        protected void Awake() {
            CacheComponents();
        }
        #endregion MONO_EVENTS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            SpriteRenderer r = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (r == null)
                throw new ComponentNotFoundException("SpriteRenderer", this);

            _icon = r.sprite;
        }

        /// <summary>
        /// Raise the event
        /// </summary>
        protected override void OnItemCollected() {
            ItemCollectedEventArgs args = new ItemCollectedEventArgs();
            // Check if there are any Subscribers
            if (Collected != null)
                Collected(this, args);
        }
        #endregion PRIVATE_METHODS
    }
}