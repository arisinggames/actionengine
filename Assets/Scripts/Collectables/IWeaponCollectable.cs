﻿using ArisingGames.Assets.Scripts.Weapons;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Interface for collectable weapons
    /// </summary>
	public interface IWeaponCollectable {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the weapon. Multiple (collectable) weapons 
        /// gameobjects may have the same type
        /// </summary>
        WeaponTypes Type {
            get;
        }

        /// <summary>
        /// The image of the collectable
        /// </summary>
        Sprite Icon {
            get;
        }

        /// <summary>
        /// Show the number of (remaining) ammo?
        /// </summary>
        bool ShowAmmo {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}