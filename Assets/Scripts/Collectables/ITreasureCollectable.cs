﻿namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// Interface for treasures
    /// </summary>
	public interface ITreasureCollectable {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The treasure value
        /// </summary>
        int Value {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}