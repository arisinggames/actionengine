﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Collectables {
    /// <summary>
    /// All collectable items
    /// </summary>
    public enum CommonCollectableTypes {
        Sheep
    }

    /// <summary>
    /// Interface for ammo (bullets, rockets, arrows)
    /// </summary>
    public interface ICommonCollectable {
        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The type of the ammo. Multiple ammo gameobjects may have the same
        /// type
        /// </summary>
        CommonCollectableTypes Type {
            get;
        }

        /// <summary>
        /// The image of the collectable
        /// </summary>
        Sprite Icon {
            get;
        }
        #endregion PUBLIC_FIELDS_AND_PROPERTIES
    }
}