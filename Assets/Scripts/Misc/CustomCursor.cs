﻿using ArisingGames.Assets.Scripts.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Misc {
    /// <summary>
    /// Set the cursor during the "action" and the "menu"
    /// </summary>
    public class CustomCursor : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The cursor during gameplay
        /// Must be of texture type "cursor"
        /// </summary>
        [Header("References")]
        [AssertReference]
        [SerializeField]
        private Texture2D _actionCursor;

        /// <summary>
        /// The cursor in the menu
        /// </summary>
        [SerializeField]
        [AssertReference]
        private Texture2D _menuCursor;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONFIGURATION
        /// <summary>
        /// Force software cursor?
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [SerializeField]
        private CursorMode _mode = CursorMode.Auto;

        /// <summary>
        /// The offset
        /// </summary>
        [SerializeField]
        private Vector2 _hotSpot = Vector2.zero;
        #endregion CONFIGURATION

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
        }

        /// <summary>
        /// ShowActionCursor must be called in Start() otherwise
        /// it won't work reliable (eg. with WebGL)
        /// </summary>
        private void Start() {
            ShowActionCursor();
        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Set action cursor
        /// </summary>
        public void ShowActionCursor() {
            Cursor.SetCursor(_actionCursor, _hotSpot, _mode);
        }

        /// <summary>
        /// Set menu cursor
        /// </summary>
        public void ShowMenuCursor() {
            Cursor.SetCursor(_menuCursor, _hotSpot, _mode);
        }
        #endregion PUBLIC_METHODS
    }
}
