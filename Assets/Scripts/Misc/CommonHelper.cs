﻿using System;
using System.Reflection;
using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Exceptions;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Misc {
    public static class CommonHelper {
        #region PUBLIC_METHODS
        /// <summary>
        /// Check if the gameobjects layer is in certain masks
        /// 
        /// See http://answers.unity3d.com/questions/150690/using-a-bitwise-operator-with-layermask.html
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="mask"></param>
        /// <returns></returns>
        public static bool IsInLayerMask(GameObject obj, LayerMask mask) {
            return ((mask.value & (1 << obj.layer)) > 0);
        }

        /// <summary>
        /// Create a rotation struct (Quarternion) pointing in a 
        /// certain direction
        /// 
        /// See
        /// https://docs.unity3d.com/ScriptReference/Mathf.Atan2.html
        /// http://www.gamefromscratch.com/post/2012/11/18/GameDev-math-recipes-Rotating-to-face-a-point.aspx
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Quaternion Rotate(Vector3 direction) {
            // get the angle between the positive x axis and the direction of the arrow 
            var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            // based on the x axis (= Vector3.forward) create a rotation object for the
            // new angle
            return Quaternion.AngleAxis(angle, Vector3.forward);
        }

        /// <summary>
        /// Rotate a vector based on an angle and return the new vector
        /// see http://answers.unity3d.com/questions/661383/whats-the-most-efficient-way-to-rotate-a-vector2-o.html
        /// Another approach would be to use trigonometry (also taken from the linked site):
        /// 
        /// float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        /// float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
        /// 
        /// float tx = v.x;
        /// float ty = v.y;
        /// v.x = (cos* tx) - (sin* ty);
        /// v.y = (sin* tx) + (cos* ty);
        /// return v;
        /// 
        /// Some more trigonometry:
        /// http://stackoverflow.com/questions/11773889/how-to-calculate-a-vector-from-an-angle-with-another-vector-in-2d
        /// </summary>
        /// <param name="v"></param>
        /// <param name="degrees"></param>
        /// <returns></returns>
        public static Vector3 RotateVector(Vector3 v, float degrees) {
            return Quaternion.Euler(0, 0, degrees) * v;
        }

        /// <summary>
        /// Find in the supplied obj all private fields that 
        /// have the attribute AssertReference and check if a reference is set
        /// 
        /// Execute the method only in debug mode as in production mode
        /// the references should already be set
        /// </summary>
        /// <param name="obj"></param>
        public static void AssertReferences(MonoBehaviour obj) {
#if DEBUG
            var type = obj.GetType();

            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo field in fields) {
                object value = field.GetValue(obj);

                Attribute attribute = Attribute.GetCustomAttribute(field, typeof(AssertReference));
                if (attribute != null && (
                    value == null ||
                    IsReferenceFakeNull(value))) {
                    throw new InspectorMissingReferenceException(field.ToString(), obj);
                }
            }
#endif
        }

        /// <summary>
        /// See
        /// http://answers.unity3d.com/questions/939119/using-reflection-sometimes-a-field-is-null-and-som.html
        /// </summary>
        /// <param name="aRef"></param>
        /// <returns></returns>
        private static bool IsReferenceFakeNull(object aRef) {
            return aRef != null && aRef.Equals(null);
        }
        #endregion PUBLIC_METHODS
    }
}
