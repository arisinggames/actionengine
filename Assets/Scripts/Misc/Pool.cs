﻿using System.Collections.Generic;
using ArisingGames.Assets.Scripts.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.Misc {
    /// <summary>
    /// Based on https://www.youtube.com/watch?v=9-zDOJllPZ8
    /// </summary>
    public class Pool : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
#pragma warning disable 0649
        /// <summary>
        /// The object that lays the foundation for this pool
        /// </summary>
        [Header("References")]
        [Tooltip("The owner of the sensors")]
        [AssertReference]
        [SerializeField]
        private GameObject _pooledGameObject;
#pragma warning restore 0649
        #endregion REFERENCES

        #region CONSTANTS
        private const bool ObjectEnabled = true;
        private const bool ObjectDisabled = false;
        #endregion CONSTANTS

        #region CONFIGURATION
        /// <summary>
        /// When the chase state gets first time active
        /// create a pool with bullets with the number of
        /// bullets specified in InitPoolSize
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [SerializeField]
        private int _initialPoolSize = 20;

        /// <summary>
        /// If there is a call for an object in the pool
        /// but the pool has reached it's maximum
        /// create a new one?
        /// </summary>
        [SerializeField]
        private bool _willGrow = true;
        #endregion CONFIGURATION

        #region PRIVATE_FIELDS
        private List<GameObject> _pool;

        /// <summary>
        /// Enumerating the names
        /// </summary>
        private static int _id;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);

            _pool = new List<GameObject>();
            for (int i = 0; i < _initialPoolSize; i++)
                Inc(ObjectDisabled);
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {

        }
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Return an object from the pool
        /// </summary>
        /// <returns></returns>
        public GameObject Get() {
            for (int i = 0; i < _pool.Count; i++)
                if (!_pool[i].activeInHierarchy) 
                    return _pool[i];

            if (_willGrow)
                return Inc(ObjectEnabled);

            return null;
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Add pooled obj to the pool
        /// </summary>
        private GameObject Inc(bool enabled) {
            GameObject go = Instantiate(_pooledGameObject);
            go.transform.parent = transform;
            go.name = go.name + " " + (++_id);
            go.SetActive(enabled);
            _pool.Add(go);
            return go;
        }
        #endregion PRIVATE_METHODS
    }
}
