﻿using System;

namespace ArisingGames.Assets.Scripts.Attributes {
    /// <summary>
    /// Used for AssertReferences
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    class AssertReference : Attribute { } 
}
