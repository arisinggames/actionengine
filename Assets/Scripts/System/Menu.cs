﻿using ArisingGames.Assets.Scripts.Attributes;
using ArisingGames.Assets.Scripts.Misc;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ArisingGames.Assets.Scripts.System {
	public class Menu : MonoBehaviour {
        #region REFERENCES
        // disable the warning that fields are unassigned 
        // as they _are_ assigned in the inspector
        #pragma warning disable 0649
        /// <summary>
        /// Reference to the pause panel
        /// </summary>
        [Header("References")]
        [AssertReference]
        [SerializeField]
        private GameObject _pauseUiGameObject;

        /// <summary>
        /// Reference to the game start menu
        /// </summary>
        [AssertReference]
        [SerializeField]
        private GameObject _startUiGameObject;

        /// <summary>
        /// Reference to the customcursos script (=the gameobject containing
        /// the script)
        /// </summary>
        [Tooltip("Reference to the custom cursos script")]
        [AssertReference]
        [SerializeField]
        private GameObject _cursorGameObject;
#pragma warning restore 0649
        #endregion

        #region PRIVATE_FIELDS
        /// <summary>
        /// State: paused
        /// </summary>
        private bool _paused;

        /// <summary>
        /// The cursor script
        /// </summary>
        private CustomCursor _cursor;
        #endregion

        #region MONO_EVENTS
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            CommonHelper.AssertReferences(this);
            CacheComponents();
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
	        _pauseUiGameObject.SetActive(false);
            _startUiGameObject.SetActive(false);
	    }

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
	        if (Input.GetButtonDown("Pause"))
	            _paused = !_paused;

	        if (_paused) {
	            _pauseUiGameObject.SetActive(true);
	            Time.timeScale = 0;
	        } else {
                _pauseUiGameObject.SetActive(false);
                Time.timeScale = 1;
            }
	    }
        #endregion

        #region PUBLIC_METHODS
        /// <summary>
        /// Restart game
        /// </summary>
	    public void Restart() {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        /// <summary>
        /// Go to the main manu
        /// </summary>
        public void MainMenu() {
            SceneManager.LoadScene("MainMenu");
        }

        /// <summary>
        /// Toggle pause menu
        /// </summary>
        public void TogglePause() {
            _paused = !_paused;

            if (_paused)
                Pause();
            else
                Resume();
        }

        /// <summary>
        /// Show pause menu
        /// </summary>
        public void Pause() {
            _paused = true;
            _pauseUiGameObject.SetActive(true);
            Time.timeScale = 0;
            _cursor.ShowMenuCursor();
        }

        /// <summary>
        /// Resume game
        /// </summary>
	    public void Resume() {
            _paused = false;
            _pauseUiGameObject.SetActive(false);
            Time.timeScale = 1;
            _cursor.ShowActionCursor();
        }

        /// <summary>
        /// Exit game
        /// </summary>
        public void Quit() {
            Application.Quit();
        }
        #endregion PUBLIC_METHODS

        #region PRIVATE_METHODS
        /// <summary>
        /// Cache the components for faster access
        /// </summary>
        private void CacheComponents() {
            _cursor = _cursorGameObject
                .GetComponent(typeof(CustomCursor)) as CustomCursor;
        }
        #endregion PRIVATE_METHODS
    }
}