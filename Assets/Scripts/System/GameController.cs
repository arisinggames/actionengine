﻿using System;
using ArisingGames.Assets.Scripts.Player.Attributes;
using UnityEngine;

namespace ArisingGames.Assets.Scripts.System {
	public class GameController : MonoBehaviour {
        #region PRIVATE_FIELDS
        /// <summary>
        /// Singleton to prevent mutliple instances over level loading
        /// (an effect that will happeb as the GameController is 
        /// kept during scenes)
        /// </summary>
        private static GameController _instance;
        #endregion PRIVATE_FIELDS

        #region MONO_EVENTS	
        /// <summary>
        /// Awake event
        /// </summary>
        private void Awake() {
            InitSingleton();

            // subscribe to the death event
            HealthAttribute.Death += OnDeath;
        }

        /// <summary>
        /// Start event
        /// </summary>
        private void Start() {
		    // --- wie hier eine FSM implementieren?
            // --- wenn kein status: menu und die scene laden
            // ansonsten globale keys abfangen und dementsprechend game laden oder
            // pause anzeigen
		}

        /// <summary>
        /// Update event
        /// </summary>
        private void Update() {
		
		}
        #endregion MONO_EVENTS

        #region PUBLIC_METHODS
        /// <summary>
        /// Singleton accessor
        /// </summary>
        public static GameController Instance {
            get { return _instance; }
        }
        #endregion PUBLIC_METHODS

        /////////////////////////////
        /// 
        /// checkk für singleton in Start() einer jeden Methode
        /// die darauf zugreift
        /// http://blog.theknightsofunity.com/story-nullreferenceexception-part-22/



        #region PRIVATE_METHODS
        /// <summary>
        /// init singleton
        /// </summary>
        private void InitSingleton() {
            if (_instance != null) {
                DestroyImmediate(gameObject);
                return;
            }
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        /// <summary>
        /// if player dies restart
        /// </summary>
	    private void OnDeath(object sender, EventArgs e) {
            Debug.Log("GameController.OnDeath() player has died");
            //SceneManager.LoadScene(SceneManager.GetActiveScene());
        }
        #endregion PRIVATE_METHODS
    }
}