﻿using UnityEngine;

namespace ArisingGames.Assets.Scripts.Map {
    /// <summary>
    /// Class represents the boundary of the map in units
    /// </summary>
    public class MapBoundaries : MonoBehaviour {
        #region CONFIGURATION
        /// <summary>
        /// The bottom left coordinate (x axis)
        /// </summary>
        [Space(3)]
        [Header("Configuration")]
        [Tooltip("The bottom left coordinate (x axis)")]
        [SerializeField]
        private float _xMin = 0;

        /// <summary>
        /// The bottom left coordinate (y axis)
        /// </summary>
        [Tooltip("The bottom left coordinate (y axis)")]
        [SerializeField]
        private float _yMin = 0;

        /// <summary>
        /// The top right coordinate (x axis)
        /// </summary>
        [Tooltip("The top right coordinate (y axis)")]
        [SerializeField]
        private float _xMax = 25;

        /// <summary>
        /// The top right coordinate (y axis)
        /// </summary>
        [Tooltip("The top right coordinate (y axis)")]
        [SerializeField]
        private float _yMax = 25;
        #endregion CONFIGURATION

        #region PUBLIC_FIELDS_AND_PROPERTIES
        /// <summary>
        /// The bottom left coordinate (x axis)
        /// </summary>
        public float XMin {
            get { return _xMin; }
        }

        /// <summary>
        /// The bottom left coordinate (y axis)
        /// </summary>
        public float YMin {
            get { return _yMin; }
        }

        /// <summary>
        /// The top right coordinate (x axis)
        /// </summary>
        public float XMax {
            get { return _xMax; }
        }

        /// <summary>
        /// The top right coordinate (y axis)
        /// </summary>
        public float YMax {
            get { return _yMax; }
        }
        #endregion PROPERTIES

        #region MONO_EVENTS
        /// <summary>
        /// Highlight tne boundaries
        /// </summary>
        private void OnDrawGizmos() {
            Gizmos.DrawIcon(new Vector2(XMin, YMin), "map-marker-min.png", true);
            Gizmos.DrawIcon(new Vector2(XMax, YMax), "map-marker-max.png", true);

            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(new Vector2(XMin, YMax), new Vector2(XMax, YMax));
            Gizmos.DrawLine(new Vector2(XMax, YMax), new Vector2(XMax, YMin));
            Gizmos.DrawLine(new Vector2(XMax, YMin), new Vector2(XMin, YMin));
            Gizmos.DrawLine(new Vector2(XMin, YMin), new Vector2(XMin, YMax));
        }
        #endregion MONO_EVENTS
    }
}